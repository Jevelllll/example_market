import {detectTrTable, translatorTableColumn} from "../base";

document.addEventListener('DOMContentLoaded', function () {
    setTimeout(function () {
        $('#dataTable').DataTable(translatorTableColumn());
        // var data = table.data().sort().reverse();
    }, 0);
    let urlTegs = document.querySelector('.tegs');
    if (urlTegs) {
        detectTrTable('dataTable', urlTegs.getAttribute('tegs'));
    }

    function sizeViewCardKey(selector) {
        if (selector) {
            let elementCollection = [];
            let maxWidthList = [].slice.call(selector.parentElement.querySelectorAll('.view_card__key'))
                .map((el) => {
                    elementCollection.push(el);
                    return el.clientWidth
                });
            let maxWidth = Math.max(...maxWidthList);

            elementCollection.forEach((el) => el.style.width = `${maxWidth}px`);
        }
    }

    sizeViewCardKey(document.querySelector('.view_card'));
    sizeViewCardKey(document.querySelector('.view_card_c'));
});
