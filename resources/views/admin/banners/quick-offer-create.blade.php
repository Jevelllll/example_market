@extends('admin.layouts.admin_template')
@section('content')


    <div class="card shadow mb-4">
        @if($isCreate)
            <h1 class="h3 mb-4 text-gray-800 text-center pt-3">Создать рекламное предложение</h1>
        @else
            <h1 class="h3 mb-4 text-gray-800 text-center pt-3">Обновить рекламное предложение</h1>
        @endif
        <div class="row justify-content-center">
            @if($isCreate)
                {{Form::model($model, array('route' => array('qOffer.save'),
                    'id'=>'QOfferCR', 'method' => 'POST', 'enctype'=>'multipart/form-data')) }}
            @else
                {{Form::model($model, array('route' => array('qOffer.update', $model->id),
                    'id'=>'QOfferCR', 'method' => 'PUT', 'enctype'=>'multipart/form-data')) }}
            @endif
            <div class="form-group">
                {{Form::label('name', 'Название', ['for' => 'name'])}}
                {{Form::text('name',  null, ['id' => 'name', 'class' => 'form-control'])}}
                <div class="alert alert-danger alert-dismissible d-none">
                    <button type="button" class="close">&times;</button>
                    <strong class="infoError"></strong>
                </div>
            </div>
            <div class="form-group">
                {{Form::label('desc', 'Описание', ['for' => 'desc'])}}
                {{Form::textarea('desc', null, ['id' => 'desc','class'=> 'form-control', 'rows' => 3])}}
                <div class="alert alert-danger alert-dismissible d-none">
                    <button type="button" class="close">&times;</button>
                    <strong class="infoError"></strong>
                </div>
            </div>
            <div class="form-group">
                {{Form::label('start', 'Начало предложения', ['for' => 'start'])}}
                {{Form::date('start', null, ['id' => 'start','class'=> 'form-control'])}}
                <div class="alert alert-danger alert-dismissible d-none">
                    <button type="button" class="close">&times;</button>
                    <strong class="infoError"></strong>
                </div>
            </div>
            <div class="form-group">
                {{Form::label('end', 'Окончание предложения', ['for' => 'end'])}}
                {{Form::date('end', null, ['id' => 'end','class'=> 'form-control'])}}
                <div class="alert alert-danger alert-dismissible d-none">
                    <button type="button" class="close">&times;</button>
                    <strong class="infoError"></strong>
                </div>
            </div>
            <div class="form-group col-md-6">
                <label for="gallery">Изображение</label>
                <div class="needsclick dropzone" id="document-dropzone">
                </div>
                <div class="alert alert-danger alert-dismissible d-none">
                    <button type="button" class="close">&times;</button>
                    <strong class="infoError"></strong>
                </div>
            </div>
            @if($isCreate)
                {{Form::submit('Создать',
['class' => 'btn btn-form btn-success shadow-custom text-uppercase border-radius-50 btn-lg w-100 mt-2 mb-3'])}}
            @else
                {{Form::submit('Обновить',
 ['class' => 'btn btn-form btn-success shadow-custom text-uppercase border-radius-50 btn-lg w-100 mt-2 mb-3'])}}
            @endif
            {{ Form::close() }}
        </div>
    </div>
@endsection
@push('scripts')
    <script src="{{asset('/js/admin/brs/bList.js')}}"></script>

    <script>
        var uploadedDocumentMap = {};
        var removeCurrentFile = '';
        Dropzone.options.documentDropzone = {

            url: '{{ route('projects.storeMedia') }}',
            maxFilesize: 2, // MB
            maxFiles: 1,
            uploadMultiple: false,
            addRemoveLinks: true,
            // acceptedFiles: ".jpeg,.jpg,.png,.gif",
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            success: function (file, response) {

                @if($isCreate)
                $('#QOfferCR').append('<input type="hidden" name="gallery[]" value="' + response.name + '">')
                @else
                $('#QOfferCR').append('<input type="hidden" name="gallery[]" value="' + response.name + '">')
                @endif

                    uploadedDocumentMap[file.name] = response.name
            },
            sending: function () {
                $('.dz-image-preview').remove();
            },
            init: function () {
                    @if(isset($project) && $project->gallery)
                var files =
                {!! json_encode($project->gallery) !!}
                    for (var i in files) {
                    var file = files[i]
                    console.log(file);
                    this.options.addedfile.call(this, file)
                    file.previewElement.classList.add('dz-complete')
                    $('#QOfferCR').append('<input type="hidden" name="gallery[]" value="' + file.file_name + '">')
                }
                @endif
                    this.on("maxfilesexceeded", function (file) {
                    this.removeAllFiles();
                    this.addFile(file);
                });

                this.on('addedfile', function (file) {
                    var preview = document.getElementsByClassName('dz-preview');
                    preview = preview[preview.length - 1];
                    var imageName = document.createElement('span');
                    imageName.innerHTML = file.name;
                });
                    @if(!$isCreate)
                var url = '/admin/banner/banners-files/{{$model->id}}/{{true}}';
                var thisDropzone = this;
                $.getJSON(url, function (data) { // get the json response
                    if (Array.isArray(data)) {
                        removeCurrentFile = data[0].name;
                    }
                    $.each(data, function (key, value) { //loop through it
                        if (value.hasOwnProperty('shortName')) {
                            var mockFile = {name: value.shortName, size: value.size}; // here we get the file name and size as response
                            thisDropzone.options.addedfile.call(thisDropzone, mockFile);
                            thisDropzone.options.thumbnail.call(thisDropzone, mockFile, value.name);//uploadsfolder is the folder where you have all those uploaded files
                        }
                    });
                });
                @endif
            },
            removedfile: function (file) {
                var foolFileName = {};
                var name = '';
                file.previewElement.remove();
                if (removeCurrentFile) {
                    if (removeCurrentFile.length === 0) {
                        foolFileName = file.xhr.responseText;
                        name = '';
                        if (typeof file.file_name !== 'undefined') {
                            name = file.file_name
                        } else {
                            name = uploadedDocumentMap[file.name]
                        }
                    }
                }
                var mainGallery = $('#document-dropzone');
                var alert = $(mainGallery).siblings(".alert");
                $(alert).find(".infoError").text('Тип файла не изображение');
                $(alert).removeClass("d-block");
                  console.log(foolFileName)
                  console.log(removeCurrentFile)
                $.ajax({
                    url: '{{ route('projects.delete')}}',
                    type: 'post',
                    data: {
                        name: foolFileName,
                        filePath: removeCurrentFile
                    },
                    headers: {
                        'X-CSRF-TOKEN': "{{ csrf_token() }}"
                    },
                    dataType: 'json',
                    success: function (data) {
                        console.info(data);
                    }
                });

                $('form').find('input[name="gallery[]"][value="' + name + '"]').remove()
            },
        };
        Dropzone.prototype.defaultOptions.dictRemoveFile = "Удалить файл";
    </script>
@endpush
