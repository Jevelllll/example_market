<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    @foreach ($products  as $product)
        @if($product instanceof \App\Products)
            <url>
                <loc>{{route('client-product', ['seoName' => $product->seo_name])}}</loc>
                <lastmod>{{ $product->updated_at->toAtomString() }}</lastmod>
                <changefreq>daily</changefreq>
                <priority>1</priority>
            </url>
        @endif
    @endforeach
</urlset>
