(function () {
    setTimeout(() => {
        $(".loader").hide();
    }, 200)


    let pathArray = '/' + window.location.pathname.split('/').filter((it) => it).join('/');
    let accordionSidebar = document.getElementById('accordionSidebar');
    let elementsByClassName = accordionSidebar.getElementsByClassName('nav-item');



    [].slice.call(elementsByClassName).forEach((element, index) => {
        if (index !== 0) {

            let elCollapseItem = element.getElementsByClassName('collapse-item');
            if (elCollapseItem.length > 0) {

                [].slice.call(elCollapseItem).forEach((collapseElement, collapseIndex) => {
                    let attributePath = collapseElement.getAttribute('href');

                    if (attributePath.indexOf(pathArray) !== -1 && pathArray !== '/admin') {

                        let parentElement = collapseElement.parentElement.parentElement;
                        $(parentElement).collapse();
                        collapseElement.classList.add('active');
                    }
                });
            }
        }
    });

    const date = new Date();
    document.getElementById('copyrightDate').innerText = `Copyright INITIALES ${date.getFullYear().toString()}`;

    HTMLElement.prototype.serialize = function(){
        let requestArray = [];
        this.querySelectorAll('[name]').forEach((elem) => {
            requestArray.push(elem.name + '=' + elem.value);
        });
        if(requestArray.length > 0)
            return requestArray.join('&');
        else
            return false;
    }
})();
