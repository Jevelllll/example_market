<?php


namespace App\Services;


use App\Banners;
use App\Http\Controllers\Controller;

class BannersListService extends Controller
{
    public function getAll(){
        return Banners::all();
    }
}
