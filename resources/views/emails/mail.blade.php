<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Order Confirmation</title>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Montserrat:wght@500;700&display=swap');
    </style>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@500;700&display=swap" rel="stylesheet">
    <style rel="stylesheet">

        .container {
            border: 3px solid #7D7B79;
            border-radius: 4px;
            border-bottom: 4px solid #494C4C;
            border-top: 4px solid #494C4C;
            font-family: 'Montserrat', sans-serif;
            font-weight: 500;
            max-width: 600px;
            margin: 0 auto;
            width: 100%;
            padding: 15px;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
        }

        .paddings {
            margin-top: 10px;
            margin-bottom: 10px;
        }

        .wrap {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: justify;
            -ms-flex-pack: justify;
            justify-content: space-between;
            -webkit-box-orient: horizontal;
            -webkit-box-direction: normal;
            -ms-flex-direction: row;
            flex-direction: row;
        }

        .top_element {
            font-family: 'Montserrat', sans-serif;
            font-weight: 700;
            font-size: 17px;
            width: 100%;
        }

        .top_element a {
            text-decoration: none;
        }

        .prp {
            font-family: 'Montserrat', sans-serif;
            font-weight: 500;
            color: #3B0E31;
            padding-bottom: 10px;
        }

        .prp-sum {
            font-family: 'Montserrat', sans-serif;
            font-weight: 700;
            font-size: 17px;
            width: 100%;
        }

        .order {
            font-family: 'Montserrat', sans-serif;
            font-weight: 700;
        }

        img {
            max-height: 50px;
        }

        ul {
            list-style: square outside;
            margin: 0;
        }

        li {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
        }

        .pr {
            padding-right: 10px;
        }

        .text-right {
            text-align: right;
        }
    </style>
</head>
<body>
<div class="container"
     style="border: 3px solid #7D7B79;border-radius: 4px;border-bottom: 4px solid #494C4C;border-top: 4px solid #494C4C;font-family: 'Montserrat', sans-serif;font-weight: 500;max-width: 600px;margin: 0 auto;width: 100%;padding: 15px;-webkit-box-sizing: border-box;box-sizing: border-box;">
    <div class="wrap paddings"
         style="margin-top: 10px;margin-bottom: 10px;display: flex;-webkit-box-pack: justify;-ms-flex-pack: justify;justify-content: space-between;-webkit-box-orient: horizontal;-webkit-box-direction: normal;-ms-flex-direction: row;flex-direction: row;">
        <div class="top_element"
             style="font-family: 'Montserrat', sans-serif;font-weight: 700;font-size: 17px;width: 100%;">
            <a href="{{env('APP_URL')}}" style="text-decoration: none;">{{config('app.name')}}</a>
        </div>
        <div class="top_element"
             style="text-align: right;font-family: 'Montserrat', sans-serif;font-weight: 700;font-size: 17px;width: 100%;">
            Order Confirmation
        </div>
    </div>
    <div class="paddings" style="margin-top: 10px;margin-bottom: 10px;">
        <div class="prp"
             style="font-family: 'Montserrat', sans-serif;font-weight: 500;color: #3B0E31;padding-bottom: 10px;">
            Hello, @if($orderModel->customer)
                {{$orderModel->customer->first_name}} {{$orderModel->customer->last_name}}! @endif</div>
        <div>Thank you for shopping with us. Your ordered <i class="order"
                                                             style="font-family: 'Montserrat', sans-serif;font-weight: 700;">"#2-75AC3A"</i>
            Well send a
            confirmation when your item ships.
        </div>
    </div>
    <hr>
    <?php $sum = 0;?>
    <div class="paddings" style="margin-top: 10px;margin-bottom: 10px;">
        <div class="prp"
             style="font-family: 'Montserrat', sans-serif;font-weight: 500;color: #3B0E31;padding-bottom: 10px;">
            Details:
        </div>
        <ul style="list-style: square outside;margin: 0;">
            @foreach($orderModel->order_product as $ordProd)
                <?php $product = $ordProd->product;
                $sum += $product->price;
                ?>
                <li style="display: flex;-webkit-box-align: center;-ms-flex-align: center;align-items: center;">
                    <div class="pr" style="padding-right: 10px;">
                        <a href="{{env('APP_URL')}}/product/{{$product->seo_name}}">
                            <img
                                src="{{asset('images/product/pictures/thumbnails/'. $product->pictures[0]->image_list)}}"
                                alt="{{$product->name}}" style="max-height: 50px;">
                        </a>
                    </div>
                    <div class="pr" style="padding-right: 10px;">
                        <a href="{{env('APP_URL')}}/product/{{$product->seo_name}}" style="text-decoration: none; color: black;"
                           onmouseover="this.style.color='black'"
                           onMouseOut="this.style.color='green'">
                            {{$product->name}}
                        </a>
                    </div>
                    <div class="pr order"
                         style="font-family: 'Montserrat', sans-serif;font-weight: 700;padding-right: 10px;">{{$product->price}}
                        USD
                    </div>
                </li>
                <hr>
            @endforeach

        </ul>
    </div>
    <hr>
    <div class="paddings" style="margin-top: 10px;margin-bottom: 10px;">
        <div class="prp"
             style="font-family: 'Montserrat', sans-serif;font-weight: 500;color: #3B0E31;padding-bottom: 10px;">
            SHIPPING (Standard Post International Air Shipping)
        </div>
        <div class="prp prp-sum text-right"
             style="font-family: 'Montserrat', sans-serif;font-weight: 700;color: #3B0E31;padding-bottom: 10px;font-size: 17px;width: 100%;text-align: right;">
            total: {{$sum}} USD
        </div>
    </div>
</div>

</body>
</html>
