<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer id
 * @property integer order_id
 * @property string first_name
 * @property string last_name
 * @property string address_country
 * @property string address_state
 * @property string address_city
 * @property string address_street
 * @property string address_zip_code
 * @property string address_house_number
 * @property string phone
 * @property string email
 * @property string comment
 */
class Customers extends Model
{
    protected $fillable = [
        "created_at", "updated_at", "order_id", "first_name",
        "last_name", "address_country", "address_state", "address_city", "address_street",
        "address_zip_code", "address_house_number", "phone", "email", "comment"
    ];
}
