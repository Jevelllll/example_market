<?php


namespace App\Facades;


use Illuminate\Support\Facades\Facade;

class UserRulesInfoFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'rulesUsers';
    }
}
