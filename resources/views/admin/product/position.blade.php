@extends('admin.layouts.admin_template')
@section('content')
    @push('style')
  <style>
      #scrollable {
          overflow-x: scroll;
      }

      #sortable {
          list-style: none;
          border-width: 6px 0;
          padding: 4px 0;
          height: 150px;
          width:calc(4 * 200px + 1px); // 1px for dragging
      }

      .item_sortable {
          float: left;
          /*height: 100%;*/
          width: 200px;
          box-sizing: border-box; /* keep padding/border inside height/width */
          border: solid green 4px;
          background-color: white;
      }
  </style>
    @endpush
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary pt-3">Позиционирование изображений</h6>
            <div class="card-body ">
                <ul id="sortable" class="m-auto">
                    @if($pictures)
                        @foreach($pictures as $picture)
                            @if($picture)
                                <?php
                                    $index = ($picture->position_index) ? $picture->position_index: $loop->index;
                                ?>
                                <li attr-picture-id="{{$picture->id}}"
                                    attr-picucre-index="{{$index}}" class="ui-state-default item_sortable">
                                    <span class="ui-icon ui-icon-arrowthick-2-n-s">
                                            <img
                                                src="{!! asset('/images/product/pictures/thumbnails/'. $picture["image_list"]) !!}"
                                                class="img-fluid rounded">
                                    </span>
                                </li>
                            @endif
                        @endforeach
                    @endif
                </ul>
            </div>
        </div>
        <button class="btn btn-sm btn-success"
                id="sendPosition"
                attr-current-url="{{route('position.pictures', ['product_id' => $pictures[0]->product_id])}}"
                attr-position-update="{{route('position.pictures.update')}}"
                type="button">Применить</button>
    </div>
    @push('scripts')
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="{{asset('/js/admin/products/position.js')}}"></script>
    @endpush
@endsection
