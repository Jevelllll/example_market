@extends('admin.layouts.admin_template')
@section('content')

    <!-- Modal body -->
    <div class="modal-body">

        <div class="card shadow mb-4 container">
            <h1 class="h3 mb-4 text-gray-800 text-center pt-3">Редактирование {{$model->title}}</h1>
            <div class="justify-content-center">

                {{Form::model($model, array('route' => array('rfu.update', $model->id), 'id'=>'rulesUpdate', 'method' => 'PUT')) }}
                <div class="form-group">
                    {{Form::label('title', 'Название', ['for' => 'title'])}}
                    {{Form::text('title',  null, ['id' => 'title', 'class' => 'form-control'])}}
                    <div class="alert alert-danger alert-dismissible d-none">
                        <button type="button" class="close">&times;</button>
                        <strong class="infoError"></strong>
                    </div>
                </div>

                <div class="form-group">
                    {{Form::label('descR', 'Описание', ['for' => 'descR'])}}
                    {{Form::textarea('desc', null, ['id' => 'descR','class'=> 'form-control', 'rows' => 10])}}
                    <div class="alert alert-danger alert-dismissible d-none">
                        <button type="button" class="close">&times;</button>
                        <strong class="infoError"></strong>
                    </div>
                </div>
                <script>
                    CKEDITOR.replace( 'desc' );
                </script>
                {{Form::submit('Обновить', ['class' => 'btn btn-form btn-success shadow-custom text-uppercase border-radius-50 btn-lg w-100 mt-2 mb-3'])}}
                {{ Form::close() }}
            </div>
{{--            <button class="delete ml-auto btn btn-danger mb-5 float-right"--}}
{{--                    attr-delete="{{$banner->id}}"--}}
{{--                    attr-route="{{route("banner.delete")}}">Удалить&nbsp;<i class="far fa-trash-alt"></i></button>--}}
        </div>
    </div>
@endsection
@push('scripts')
    <script src="{{asset('/js/admin/rules_for_users/rules_for_users.js')}}"></script>
@endpush


