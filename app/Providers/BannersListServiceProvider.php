<?php


namespace App\Providers;


use Illuminate\Support\ServiceProvider;
use App\Services\BannersListService;

class BannersListServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $cL = new BannersListService();
        \Illuminate\Support\Facades\View::share('bannerList', $cL->getAll()->where('active', 1));
    }
}
