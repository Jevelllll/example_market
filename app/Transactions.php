<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer id
 * @property double mc_gross
 * @property string invoice
 * @property string protection_eligibility
 * @property string address_status
 * @property string payer_id
 * @property string address_street
 * @property string payment_date
 * @property string payment_status
 * @property string address_zip
 * @property string first_name
 * @property string address_country_code
 * @property string address_name
 * @property double notify_version
 * @property string custom
 * @property string payer_status
 * @property string address_country
 * @property integer num_cart_items
 * @property string address_city
 * @property string verify_sign
 * @property string payer_email
 * @property string txn_id
 * @property string payment_type
 * @property string last_name
 * @property string address_state
 * @property string receiver_email
 * @property double shipping_discount
 * @property double insurance_amount
 * @property string pending_reason
 * @property string txn_type
 * @property double discount
 * @property string mc_currency
 * @property string residence_country
 * @property integer test_ipn
 * @property integer order_id
 * @property string shipping_method
 * @property string transaction_subject
 * @property double payment_gross
 * @property string ipn_track_id
 * @property string transaction_id
 * @property string mode
 * @property string dispatch
 * @property string is_request_to_store
 * @property string wb_result
 * @property string wb_hash
 * @property string created_at
 * @property string updated_at
 * @property string is_read
 */
class Transactions extends Model
{
    protected $fillable = [
        "mc_gross", "invoice", "protection_eligibility", "address_status", "payer_id", "address_street",
        "payment_date", "payment_status", "address_zip", "first_name", "address_country_code", "address_name",
        "notify_version", "custom", "payer_status", "address_country", "num_cart_items", "address_city",
        "verify_sign", "payer_email", "txn_id", "payment_type", "last_name", "address_state",
        "receiver_email", "shipping_discount", "insurance_amount", "pending_reason", "txn_type", "discount",
        "mc_currency", "residence_country", "test_ipn", "shipping_method", "transaction_subject", "payment_gross",
        "ipn_track_id", "transaction_id", "mode", "dispatch", "is_request_to_store", "wb_result",
        "wb_hash", "created_at", "updated_at", "is_read", "order_id"
    ];
    public static function updateRead($id) {
        $model = Transactions::find($id);
        $model->is_read = '1';
        return $model->save();
    }
}
