@extends('admin.layouts.admin_template')
@push('style')
    <link href="{{asset('/css/admin/order_truns.css')}}" rel="stylesheet">
@endpush
@section('content')

    @if(isset($order))
        <h1>Заказ {{$order->order_nr}}</h1>
        <?php $ordKeys = array_keys(collect($order)->toArray())?>
        <ul class="card shadow mb-4 list-group">
            @foreach($ordKeys as $keys)
                <li class="view_card list-group-item">
                    @if($keys === 'status')
                        <span class="view_card__key">
                {{Translators::translateRow()[$keys]}}
            </span>
                        <span class="view_card__val">
                {{$order->$keys}}
                            @if(isset($order->status_paypal))
                                ({{$order->status_paypal->desc}})
                            @endif
            </span>
                    @elseif($keys === 'total_sum')
                        <span class="view_card__key">
                {{Translators::translateRow()[$keys]}}
            </span>
                        <span class="view_card__val">
                {{$order->$keys}} USD
            </span>
                    @else
                        <span class="view_card__key">
                {{Translators::translateRow()[$keys]}}
            </span>
                        <span class="view_card__val">
                {{$order->$keys}}
            </span>
                    @endif
                </li>
            @endforeach
        </ul>
        <h2>Данные доставки</h2>
        @if(count(collect($customer)->toArray()) > 0)
            <?php $customerKeys = array_keys(collect($customer)->toArray())?>
            <ul class="card shadow mb-4 list-group">
                @foreach($customerKeys as $customerKey)
                    @if($customer->$customerKey)
                        <li class="view_card view_card_c list-group-item">
                        <span class="view_card__key">
                            {{Translators::translateRow()[$customerKey]}}
                        </span>
                            <span class="view_card__val">
                            {{$customer->$customerKey}}
                        </span>
                        </li>
                    @endif

                @endforeach
            </ul>
        @else
            <ul class="card shadow mb-4 list-group">
                <li class="view_card list-group-item">Нет данных</li>
            </ul>
        @endif

        <h2>Товары текущего заказа</h2>
        <ul class="card shadow mb-4 list-group view_card">
            @foreach($orderProducts as $orderProduct)
                @if($orderProduct->product)
                <a href="{{route('edit.product', ['id' => $orderProduct->product->id])}}">
                    <li class="list-group-item view_card__transaction">
                        {{$orderProduct->product->name}} |
                        {{$orderProduct->product->price}} USD |
                    @if(isset($orderProduct->product->pictures))
                            @if(count($orderProduct->product->pictures) > 0)
                                @foreach($orderProduct->product->pictures as $picture)
                                    <img
                                        src="{!!asset('/images/product/pictures/thumbnails').'/'.$picture->image_list!!}">
                                @endforeach
                            @endif
                        @endif
                    </li>
                </a>
                @else
                        <li class="list-group-item view_card__transaction">Нет данных</li>
                @endif
            @endforeach
        </ul>
        <h2>Статусы платежей</h2>
        @if(count($transactions) > 0)
            <ul class="card shadow mb-4 list-group view_card">
                @foreach($transactions as $transaction)
                    <a href="{{route('admin.transactions.view', ['id' => $transaction->id])}}">
                        <li class="list-group-item view_card__transaction">
                            {{$transaction->payment_date}} |
                            {{$transaction->payer_email}} |
                            {{$transaction->payment_status}}
                        </li>
                    </a>
                @endforeach
            </ul>
        @else
            <ul class="card shadow mb-4 list-group view_card">
                <li class="list-group-item view_card__transaction">
                    Пусто
                </li>
            </ul>
        @endif
    @else
        <div style="font-size: 2.5rem" class="text-center">Отсутствуют заказы</div>
    @endif
@endsection
@push('scripts')
    <script
        src="{{asset('js/admin/orders/ordersList.js')}}?v=<?= filemtime('js/admin/orders/ordersList.js') ?>"></script>
@endpush
