@extends('admin.layouts.admin_template')
@push('style')
    <link href="{{asset('/css/admin/order_truns.css')}}" rel="stylesheet">
@endpush
@section('content')
    <span class='tegs' tegs="/admin/transactions/view/"></span>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary pt-3 ml-md-1">Статусы платежей</h6>
        </div>
        @if($transactions)
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            @foreach($transactionsTitle as $titles)
                                <th>{{Translators::translateRow()[$titles]}}</th>
                            @endforeach
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            @foreach($transactionsTitle as $titles)
                                <th>{{Translators::translateRow()[$titles]}}</th>
                            @endforeach
                        </tr>
                        </tfoot>
                        <tbody>
                        @foreach($transactions as $transaction)
                            <tr data-id="{{$transaction->id}}">
                                @foreach($transactionsTitle as $key => $titles)
                                    @if($titles === 'is_read')
                                        @if($transaction->$titles == 1)
                                            <td><span class="label label-success">Да</span></td>
                                        @else
                                            <td><span class="label label-warning">Нет</span></td>
                                        @endif
                                    @elseif($titles === 'total_sum')
                                        <td>{{$transaction->$titles}}&nbsp;USD</td>
                                    @else
                                        <td>{{$transaction->$titles}}</td>
                                    @endif
                                @endforeach
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        @else
            <div style="font-size: 2.5rem" class="text-center">Отсутствуют плетежи</div>
        @endif
    </div>
@endsection
@push('scripts')
    <script src="{{asset('/js/admin/orders/ordersList.js')}}"></script>
@endpush
