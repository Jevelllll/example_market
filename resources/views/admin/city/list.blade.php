@extends('admin.layouts.admin_template')
@section('content')
    <span class='tegs' tegs="/admin/city/edit/"></span>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary pt-3 ml-md-1">Список городов PayPal</h6>
            {{--            <a class="btn btn-md btn-success mt-2" href="{{route('new.country')}}">Добавить страну</a>--}}
        </div>
        @if($stateList)
            <div class="form-group pt-4 pl-4 pr-4">

                <select class="form-control" id="controlS">
                    <option value="null">Выберите регион</option>
                    @foreach($stateList as $state)
                        <option value="{{$state->id}}">
                            {{$state->name}}
                        </option>
                    @endforeach
                </select>
            </div>
        @else
            <div style="font-size: 2.5rem" class="text-center">Отсутствуют Регионы PayPal</div>
        @endif
        <div id="citiesList" attr-route="{{route('getCitiesByStateId.list', [0])}}"></div>
    </div>
@endsection
@push('scripts')
    <script src="{{asset('/js/admin/countries/countriesList.js')}}"></script>
@endpush
