@extends('admin.layouts.admin_template')
@push('style')
    <link href="{{asset('/css/admin/order_truns.css')}}" rel="stylesheet">
@endpush
@section('content')
    @if(isset($answer))
        <?php $answerKeys = array_keys(collect($answer)->toArray())?>
        <ul class="card shadow mb-4 list-group">
            @foreach($answerKeys as $keys)
                @if($keys === 'product_id' || $keys === 'to' || $keys === 'id')
                @else
                    <li class="view_card list-group-item">
                              <span class="view_card__key">
                {{Translators::translateRow()[$keys]}}
            </span>
                        <span class="view_card__val">
                {{$answer->$keys}}
            </span>
                    </li>
                @endif
            @endforeach
            @if($answer->product)
                <li class="view_card list-group-item">
                <span class="view_card__key">
                    Товар
                </span>
                    <span class="view_card__val">
                    <a href="{{route('edit.product', ['id' => $answer->product->id])}}" class="btn btn-success">Просмотреть</a>
                </span>
                </li>
            @endif
            @if($answer->parent)
                <li class="view_card list-group-item">
                <span class="view_card__key">
                    Ваш ответ
                </span>
                    <span class="view_card__val">
                   {{$answer->parent->message}}
                </span>
                </li>
            @endif
        </ul>

        @if(!$answer->parent)
            <div class="col-12 col-md-8 hide-pre-success pl-1 pl-md-3">
                {{Form::model($answer, ['route' => 'send.product.answer.to', 'id'=> 'askAnswer', 'method' => 'post', 'class' => 'mui-form'])}}
                {!! csrf_field() !!}
                {{ Form::hidden('from', $answer->to) }}
                {{ Form::hidden('to', $answer->from) }}
                {{ Form::hidden('product_id', $answer->product->id) }}
                {{ Form::hidden('answer_id', $answer->id) }}
                <div class="form-group">
                    <label for="message">Сообщение (<span style="color: red">ВАЖНО!</span> Дальнейшая переписка напрямую
                        через MAIL)</label>
                    <textarea value="" id="message" name="message" rows="3" class="form-control">
                </textarea>
                    <div class="alert alert-danger alert-dismissible d-none">
                        <button type="button" class="close">&times;</button>
                        <strong class="infoError"></strong>
                    </div>
                </div>
                {{Form::submit('Отправить', ['disabled' => true,'id' => 'sAns', 'class' => 'btn btn-form btn-success shadow-custom text-uppercase border-radius-50 btn-lg w-100 mt-2 mb-3'])}}
                {{Form::close()}}
            </div>
        @endif
    @else
        <div style="font-size: 2.5rem" class="text-center">Отсутствуют данные</div>
    @endif
@endsection
@push('scripts')
    <script
        src="{{asset('js/admin/answer/index.js')}}?v=<?= filemtime('js/admin/answer/index.js') ?>"></script>
    <script
        src="{{asset('js/admin/orders/ordersList.js')}}?v=<?= filemtime('js/admin/orders/ordersList.js') ?>"></script>
@endpush
