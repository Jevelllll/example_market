<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="preload" href="{{ asset('css/bootstrap/bootstrap.min.css') }}" as="style">
    <link href="{{ asset('css/bootstrap/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}?v=<?= filemtime('css/app.css') ?>" rel="stylesheet">
    <link rel="stylesheet"
          href="{{asset('css/errors/error-404.css')}}?v=<?= filemtime('css/errors/error-404.css') ?>">
    {{--    font awesome--}}
    <link rel="stylesheet"
          href="{{asset('font-awesome/css/fontawesome-all.min.css')}}?v=<?= filemtime('font-awesome/css/fontawesome-all.min.css') ?>">

    <title>{{ ($title) ? $title: env('APP_NAME') }}</title>
</head>
<body>
<div class="wrap-error mt-3 mb-3 pt-2 pb-2">
    <div class="row ml-0 mr-0 pl-0 pr-0">
        <div class="col-12 text-center ml-0 mr-0 pl-0 pr-0 pt-4 pb-0"><i class="far fa-frown"></i></div>
        <div class="col-12 text-center ml-0 mr-0 pl-0 pr-0 pb-2 line-normal text-center">
            <span class="error-number">404</span>
            <h1>SORRY, PAGE NOT FOUND</h1>
        </div>
        <div class="col-12 text-center ml-0 mr-0 pl-0 pr-0">Sorry, we couldn't find the page you were looking for.</div>
        <div class="col-12 text-center ml-0 mr-0 pl-0 pr-0">We suggest that you return to main section.</div>
        <div class="col-12 d-flex justify-content-center ml-0 mr-0 pl-0 pr-0">
            <a href="/" class="btn button-add-cart purple-gr text-center">Go to the main page</a>
        </div>
    </div>
</div>
</body>
</html>

