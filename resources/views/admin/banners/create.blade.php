@extends('admin.layouts.admin_template')
@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="card shadow mb-4">
        <h1 class="h3 mb-4 text-gray-800 text-center pt-3">Создать новый баннер</h1>
        <div class="row justify-content-center">
            {{ Form::open(['action' => 'BannersController@dataBannerSave', 'method' => 'post', 'class' => 'form',
            'id' => 'bannerSave', 'enctype'=>'multipart/form-data']) }}
            <div class="form-group">
                {{Form::label('name', 'Название', ['for' => 'name'])}}
                {{Form::text('name',  old('name'), ['id' => 'name', 'class' => 'form-control'])}}
                <div class="alert alert-danger alert-dismissible d-none">
                    <button type="button" class="close">&times;</button>
                    <strong class="infoError"></strong>
                </div>
            </div>
            <div class="form-group">
                {{Form::label('redirect_url', 'Адресс переадресации')}}
                {{Form::text('redirect_url', old('redirect_url'), ['id' => 'redirect_url', 'class' => 'form-control'])}}

                <div class="alert alert-danger alert-dismissible d-none">
                    <button type="button" class="close">&times;</button>
                    <strong class="infoError"></strong>
                </div>
            </div>

            <div class="form-group active">
                {{Form::label('Статус')}}

                <div class="form-check">
                    {{Form::radio('active', '1', null, ['id' => 'active'])}}
                    {{Form::label('active', 'Активен')}}
                </div>
                <div class="form-check">
                    {{Form::radio('active', '0', null, ['id' => 'not_active'])}}
                    {{Form::label('not_active', 'Не активно')}}
                </div>
                <div class="alert alert-danger alert-dismissible d-none">
                    <button type="button" class="close">&times;</button>
                    <strong class="infoError"></strong>
                </div>
            </div>
            <div class="form-group">
                {{Form::label('desc', 'Описание', ['for' => 'description'])}}
                {{Form::textarea('desc', old('desc'), ['id' => 'desc','class'=> 'form-control', 'rows' => 3])}}
                <div class="alert alert-danger alert-dismissible d-none">
                    <button type="button" class="close">&times;</button>
                    <strong class="infoError"></strong>
                </div>
            </div>
            <div class="form-group col-md-6">
                <label for="gallery">Изображение</label>
                <div class="needsclick dropzone" id="document-dropzone">
                </div>
                <div class="alert alert-danger alert-dismissible d-none">
                    <button type="button" class="close">&times;</button>
                    <strong class="infoError"></strong>
                </div>
            </div>

            {{Form::submit('Создать!', ['class' => 'btn btn-form btn-success shadow-custom text-uppercase border-radius-50 btn-lg w-100 mt-2 mb-3'])}}
            {{ Form::close() }}
        </div>

    </div>
@endsection
@push('scripts')
    <script>
        var uploadedDocumentMap = {}
        Dropzone.options.documentDropzone = {
            url: '{{ route('projects.storeMedia') }}',
            maxFilesize: 2, // MB
            maxFiles: 1,
            uploadMultiple: false,
            addRemoveLinks: true,
            // acceptedFiles: ".jpeg,.jpg,.png,.gif",
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            success: function (file, response) {
                $('#bannerSave').append('<input type="hidden" name="gallery[]" value="' + response.name + '">')
                uploadedDocumentMap[file.name] = response.name
            },
            removedfile: function (file) {
                var foolFileName = file.xhr.responseText;
                file.previewElement.remove()
                var name = ''
                if (typeof file.file_name !== 'undefined') {
                    name = file.file_name
                } else {
                    name = uploadedDocumentMap[file.name]
                }

                let mainGallery = $('#document-dropzone');
                let alert = $(mainGallery).siblings(".alert");
                $(alert).find(".infoError").text('Тип файла не изображение');
                $(alert).removeClass("d-block");

                $.ajax({
                    url: '{{ route('projects.delete')}}',
                    type: 'post',
                    data: {
                        name: foolFileName
                    },
                    headers: {
                        'X-CSRF-TOKEN': "{{ csrf_token() }}"
                    },
                    dataType: 'json',
                    success: function (data) {
                        console.info(data);
                    }
                });

                $('form').find('input[name="gallery[]"][value="' + name + '"]').remove()
            },
            init: function () {
                        @if(isset($project) && $project->gallery)
                var files =
                {!! json_encode($project->gallery) !!}
                    for (var i in files) {
                    var file = files[i]
                    this.options.addedfile.call(this, file)
                    file.previewElement.classList.add('dz-complete')
                    $('#bannerSave').append('<input type="hidden" name="gallery[]" value="' + file.file_name + '">')
                }
                @endif
                    this.on("maxfilesexceeded", function(file) {
                    this.removeAllFiles();
                    this.addFile(file);
                });
            },
            error: function (response, respAjax) {
                let mainGallery = $('#document-dropzone');
                let alert = $(mainGallery).siblings(".alert");
                $(alert).find(".infoError").text('Тип файла не изображение');
                $(alert).addClass("d-block");
            }
        }
        Dropzone.prototype.defaultOptions.dictDefaultMessage = "\n" +
            "Перетащите изображение сюда, чтобы загрузить";
        Dropzone.prototype.defaultOptions.dictRemoveFile = "Удалить файл";
    </script>
    <script src="{{asset('/js/admin/brs/bList.js')}}"></script>
@endpush
