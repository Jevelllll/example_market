<form method="POST" id="form_pro">
    <input type="hidden" name="pro_id" value="{!! $product->id !!}">
    <input type="hidden" name="cart" value="add">
    <button type="button" id="add-cart" class="button-add-cart w-100 purchase purple-gr" data-purchase="{!! $product->id !!}">
        <i class="fa fa-shopping-cart"></i>
        ADD TO CARD
    </button>
    <button type="button" id="remove-cart" class="button-add-cart w-100 purchase already-shoping-cart purple-gr"
            remove-id="{!! $product->id !!}">
        <i class="fa fa-check"></i>
        Already In Your Shopping Cart!
    </button>
</form>
