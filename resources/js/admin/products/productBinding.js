
$(document).ready(function () {
    let mySelect = document.getElementById('my-select');
    $(mySelect).multiSelect();

    function initializeBindingData() {

        let bindingData = document.getElementById('bindingData');
        if (bindingData) {
            let textBindingText = bindingData.innerText;
            if (textBindingText) {
                textBindingText = textBindingText.trim()
                if (textBindingText.length > 2) {
                    let bindingDecode = [];
                    try {
                        bindingDecode = JSON.parse(textBindingText);
                    } catch (e) {
                    }
                    if (bindingDecode.length > 0) {
                        bindingDecode.map(it => console.log(it.parent_id));
                        setTimeout(() => {
                            $(mySelect).multiSelect('select', bindingDecode.map((it) => it.parent_id.toString()).filter(it => it));
                        }, 0);


                        console.log(bindingDecode);
                    }
                }
            }
        }

    }

    function saveBinding() {
        initializeBindingData();
        let addBindingForm = document.getElementById('bindingForm');
        addBindingForm.addEventListener('submit', function (e) {
            e.preventDefault();
            let serialize = $(this).serialize();
            if (serialize) {

                $.ajax({
                    method: e.currentTarget.method,
                    url: e.currentTarget.action,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: serialize,
                    beforeSend: function () {
                    },
                    success: function (data) {
                        swal({
                            title: 'Успех!',
                            text: 'Запись успешно добавлена!',
                            type: "success",
                            confirmButtonText: "OK",
                            showCancelButton: false
                        }).then((result) => {
                            console.log(result);
                        });
                    },
                    error: function (data) {

                    }
                });
            }
        });
    }

    saveBinding();
})
