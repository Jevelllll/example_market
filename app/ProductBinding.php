<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductBinding extends Model
{
    protected $table = "product_bindings";
    protected $fillable = ['product_id', 'parent_id'];
    public  function getTableName() {
        return $this->table;
    }
    public function prodbind()
    {
        return $this->hasOne('App\Products', 'id', 'parent_id');
    }
}
