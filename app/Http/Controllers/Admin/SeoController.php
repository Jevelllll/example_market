<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\ProductCategory;
use App\Products;
use App\Seo;
use App\Subcategories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class SeoController extends Controller
{
    public function index()
    {
        $seo = Seo::orderBy('id', 'desc')->get();
        if (count(collect($seo)->toArray()) > 0) {
            if ($seo[0] instanceof Seo) {
                $seoTitle = collect($seo)[0]->toArray();
                $seoTitle = array_keys($seoTitle);
                return view('admin.seo.list', compact('seo', 'seoTitle'))->withTitle('SEO список');
            } else {
                dd(503);
            }
        } else {
            $data = ["seoTitle" => null, "seo" => null];
            return view('admin.seo.list', $data)
                ->withTitle('SEO список');
        }
    }

    public function product()
    {
        $idS = [];
        $isCreate = true;
        $seo = new Seo();
        $seoPatterns = Seo::select('product_id')->whereNotNull('product_id')->get()->toArray();
        foreach ($seoPatterns as $seoPattern) {
            $idS[] = $seoPattern['product_id'];
        }
        $products = Products::whereNotIn('id', $idS)->pluck('name', 'id');
        $products[0] = 'Выбрать товар';
        return view('admin.seo.product', compact('seo', 'products', 'isCreate'))->withTitle('SEO Товар');
    }

    //product_category
    public function brand()
    {
        $idS = [];
        $isCreate = true;
        $seo = new Seo();
        $seoPatterns = Seo::select('product_category_id')->whereNotNull('product_category_id')->get()->toArray();
        foreach ($seoPatterns as $seoPattern) {
            $idS[] = $seoPattern['product_category_id'];
        }
        $category = ProductCategory::whereNotIn('id', $idS)->pluck('category_name', 'id');
        $category[0] = 'Выбрать бренд';
        return view('admin.seo.brand', compact('seo', 'category', 'isCreate'))->withTitle('SEO Бренд');
    }

    public function subcategory()
    {
        $idS = [];
        $isCreate = true;
        $seo = new Seo();
        $seoPatterns = Seo::select('subcategory_id')->whereNotNull('subcategory_id')->get()->toArray();
        foreach ($seoPatterns as $seoPattern) {
            $idS[] = $seoPattern['subcategory_id'];
        }
        $subcategory = Subcategories::whereNotIn('id', $idS)->pluck('name', 'id');
        $subcategory[0] = 'Выбрать подкатегорию';
        return view('admin.seo.subcategory', compact('subcategory', 'seo', 'isCreate'))->withTitle('SEO Подкатегория');
    }

    public function default()
    {
        $seo = Seo::whereNull('product_id')->whereNull('subcategory_id')->whereNull('product_category_id')->first();
        if ($seo){
            $isCreate = false;
            return view('admin.seo.default', compact('isCreate', 'seo'))->withTitle('SEO Общие');
        } else {
            $seo = new Seo();
            $isCreate = true;
            return view('admin.seo.default', compact('isCreate', 'seo'))->withTitle('SEO Общие');
        }
    }

    public function save(Request $request)
    {

        $seo = new Seo();
        $dataAll = $request->all();
        $errorClass = new \stdClass();
        $errorClass->errors = new \stdClass();

        $errorList = new \stdClass();
        if (isset($_POST['product_id'])) {
            $errorList->product_id = ['Поле "Товар" обязательно'];
            $errorClass->errors = $errorList;
            $product_id = $dataAll['product_id'];
            $productM= Products::where('id', $product_id)->first();
            if ($productM instanceof Products) {
                $seo->meta_title = $productM->name;
            }
            if ((int)$product_id === 0) return response()->json($errorClass, 422);
            $seo->product_id = $product_id;
        } else if (isset($_POST['subcategory_id'])) {
            $errorList->subcategory_id = ['Поле "Подкатегория" обязательно'];
            $errorClass->errors = $errorList;
            $subcategory_id = $dataAll['subcategory_id'];
            $subcategoryM = Subcategories::where('id', $subcategory_id)->first();
            if ($subcategoryM instanceof Subcategories){
                $seo->meta_title = $subcategoryM->name;
            }
            if ((int)$subcategory_id === 0) return response()->json($errorClass, 422);
            $seo->subcategory_id = $subcategory_id;
        } else if (isset($_POST['product_category_id'])) {
            $errorList->product_category_id = ['Поле "Бренд" обязательно'];
            $errorClass->errors = $errorList;
            $product_category_id = $dataAll['product_category_id'];
            $brand = ProductCategory::where('id', $product_category_id)->first();
            if ($brand instanceof ProductCategory){
                $seo->meta_title = $brand->category_name;
            }
            if ((int)$product_category_id === 0) return response()->json($errorClass, 422);
            $seo->product_category_id = $product_category_id;
        }
        if (isset($dataAll['meta_title']) && !empty($dataAll['meta_title'])){
            $seo->meta_title = $dataAll['meta_title'];
        }

        if ($this->is_base64_encoded($dataAll['meta_desc'])){
            $seo->meta_desc = $dataAll['meta_desc'];
        } else {
            $seo->meta_desc = base64_encode($dataAll['meta_desc']);
        }

        if ($this->is_base64_encoded($dataAll['meta_keywords'])){
            $seo->meta_keywords = $dataAll['meta_keywords'];
        } else {
            $seo->meta_keywords = base64_encode($dataAll['meta_keywords']);
        }

        if ($seo->save()) {
            return response()->json(['status' => 'success'], 200);
        }
        return response()->json(['status' => 'error'], 404);
    }

    public function edit($id)
    {
        $isCreate = false;
        $seo = Seo::where('id', $id)->first();
        $products = Products::pluck('name', 'id');

        $category = ProductCategory::where('id', $seo->product_category_id)->pluck('category_name', 'id');
        $subcategory = Subcategories::where('id', $seo->subcategory_id)->pluck('name', 'id');
        $subcategory[0] = 'Выбрать подкатегорию';
        $products[0] = 'Выбрать товар';
        $category[0] = 'Выбрать бренд';
        $view = $this->checkType(collect($seo)->toArray());
        if ($seo instanceof Seo) {
            return view($view, compact('seo', 'products', 'isCreate', 'category', 'subcategory'))->withTitle('SEO');
        } else {
            dd(404);
        }

    }

    private function checkType($data)
    {
        $view = 'admin.seo.default';
        unset($data['id']);
        unset($data['meta_title']);
        unset($data['meta_keywords']);
        unset($data['meta_desc']);
        unset($data['created_at']);
        unset($data['updated_at']);
        $detect = new \stdClass();
        foreach ($data as $key => $datum) {
            if ((int)$datum > 0) {
                $detect->$key = true;
            }
        }
        if (isset($detect->product_id)) {
            $view = 'admin.seo.product';
        } else if (isset($detect->subcategory_id)) {
            $view = 'admin.seo.subcategory';
        } else if (isset($detect->product_category_id)) {
            $view = 'admin.seo.brand';
        } else {
            $view = 'admin.seo.default';
        }
        return $view;
    }

    public function update(Request $request, $id)
    {
        $model = Seo::where('id', (int)$id)->first();
        $letPreUpdate = [];
        $requestAll = $request->all();
        foreach ($requestAll as $key => $val) {
            if ($key === 'product_id' || $key === 'id' || $key === 'subcategory_id' || $key === 'product_category_id') {
                $letPreUpdate[$key] = $val;
            } else {
                if ($this->is_base64_encoded($val)){
                    $letPreUpdate[$key] = $val;
                } else {
                    $letPreUpdate[$key] = base64_encode($val);
                }
            }
        }

        if ($model instanceof Seo) {
            if ($model->update($letPreUpdate)) {
                return response()->json(['status' => 'success'], 200);
            } else {
                return response()->json(['status' => 'error'], 404);
            }
        }
        return response()->json(['status' => 'error'], 404);
    }

    public function delete(Request $request)
    {
        $id = $request->post('id');
        $model = Seo::where('id', (int)$id)->first();
        if ($model instanceof Seo) {
            if ($model->delete()) {
                return response()->json(['status' => 'success'], 200);
            } else {
                return response()->json(['status' => 'error'], 404);
            }
        }
        return response()->json(['status' => 'error'], 404);
    }

    function is_base64_encoded($data)
    {
        if (base64_encode(base64_decode($data)) === $data) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
}
