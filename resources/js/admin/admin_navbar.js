document.addEventListener('DOMContentLoaded', function () {
    let myContainerList = document.querySelector('.container-list');
    let myBadgeOrder = document.getElementById('badgeOrder');
    let getOrderRoute = document.querySelector(".navbar[route_get-new-order]")
        .getAttribute('route_get-new-order');
    let findNewOrder = () => {
        let httpClient = new XMLHttpRequest();
        httpClient.open('POST', getOrderRoute, true);
        httpClient.setRequestHeader(
            'X-CSRF-TOKEN',
            document.querySelector("meta[name='csrf-token']").getAttribute('content'));
        httpClient.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        httpClient.setRequestHeader("X-Requested-With", "XMLHttpRequest");
        httpClient.responseType = 'json';
        httpClient.send();
        httpClient.onload = () => {
            if (httpClient.status === 200) {
                let response = httpClient.response;

                let htmlContainerList = document.createElement('div');
                htmlContainerList.innerHTML = response.navbars_alerts;
                let customContainerList = htmlContainerList.querySelector('.container-list').querySelectorAll('.dropdown-item');
                let customBadgeOrder = htmlContainerList.querySelector('#badgeOrder').innerText;
                myContainerList.innerHTML = "";
                myBadgeOrder.innerText = customBadgeOrder;
                customContainerList.forEach((el) => {
                    myContainerList.appendChild(el);
                })
            }
        };
        httpClient.onerror = () => {
        }
    };
    findNewOrder();
    // setInterval(() => findNewOrder(), 5000);

});
