<?php


namespace App\Facades;


use Illuminate\Support\Facades\Facade;

class CategoryListFacade extends Facade
{

    protected static function getFacadeAccessor()
    {
        return 'categoryList';
    }
}
