<?php

namespace App\Http\Requests\admin\validate\banners;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreBannersUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'min:7',
            'desc' => 'min:7',
            'redirect_url' => 'min:7',
            'gallery' => 'max:10000'
        ];
    }
    public function messages()
    {
        return [
            'name.min' => 'Минимальный размер поля "Название" 7 символов',
            'desc.min' => 'Минимальный размер поля "Описание" 7 символов',
            'redirect_url.min' => 'Минимальный размер поля "Переадресации" 7 символов',
        ];
    }

    public function  attributes()
    {
        return [
            'name' => 'category title'
        ];
    }
    public function filters()
    {
        return [
            'name' => 'trim|capitalize|escape'
        ];
    }

}
