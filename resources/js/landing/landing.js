$(document).ready(function () {
    var swiper = new Swiper('.swiper-container', {
        spaceBetween: 1,
        effect: 'fade',
        loop: true,
        lazy: true,
        // preloadImages: false,
        // simulateTouch:false,
        // scrollContainer : true,
        autoplay: {
            delay: 2500,
            disableOnInteraction: false,
        },
        direction: 'vertical',
        pagination: {

            el: '.swiper-pagination',
            clickable: true,
        },
        breakpoints: {
            540: {
                direction: 'horizontal',
            },
        } ,   on: {
            init: function () {
                $('.card-container').css('opacity', '1');
            },
        },
    });
    // card-container
    var startScroll, touchStart, touchCurrent;
    // swiper.slides.on('touchstart', function (e) {
    //     startScroll = this.scrollTop;
    //     touchStart = e.targetTouches[0].pageY;
    // }, false);
    // swiper.slides.on('touchmove', function (e) {
    //     touchCurrent = e.targetTouches[0].pageY;
    //     var touchesDiff = touchCurrent - touchStart;
    //     var slide = this;
    //     var onlyScrolling =
    //         ( slide.scrollHeight > slide.offsetHeight ) && //allow only when slide is scrollable
    //         (
    //             ( touchesDiff < 0 && startScroll === 0 ) || //start from top edge to scroll bottom
    //             ( touchesDiff > 0 && startScroll === ( slide.scrollHeight - slide.offsetHeight ) ) || //start from bottom edge to scroll top
    //             ( startScroll > 0 && startScroll < ( slide.scrollHeight - slide.offsetHeight ) ) //start from the middle
    //         );
    //     if (onlyScrolling) {
    //         e.stopPropagation();
    //     }
    // }, false);

    function hoverParentCategory() {
        $('.cat_box__item').hover(function () {
            let nameBlock = $(this).find('.name_block');
            if ($(nameBlock).hasClass('dark')) {
            } else {
                nameBlock.addClass('dark');
            }
        }, function () {
                $(this).find('.dark').removeClass('dark');
        })
    }

    hoverParentCategory();
});
