@extends('admin.layouts.admin_template')
@push('style')
    <link href="{{asset('/js/admin/lou-multi-select/css/multi-select.css')}}" rel="stylesheet">
    <link href="{{asset('/css/admin/productBinding.css')}}" rel="stylesheet">
@endpush
@section('content')
    @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
    @else
        <div id="bindingData">
            @if(isset($dataProduct->bindings) && !empty($dataProduct->bindings))
                @if(count($dataProduct->bindings) > 0)
                    {{json_encode($dataProduct->bindings)}}
                @endif
            @endif
        </div>
        <div class="card shadow mb-4 pb-5 pt-5">
            <h1 class="h3 mb-4 text-gray-800 text-center pt-3">
                <i class="fa fa-caret-down" aria-hidden="true"></i>
                Связать товар <strong>{{$dataProduct->name}}</strong> <i
                    class="fa fa-caret-down" aria-hidden="true"></i></h1>
            <form id="bindingForm" method="POST" action="{{route('save.product-binding')}}">
                <div class="row justify-content-center">
                    <input type="hidden" name="product_id" value="{{$dataProduct->id}}">
                    <select multiple="multiple" id="my-select" name="product_parent_id[]">
                        @foreach($productOther as $product)
                            <option value='{{$product->id}}'>{{$product->name}}</option>
                        @endforeach
                    </select>
                    <button type="submit" class="btn btn-success w-75" id="addBinding">Сохранить</button>
                </div>
            </form>
        </div>
    @endif
@endsection
@push('scripts')
    <script src="{{asset('/js/admin/lou-multi-select/js/jquery.multi-select.js')}}"></script>
    <script src="{{asset('/js/admin/products/productBinding.js')}}"></script>
@endpush
