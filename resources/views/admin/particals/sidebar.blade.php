<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
{{--<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark toggled" id="accordionSidebar">--}}

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/admin">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Initiales Admin <sup></sup></div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item">
        <a class="nav-link" href="/admin">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Панель приборов</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Интерфейсы
    </div>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseBanners" aria-expanded="true" aria-controls="collapseBanners">
            <i class="fas fa-fw fa-cog"></i>
            <span>Баннеры</span>
        </a>
        <div id="collapseBanners" class="collapse" aria-labelledby="collapseBanners" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Дейстивия:</h6>
                <a class="collapse-item" href="/admin/banner/list">Вывести список</a>
                <a class="collapse-item" href="/admin/banner/create">Создать новый</a>
                <a class="collapse-item" href="/admin/banner/quick-offer-create">Рекламное предложение</a>
            </div>
        </div>
    </li>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseBrands" aria-expanded="true" aria-controls="collapseBrands">
            <i class="fas fa-fw fa-cog"></i>
            <span>Бренды</span>
        </a>
        <div id="collapseBrands" class="collapse" aria-labelledby="collapseBrands" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Дейстивия:</h6>
                <a class="collapse-item" href="/admin/brand/list">Список брендов</a>
                <a class="collapse-item" href="{{route('new.brand')}}" >Добавить бренд</a>
            </div>
        </div>
    </li>    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseSub" aria-expanded="true" aria-controls="collapseSub">
            <i class="fas fa-fw fa-cog"></i>
            <span>Подкатегории</span>
        </a>
        <div id="collapseSub" class="collapse" aria-labelledby="collapseSub" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Дейстивия:</h6>
                <a class="collapse-item" href="{{route('list.subcategory')}}">Список подкатегорий</a>
                <a class="collapse-item" href="{{route('create.subcategory')}}">Добавить подкатегорию</a>
            </div>
        </div>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseParams" aria-expanded="true" aria-controls="collapseParams">
            <i class="fas fa-fw fa-cog"></i>
            <span>Товары</span>
        </a>
        <div id="collapseParams" class="collapse" aria-labelledby="collapseParams" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Дейстивия:</h6>
                <a class="collapse-item" href="{{route('list.product')}}">Список товаров</a>
                <a class="collapse-item" href="{{route('new.product')}}">Добавить товар</a>
            </div>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseSeo" aria-expanded="true" aria-controls="collapseSeo">
            <i class="fas fa-fw fa-cog"></i>
            <span>SEO оптимизация</span>
        </a>
        <div id="collapseSeo" class="collapse" aria-labelledby="collapseSeo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Дейстивия:</h6>
                <a class="collapse-item" href="{{route('seo.list')}}">Список</a>
                <a class="collapse-item" href="{{route('seo.default')}}">По умолчанию</a>
                <a class="collapse-item" href="{{route('seo.brand')}}">Бренды</a>
                <a class="collapse-item" href="{{route('seo.subcategory')}}">Подкатегории</a>
                <a class="collapse-item" href="{{route('seo.product')}}">Товары</a>
            </div>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseInfo" aria-expanded="true" aria-controls="collapseInfo">
            <i class="fas fa-fw fa-cog"></i>
            <span>Инфо</span>
        </a>
        <div id="collapseInfo" class="collapse" aria-labelledby="collapseInfo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Дейстивия:</h6>
                <a class="collapse-item" href="{{route('rfu.list')}}">Список</a>
            </div>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseCountryPay" aria-expanded="true" aria-controls="collapseCountryPay">
            <i class="fas fa-fw fa-cog"></i>
            <span>Страны PAYPAL</span>
        </a>
        <div id="collapseCountryPay" class="collapse" aria-labelledby="collapseCountryPay" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Дейстивия:</h6>
                <a class="collapse-item" href="{{route('new.country')}}">Добавить страну</a>
                <a class="collapse-item" href="{{route('country.list')}}">Страны</a>
                <a class="collapse-item" href="{{route('state.list')}}">Регионы</a>
                <a class="collapse-item" href="{{route('city.list')}}">Города</a>
            </div>
        </div>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePay" aria-expanded="true" aria-controls="collapsePay">
            <i class="fas fa-fw fa-cog"></i>
            <span>Заказы</span>
        </a>
        <div id="collapsePay" class="collapse" aria-labelledby="collapseInfo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Дейстивия:</h6>
                <a class="collapse-item" href="{{route('admin.order.list')}}">Список заказов</a>
                <a class="collapse-item" href="{{route('admin.transactions.list')}}">Список платежей</a>
            </div>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseAnswer" aria-expanded="true" aria-controls="ollapseAnswer">
            <i class="fas fa-fw fa-cog"></i>
            <span>Сообщения</span>
        </a>
        <div id="collapseAnswer" class="collapse" aria-labelledby="collapseInfo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Дейстивия:</h6>
                <a class="collapse-item" href="{{route('admin.answer.list')}}">Список сообщений</a>
            </div>
        </div>
    </li>
    <!-- Nav Item - Pages Collapse Menu -->
{{--    <li class="nav-item">--}}
{{--        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">--}}
{{--            <i class="fas fa-fw fa-cog"></i>--}}
{{--            <span>Components</span>--}}
{{--        </a>--}}
{{--        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">--}}
{{--            <div class="bg-white py-2 collapse-inner rounded">--}}
{{--                <h6 class="collapse-header">Custom Components:</h6>--}}
{{--                <a class="collapse-item" href="buttons.html">Buttons</a>--}}
{{--                <a class="collapse-item" href="cards.html">Cards</a>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </li>--}}

    <!-- Nav Item - Utilities Collapse Menu -->
{{--    <li class="nav-item">--}}
{{--        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">--}}
{{--            <i class="fas fa-fw fa-wrench"></i>--}}
{{--            <span>Utilities</span>--}}
{{--        </a>--}}
{{--        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">--}}
{{--            <div class="bg-white py-2 collapse-inner rounded">--}}
{{--                <h6 class="collapse-header">Custom Utilities:</h6>--}}
{{--                <a class="collapse-item" href="/utilities-color.html">Colors</a>--}}
{{--                <a class="collapse-item" href="utilities-border.html">Borders</a>--}}
{{--                <a class="collapse-item" href="utilities-animation.html">Animations</a>--}}
{{--                <a class="collapse-item" href="utilities-other.html">Other</a>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </li>--}}

    <!-- Divider -->
    <hr class="sidebar-divider">

{{--    <!-- Heading -->--}}
{{--    <div class="sidebar-heading">--}}
{{--        Addons--}}
{{--    </div>--}}

{{--    <!-- Nav Item - Pages Collapse Menu -->--}}
{{--    <li class="nav-item active">--}}
{{--        <a class="nav-link" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">--}}
{{--            <i class="fas fa-fw fa-folder"></i>--}}
{{--            <span>Pages</span>--}}
{{--        </a>--}}
{{--        <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">--}}
{{--            <div class="bg-white py-2 collapse-inner rounded">--}}
{{--                <h6 class="collapse-header">Login Screens:</h6>--}}
{{--                <a class="collapse-item" href="login.html">Login</a>--}}
{{--                <a class="collapse-item" href="register.html">Register</a>--}}
{{--                <a class="collapse-item" href="forgot-password.html">Forgot Password</a>--}}
{{--                <div class="collapse-divider"></div>--}}
{{--                <h6 class="collapse-header">Other Pages:</h6>--}}
{{--                <a class="collapse-item" href="404.html">404 Page</a>--}}
{{--                <a class="collapse-item active" href="blank.html">Blank Page</a>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </li>--}}

{{--    <!-- Nav Item - Charts -->--}}
{{--    <li class="nav-item">--}}
{{--        <a class="nav-link" href="charts.html">--}}
{{--            <i class="fas fa-fw fa-chart-area"></i>--}}
{{--            <span>Charts</span></a>--}}
{{--    </li>--}}

{{--    <!-- Nav Item - Tables -->--}}
{{--    <li class="nav-item">--}}
{{--        <a class="nav-link" href="tables.html">--}}
{{--            <i class="fas fa-fw fa-table"></i>--}}
{{--            <span>Tables</span></a>--}}
{{--    </li>--}}

    <!-- Divider -->
{{--    <hr class="sidebar-divider d-none d-md-block">--}}

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
