<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ProductCategory
 * @package App
 * @property string category_name
 * @property string img
 * @property string seo_name
 * @property string down_desc
 * @property string created_at
 * @property string updated_at
 */
class ProductCategory extends Model
{
    protected $table = "product_category";
    public static $TABLE = "product_category";
    protected $fillable = ['category_name', 'img', 'seo_name', 'down_desc', 'created_at', 'updated_at'];
    public function subcategory()
    {
        return $this->hasMany('App\Subcategories', 'category_id', 'id');
    }
    public function productfc()
    {
        return $this->hasMany('App\Products', 'category_id', 'id')->orderBy('created_at', 'DESC');
    }

}
