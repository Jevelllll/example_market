<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Orders;
use App\Transactions;

class OrderController extends Controller
{
    public function index()
    {
        $orders = Orders::select('id', 'is_order_read', 'total_sum', 'created_at', 'order_nr')->get();
        if (count(collect($orders)->toArray()) > 0) {
            if ($orders[0] instanceof Orders) {
                $ordersTitle = collect($orders)[0]->toArray();
                $ordersTitle = array_keys($ordersTitle);
                return view('admin.order.index', compact('orders', 'ordersTitle'))->withTitle('Список заказов');
            } else {
                dd(503);
            }
        } else {
            $data = ["ordersTitle" => null, "orders" => null];
            return view('admin.order.index', $data)
                ->withTitle('Список заказов');
        }

    }

    public function show($id)
    {
        $order = Orders::where('id', $id)->select('id', 'order_nr', 'total_sum', 'created_at', 'status')
            ->with(['order_product', 'customer' => function ($qw) {
                return $qw->select(
                    'order_id', 'first_name', 'last_name', 'address_country',
                    'address_state', 'address_city', 'address_street', 'address_house_number',
                    'address_zip_code', 'email', 'phone', 'comment')->get();
            }])->orderBy('created_at', 'desc')->first();

        $orderProducts = $order->order_product;
        $customer = $order->customer;
        unset($order->order_product);
        unset($order->customer);
        unset($order->id);
        unset($customer->order_id);
        Orders::readNewOrder($id);
        $transactions = Transactions::where('invoice', 'like', '%-' . $order->order_nr)
            ->orderBy('id', 'desc')->get();
        return view('admin.order.show', compact('order', 'transactions', 'orderProducts', 'customer'))
            ->withTitle('Заказ ' . $order->order_nr);
    }

}
