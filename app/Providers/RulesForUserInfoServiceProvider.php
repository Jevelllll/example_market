<?php

namespace App\Providers;

use App\Helper\UserInfoBlock;
use Illuminate\Support\ServiceProvider;

class RulesForUserInfoServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('rulesUsers', function (){
            return new UserInfoBlock();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
