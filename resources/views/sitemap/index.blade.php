<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>

<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    @if($brand instanceof \App\ProductCategory)
        <sitemap>
            <loc>{{ route('sitemap.brand') }}</loc>
            <lastmod>{{ $brand->updated_at->toAtomString() }}</lastmod>
        </sitemap>
    @endif
    @if($subCat instanceof \App\Subcategories)
        <sitemap>
            <loc>{{ route('sitemap.subCat') }}</loc>
            <lastmod>{{ $subCat->updated_at->toAtomString() }}</lastmod>
        </sitemap>
    @endif
    @if($product instanceof \App\Products)
        <sitemap>
            <loc>{{ route('sitemap.product') }}</loc>
            <lastmod>{{ $product->updated_at->toAtomString() }}</lastmod>
        </sitemap>
    @endif
</sitemapindex>
