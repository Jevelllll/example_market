<a id="seoList" class="d-none" href="{{route('seo.list')}}"></a>
@if(!empty($isMetatitile))
    <div class="form-group">
        {{Form::label('meta_title', 'Meta Title', ['for' => 'meta_title'])}}
        {{Form::text('meta_title',  null, ['id' => 'meta_title', 'class' => 'form-control'])}}
        <div class="alert alert-danger alert-dismissible d-none">
            <button type="button" class="close">&times;</button>
            <strong class="infoError"></strong>
        </div>
        <button class="btn btn-info mt-1 seoInfo">Инфо ? (показать)</button>
        <div class="d-none">
            <p>Требования к Title:
                содержит до 70 символов или до 600 пикселей для Google — эта система измеряет длину именно в них;
                ключевые слова размещаются ближе к началу Title — в первых 1-4 словах;
                не содержит ключевых слов, по которым страница не продвигается — например, в Title сайта
                интернет-магазина не стоит писать «бесплатная доставка»;
                компания указывается в конце — исключение составляют крупные бренды, например, Rozetka, Gucci и т. д.;
                ключевые слова не дублируются — при необходимости используйте синонимы;
                Title должен отличаться от H1, заголовка первого порядка;
                не содержит перечисление всех возможных регионов и всего ассортимента;
                написан без использования капслока;
                текст читабельный, без несогласованных фраз — не стоит просто перечислять ключевые слова;
                желательно использовать один язык для одной страницы, как советует Google;
                не рекомендуется использовать разные теги Title для мобильных и десктопных страниц.
            </p>
            <p>Плохой пример Title:
                Компьютеры. Цены в г. Николаев. Купить компьютеры в г. Николаеве.
            </p>
            <p>
                Хороший пример Title:
                Компьютере в Николаеве. Купить компьютер по цене поставщика | Мир техники</p>
        </div>
    </div>
@endif
<div class="form-group">
    @if(!isset($seo->meta_desc))
        {{Form::label('meta_desc', 'Meta Description', ['for' => 'meta_desc'])}}
        {{Form::text('meta_desc',  null, ['id' => 'meta_desc', 'class' => 'form-control'])}}
    @else
        @if (base64_encode(base64_decode($seo->meta_desc)) === $seo->meta_desc)
            {{Form::label('meta_desc', 'Meta Description', ['for' => 'meta_desc'])}}
            {{Form::text('meta_desc',  base64_decode($seo->meta_desc), ['id' => 'meta_desc', 'class' => 'form-control'])}}
        @else
            {{Form::label('meta_desc', 'Meta Description', ['for' => 'meta_desc'])}}
            {{Form::text('meta_desc',  $seo->meta_desc, ['id' => 'meta_desc', 'class' => 'form-control'])}}
        @endif
    @endif
    <div class="alert alert-danger alert-dismissible d-none">
        <button type="button" class="close">&times;</button>
        <strong class="infoError"></strong>
    </div>
    <button class="btn btn-info mt-1 seoInfo">Инфо ? (показать)</button>
    <div class="d-none">
        <p>Требования к Description:
            мета-тег содержит от 70 до 155 знаков, для Google от 400 до 930 пикселей;
            начинается с ключевого слова;
            состоит из связного текста;
            не дублирует Title;
            описывает конкурентное преимущество, УТП — у пользователя должно возникнуть желание перейти на сайт;
            содержит призыв к действию;
            заканчивается точкой или восклицательным знаком;
            не слишком короткий и содержит достаточно ключевых слов;
            не дублирует кусок контента с сайта;
            не содержит спецсимволы (= / \ + _ в середине текста, зато можно использовать различные графические значки в
            конце объявления, чтобы привлечь внимание пользователей.
        </p>

        <p>Плохой пример:

            Перфораторы аккумуляторные/ продажа + доставка, доступные цены. Это залог успеха для ремонта и строительства
            в квартире или загородном доме.</p>

        <p>Хороший пример:

            Аккумуляторные перфораторы со скидкой до 50%! Бесплатная доставка по Одессе. 3 года гарантии от
            производителя. Звоните ☎ (099) 222-22-22</p>
    </div>
</div>

<div class="form-group">
    @if(!isset($seo->meta_keywords))
        {{Form::label('meta_keywords', 'Meta Keywords', ['for' => 'meta_keywords'])}}
        {{Form::text('meta_keywords',  null, ['id' => 'meta_keywords', 'class' => 'form-control'])}}
    @else
        @if (base64_encode(base64_decode($seo->meta_keywords)) === $seo->meta_keywords)
            {{Form::label('meta_keywords', 'Meta Keywords', ['for' => 'meta_keywords'])}}
            {{Form::text('meta_keywords',  base64_decode($seo->meta_keywords), ['id' => 'meta_desc', 'class' => 'form-control'])}}
        @else
            {{Form::label('meta_keywords', 'Meta Keywords', ['for' => 'meta_keywords'])}}
            {{Form::text('meta_keywords',  $seo->meta_keywords, ['id' => 'meta_keywords', 'class' => 'form-control'])}}
        @endif
    @endif
    <div class="alert alert-danger alert-dismissible d-none">
        <button type="button" class="close">&times;</button>
        <strong class="infoError"></strong>
    </div>
    <button class="btn btn-info mt-1 seoInfo">Инфо ? (показать)</button>
    <div class="d-none">
        <p>Рекомендации по заполнению Keywords
            Если вы все же решите прописать Keywords для Яндекса, то придерживайтесь следующих принципов:
            не указывайте больше 20 слов;
            словосочетания должны быть релевантны содержимому страницы и встречаться в ее тексте;
            не используйте одно и то же ключевое слово в разных словосочетаниях, это можно делать максимум два раза —
            дробите ключи, как на примере ниже.
        </p>
        <p>Неправильно
            обувь детская, продажа детской обуви, купить детскую обувь, цена детской обуви.
        </p>
        <p>
            Правильно
            обувь, детская, купить, цена, продажа.
        </p>
    </div>
</div>
