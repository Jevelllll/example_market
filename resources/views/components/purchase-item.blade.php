@if(count($productsList))
{{--    @dd($productsList)--}}
@foreach($productsList as $product)
    <div class="cart-purchase mb-2">
        <div class="cart-remove" remove-id="{!! $product->id !!}">&times;</div>
        <div class="cart-purchase-item">

            <div class="purchase-img-wrap">
                <a href="<?=route('client-product', ['seoName' => $product->seo_name]) ?>" class="purchase-img responsive-img pl-3">
                    @if(isset($product->pictures[0]->image_list))
                        <img class="rounded img-thumbnail hover-purple" src="{!!asset('/images/product/pictures/thumbnails').'/'.$product->pictures[0]->image_list!!}"
                             alt="{{$product->name}}">
                    @else
                        <img class="rounded img-thumbnail hover-purple" src="{!!asset('/images/product/no_foto.png') !!}">
                    @endif

                </a>
            </div>
            <div class="purchase-inner">
                <div class="purchase-title-wrap col-9">
                    <a href="<?=route('client-product', ['seoName' => $product->seo_name]) ?>"
                       class="purchase-title">{{$product->name}}</a>
                </div>
                <div class="purchase-item-flex col">
                    <div class="purchase-coast">{{$product->price}}&nbsp;USD</div>
                </div>
            </div>

        </div>
    </div>
@endforeach
@else
    <div>
        <img src="{!!asset('/images/product/no_foto.png') !!}">
    </div>
@endif
