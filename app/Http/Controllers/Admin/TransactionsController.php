<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Transactions;

class TransactionsController extends Controller
{
    public function index()
    {
        $transactions = Transactions::select(
            'id', 'invoice', 'payer_id', 'is_read',
            'payment_date', 'payment_status', 'address_zip', 'address_name', 'address_country', 'address_city', 'address_street', 'payer_email')
            ->whereNotNull('invoice')
            ->get();
        if (count(collect($transactions)->toArray()) > 0) {
            if ($transactions[0] instanceof Transactions) {
                $transactionsTitle = collect($transactions)[0]->toArray();
                $transactionsTitle = array_keys($transactionsTitle);
                return view('admin.transactions.index', compact('transactions', 'transactionsTitle'))
                    ->withTitle('Список платежей');
            } else {
                dd(503);
            }
        } else {
            $data = ["transactionsTitle " => null, "transactions" => null];
            return view('admin.transactions.index', $data)
                ->withTitle('Список платежей');
        }
    }

    public function show($id)
    {
        Transactions::updateRead($id);
        $transaction = Transactions::where('id', $id)->orderBy('id', 'DESC')->first();
        $order_id = $transaction->order_id;
        unset($transaction->order_id);
        $transactionTitle = array_keys(collect($transaction)->toArray());

        return view('admin.transactions.show', compact('transaction', 'transactionTitle', 'order_id'))->withTitle('Платеж');
    }

}
