<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string id
 * @property string name_ru
 * @property string desc
 * @property integer sold
 */
class PaymentStatuses extends Model
{
    public $incrementing = false;
    protected $table = 'payment_statuses';
    protected $fillable = [
        "id", "name_ru", "desc", "sold"
    ];
    public $timestamps = false;
}
