@extends('admin.layouts.admin_template')
@section('content')
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary pt-3">Список баннеров</h6>
        </div>
        @if($banners)
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        @foreach($bannersTitle as $titles)
                            @if($titles === 'gallery')
                                <th class="image_thumbnails">{{Translators::translateRow()[$titles]}}</th>
                            @else
                                <th>{{Translators::translateRow()[$titles]}}</th>
                            @endif
                        @endforeach
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        @foreach($bannersTitle as $key => $titles)
                            @if($titles === 'gallery')
                                <th class="image_thumbnails">{{Translators::translateRow()[$titles]}}</th>
                            @else
                                <th>{{Translators::translateRow()[$titles]}}</th>
                            @endif
                        @endforeach
                    </tr>
                    </tfoot>
                    <tbody>
                    @foreach($banners as $banner)
                        <tr data-id="{{$banner->id}}">
                            @foreach($bannersTitle as $key => $titles)
                                @if($titles === 'gallery')

                                    @if(file_exists(public_path().'/images/banners/thumbnails/'. $banner->$titles))
                                        <td><img class="img-fluid rounded"
                                                 src="{!! asset('images/banners/thumbnails/'. $banner->$titles) !!}">
                                        </td>
                                    @else
                                        <td><img class="img-fluid rounded"
                                                 src="{!! asset('images/product/no_foto.png') !!}">
                                        </td>
                                    @endif

                                @else
                                    <td>{{$banner->$titles}}</td>
                                @endif

                            @endforeach
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        @else
            <div style="font-size: 2.5rem" class="text-center">Отсутствуют баннера</div>
        @endif
    </div>

@endsection



@push('scripts')
    <script src="{{asset('/js/admin/brs/bList.js')}}"></script>

@endpush
