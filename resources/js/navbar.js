import {paginationWork} from "./common";

(function ($, undefined) {

    let menuHeader = $('#menu-header'),
        menuWrap = $('#menu-wrap'),
        subcategory = $('.subcategory'),
        categoryS = $('.category'),
        rootWidth = menuHeader.width();

    function interactionMenu() {

        let collectCategory = [];

        $('.category>li').hover(function () {
            for (let index = 0; index < collectCategory.length; index ++) {
                collectCategory[index]
                    .removeClass('categoryLink__active')
                    .addClass('purple-gr')
            }
            collectCategory = [];
            let width = window.innerWidth || document.documentElement.clientWidth ||
                document.body.clientWidth;
            window.addEventListener("resize", function () {
                width = window.innerWidth || document.documentElement.clientWidth ||
                    document.body.clientWidth;
                subcategory.width(width - rootWidth + 'px');
                subcategory.css('left', rootWidth - 25 + 'px');
            });
            subcategory.width(width - rootWidth + 'px');
            subcategory.css('left', rootWidth - 15 + 'px');
            cleanUlSubcat();
            collectCategory.push($(this));
            $(this).addClass('categoryLink__active');
            $(this).removeClass('purple-gr');
            let currentHoverUl = $(this).find('ul');
            $(currentHoverUl).css('display', 'flex');
            $(currentHoverUl).css('height', categoryS.height() + 30 + 'px');
            let currentCategoryStyle = $(categoryS).css('margin-top');
            let categoryLink = $(categoryS).find('.categoryLink').first().css('padding-top');
            let itemTextSpan = $(this).find('.subcategory_item_text');

            $(itemTextSpan)
                .css(
                    'margin-top', `${Number.parseInt(currentCategoryStyle) - Number.parseInt(categoryLink)}.px`
                );
        }, function () {
            // $(this).removeClass('categoryLink__active');
            // $(this).addClass('purple-gr');
        });
        const animatedSpeed = 3;
        $(menuWrap).hover(
            function () {
                $(menuHeader).animate({
                    top: "4rem",
                }, animatedSpeed);
            }, function () {
            }).click(function () {
            // window.location.href = '/';
        });
        document.getElementsByClassName('global-header')[0].addEventListener('mouseleave', event => {
            $(menuHeader).animate({
                top: "-100%",
            }, animatedSpeed);
            for (let index = 0; index < collectCategory.length; index ++) {
                collectCategory[index]
                    .removeClass('categoryLink__active')
                    .addClass('purple-gr')
            }
            cleanUlSubcat();
        });

        function cleanUlSubcat() {
            $(subcategory).each(function (i, el) {
                $(el).css('display', 'none');
            })
        }
    }


    function mobileNav() {

        let parentsBrand = document.querySelectorAll('.menu-parent-btn');
        for (let i = 0; i < parentsBrand.length; ++i) {

            let parentHtmlElement = parentsBrand[i];
            if (parentHtmlElement) {
                parentHtmlElement.addEventListener('click', function () {
                    let findChild = document.querySelector(`.menu-child[data-open-child="${this.getAttribute('data-open-p')}"]`);
                    $(findChild).toggle(250);
                    $(findChild).toggleClass('d-flex');
                    // if (findChild.classList.contains('d-none')) {
                    //     findChild.classList.remove('d-none');
                    // } else {
                    //     findChild.classList.add('d-none');
                    // }
                });
            }


        }

        function clickParent(ev) {
            console.log(ev)
        }

        $(".mobile_menu_content_item").each(function (el) {
            let find = $(this).find("a.parent");
            $(find).click(clickParent);
        })
    }

    mobileNav();
    // redirectRootCategories();
    interactionMenu();
    paginationWork();

})(jQuery);
