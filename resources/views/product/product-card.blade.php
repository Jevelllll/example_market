<div class="wrapper">
    <div class="container p-0 back-product">
        @if(count($product->pictures) > 0)
            <?php
            $sortPictures = collect($product->pictures);
            $sortPictures = $sortPictures->sortBy('position_index');
            $sortPictures = $sortPictures->values()->all();
            //                dd($sortPictures[0]->image_list);
            ?>
            @include('product.rate', ['product' => $product])
            <a href="/product/{{$product->seo_name}}">
                <div class="top d-flex justify-content-center align-items-center" style="background-color: #e4e5e7;">


                    <img class="img-fluid img-thumbnail w-100"
                         src="{!!asset('/images/product/pictures/thumbnails').'/'.$sortPictures[0]->image_list!!}"
                         alt="{{$product->name}}">
                    @else
                        <img class="img-fluid img-thumbnail w-100"
                             src="{!!asset('/images/product/no_foto.png').'/'!!}" alt="{{$product->name}}">
                    @endif

                </div>
            </a>

            @if((boolean) $product->is_sold)
                <div class="color-red-back sold-out text-white w-100 disabled-user-select">
                    SORRY,
                    SOLD OUT
                    &nbsp;
                    <i class="fas fa-exclamation-circle"></i>
                </div>
            @else
                <div class="bottom">
                    <div class="left">
                        <div class="details">
                            <h2 class="h6">
                                <a title="{{$product->name}}" href="/product/{{$product->seo_name}}">
                                    {{$product->name}}
                                </a>
                            </h2>
                        </div>
                        <div class="buy purchase" data-purchase="{!! $product->id !!}"><i class="fas fa-cart-plus"></i>
                        </div>
                    </div>
                    <div class="right">
                        <div class="done"><i class="fas fa-check"></i></div>
                        <div class="details">
                            <p>Chair</p>
                            <p>Added to your cart</p>
                        </div>
                        <div class="remove"><i remove-id="{!! $product->id !!}" class="removeEl fas fa-times"></i></div>
                    </div>
                </div>
                <div class="d-price"><p>{{$product->price}} USD</p></div>
            @endif
    </div>

    @if($product->options)
        <?php
        $options = json_decode($product->options);
        ?>
        @if(count($options) > 0)
            <div class="inside">
                <div class="icon"><i class="fas fa-info-circle"></i></div>
                <div class="contents">
                    <table>
                        @foreach($options as $option)
                            <tr>
                                <th>{{$option->contentKey}}</th>
                            </tr>
                            <tr>
                                <td>{{$option->contentVal}}</td>
                            </tr>
                        @endforeach

                    </table>
                </div>
            </div>
        @endif
    @endif

</div>
