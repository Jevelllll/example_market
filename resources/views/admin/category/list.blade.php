@extends('admin.layouts.admin_template')
@section('content')
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Список брендов ("Категорий")</h6>
        </div>
        @if($categories)
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        @foreach($categoryTitle as $titles)

                            @if($titles === 'img')
{{--                                <th class="image_thumbnails">{{Translators::translateRow()[$titles]}}</th>--}}
                            @elseif($titles === 'down_desc')
                            @else
                                <th>{{Translators::translateRow()[$titles]}}</th>
                            @endif
                        @endforeach
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        @foreach($categoryTitle as $key => $titles)
                            @if($titles === 'img')
{{--                                <th class="image_thumbnails">{{Translators::translateRow()[$titles]}}</th>--}}
                            @elseif($titles === 'down_desc')
                            @else
                                <th>{{Translators::translateRow()[$titles]}}</th>
                            @endif
                        @endforeach
                    </tr>
                    <tfoot>
                    <tbody>
                    @foreach($categories as $category)
                        <tr data-id="{{$category->id}}">
                            @foreach($categoryTitle as $key => $titles)
                                @if($titles === 'img')

                                    @if(file_exists(public_path().'/images/category/thumbnails/'. $category->$titles))
{{--                                        <td><img class="img-fluid rounded"--}}
{{--                                                 src="{!! asset('images/category/thumbnails/'. $category->$titles) !!}">--}}
{{--                                        </td>--}}
                                    @else
{{--                                        <td><img class="img-fluid rounded"--}}
{{--                                                 src="{!! asset('images/product/no_foto.png') !!}">--}}
{{--                                        </td>--}}
                                    @endif
                                @elseif($titles === 'down_desc')
                                @else
                                    <td>{{$category->$titles}}</td>
                                @endif

                            @endforeach
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        @else
            <div style="font-size: 2.5rem" class="text-center">Отсутствуют бренды</div>
        @endif
    </div>
@endsection
@push('scripts')
    <script src="{{asset('/js/admin/categories/categoryList.js')}}"></script>
@endpush
