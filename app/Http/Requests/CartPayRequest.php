<?php

namespace App\Http\Requests;

use App\Rules\PhoneNumber;
use App\Rules\SelectChange;
use Illuminate\Foundation\Http\FormRequest;

class CartPayRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order_id' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'address_country' => ['required', new SelectChange],
            'address_street' => 'required',
            'address_zip_code' => 'required',
            'address_house_number' => 'required',
            'phone' => ['required', new PhoneNumber],
            'email' => 'required',
        ];
    }

    public function filters()
    {
        return [
            'order_id' => 'trim',
            'first_name' => 'trim|capitalize',
            'last_name' => 'trim|capitalize',
            'address_country' => 'trim|capitalize',
            'address_street' => 'trim|capitalize',
            'address_zip_code' => 'trim',
            'address_house_number' => 'trim',
            'phone' => 'trim',
            'email' => 'trim',
        ];
    }

    public function messages()
    {
        return [
            'first_name.required' => 'is required',
            'last_name.required' => 'is required',
            'address_country.required' => 'is required',
            'address_street.required' => 'is required',
            'address_zip_code.required' => 'is required',
            'address_house_number.required' => 'is required',
            'phone.required' => 'is required',
            'email.required' => 'is required'
        ];
    }
}
