<div class="wrap">
    <div class="cat_box row">
        @foreach($categoryListLimit as $category)
            @if(count($category->productfc) > 0)
                @if(count($category->productfc[0]->pictures) > 0)
                    <?php
                    $sortPictures =  collect($category->productfc[0]->pictures);
                    $sortPictures = $sortPictures->sortBy('position_index');
                    $sortPictures = $sortPictures->values()->all();
                    ?>
                    <div class="cat_box__item col-12 col-md-3 col-lg-3">
                        <div class="head">
                            <a href="/catalog/{{$category->seo_name}}">
                                <img
                                    src="{!!asset('/images/product/pictures/preview/').'/'. $sortPictures[0]->image_list!!}"
                                    alt="{{$category->category_name}}">
                            </a>
                        </div>
                        <div class="name_block">
                            <div class="h-100 w-100 d-flex align-items-center justify-content-center text-whites">
                                <a href="/catalog/{{$category->seo_name}}">
                                    {{$category->category_name}}
                                </a>
                            </div>
                        </div>
                    </div>
                @endif
            @endif
        @endforeach
    </div>
</div>
