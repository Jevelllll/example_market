<?php

namespace App\Http\Controllers\Admin;
use App\ProductCategory;
use App\Products;

class ProductsHistoryController extends BaseController
{

    public function show($id)
    {
        $isCreate = true;
        $items = ProductCategory::pluck('category_name', 'id');
        $items[0] = 'Выбрать бренд';
//        $items->prepend('Выбрать бренд');
        $categories = $items;
        $model = Products::where('id', $id)->first();

        if ($model instanceof Products) {
            return view('admin/product_clone/create', compact('model', 'categories', 'isCreate'))
                ->withTitle('Дублировать товар');
        }

    }


}
