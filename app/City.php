<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer id
 * @property integer state_id
 * @property integer country_id
 * @property string name
 * @property string state_code
 * @property string country_code
 */
class City extends Model
{
    protected $table = 'cities';
    protected $fillable = [
        "created_at", "updated_at", "name", "state_id",
        "state_code","country_id", "country_code"
    ];
    public function state() {
        return $this->hasOne('App\State', 'id', 'state_id');
    }
}
