@extends('admin.layouts.admin_template')
@push('style')
    <link href="{{asset('/css/admin/order_truns.css')}}" rel="stylesheet">
@endpush
@section('content')
    <h1>Платеж @if(isset($transaction->invoice)) ({{$transaction->invoice}}) @endif</h1>
    @if($transaction instanceof \App\Transactions)
        <div class="card shadow mb-3">
            <a class="btn btn-success" href="{{route('admin.order.view', ['id' => $order_id])}}">ПЕРЕЙТИ К ЗАКАЗУ</a>
        </div>
        <ul class="card shadow mb-4 list-group">
            @foreach($transactionTitle as $titles)
                @if($titles === 'is_read')
                    <li class="view_card list-group-item">
                    <span class="view_card__key">
                {{$titles}}
            </span>
            <span class="view_card__val">
               @if($transaction->$titles == 1)
                                Просмотрено
                            @else
                                Не просмотрено
                            @endif
            </span>
                    </li>
                @else
                    <li class="view_card list-group-item">
                    <span class="view_card__key">
                {{$titles}}
            </span>
                        <span class="view_card__val">
                {{$transaction->$titles}}
            </span>
                    </li>
                @endif

            @endforeach
        </ul>
    @else
        <div style="font-size: 2.5rem" class="text-center">Нет платежей</div>
    @endif

@endsection
@push('scripts')
    <script
        src="{{asset('js/admin/orders/ordersList.js')}}?v=<?= filemtime('js/admin/orders/ordersList.js') ?>"></script>
@endpush
