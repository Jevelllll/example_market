@extends('admin.layouts.admin_template')
@push('style')
    <link href="{{asset('/css/admin/product.css')}}" rel="stylesheet">
@endpush
@section('content')
    @if($errors->any)
        @foreach($errors->all() as $error)
            <span>
                    {{$error}}
                </span>
        @endforeach
    @endif
    <div class="card shadow mb-4">
        @if($isCreate)
            <h1 class="h3 mb-4 text-gray-800 text-center pt-3">Добавить страну</h1>
        @else
            <h1 class="h3 mb-4 text-gray-800 text-center pt-3">Обновить страну</h1>
        @endif
        <div class="row justify-content-center">
            @if($isCreate)
                {!! Form::model($model, ['route' => ['country.save'], 'id'=>'countryForm', 'class'=> 'w-75','method' => 'POST']) !!}
            @else
                <span dIscreate="true" class="d-none"></span>
                {!! Form::model($model, ['route' => ['update.country', 'id' => $model->id], 'id'=>'countryForm', 'class'=> 'w-75','method' => 'PUT']) !!}
            @endif
            <div class="form-group">
                {{Form::label('value', 'Значение', ['for' => 'value'])}}
                {{Form::text('value',  null, ['id' => 'value', 'class' => 'form-control'])}}
                <div class="alert alert-danger alert-dismissible d-none">
                    <button type="button" class="close">&times;</button>
                    <strong class="infoError"></strong>
                </div>
            </div>
            <div class="form-group">
                {{Form::label('custom_value', 'Значение API', ['for' => 'custom_value'])}}
                {{Form::text('custom_value',  null, ['id' => 'custom_value', 'class' => 'form-control'])}}
                <div class="alert alert-danger alert-dismissible d-none">
                    <button type="button" class="close">&times;</button>
                    <strong class="infoError"></strong>
                </div>
            </div>
                <div class="form-group">
                {{Form::label('idMap', 'Id связи городов', ['for' => 'idMap'])}}
                {{Form::text('idMap',  null, ['id' => 'idMap', 'class' => 'form-control'])}}
                <div class="alert alert-danger alert-dismissible d-none">
                    <button type="button" class="close">&times;</button>
                    <strong class="infoError"></strong>
                </div>
            </div>
                <div class="form-group">
                {{Form::label('phonecode', 'Код тел.', ['for' => 'phonecode'])}}
                {{Form::text('phonecode',  null, ['id' => 'phonecode', 'class' => 'form-control'])}}
                <div class="alert alert-danger alert-dismissible d-none">
                    <button type="button" class="close">&times;</button>
                    <strong class="infoError"></strong>
                </div>
            </div>
            <div class="form-group">
                {{Form::label('name_en', 'Название(на английском)', ['for' => 'name_en'])}}
                {{Form::text('name_en',  null, ['id' => 'name_en', 'class' => 'form-control'])}}
                <div class="alert alert-danger alert-dismissible d-none">
                    <button type="button" class="close">&times;</button>
                    <strong class="infoError"></strong>
                </div>
            </div>
            <div class="form-group">
                {{Form::label('name_ru', 'Название(на русском)', ['for' => 'name_ru'])}}
                {{Form::text('name_ru',  null, ['id' => 'name_ru', 'class' => 'form-control'])}}
                <div class="alert alert-danger alert-dismissible d-none">
                    <button type="button" class="close">&times;</button>
                    <strong class="infoError"></strong>
                </div>
            </div>
            @if($isCreate)
                {{Form::submit('Создать',
['class' => 'btn btn-form btn-success shadow-custom text-uppercase border-radius-50 btn-lg w-100 mt-2 mb-3'])}}
            @else
                {{Form::submit('Обновить',
 ['class' => 'btn btn-form btn-success shadow-custom text-uppercase border-radius-50 btn-lg w-100 mt-2 mb-3'])}}
            @endif
            {!! Form::close() !!}
        </div>
        @if(!$isCreate)
            <button class="delete ml-auto btn btn-danger mb-5 float-right"
                    attr-delete="{{$model->id}}"
                    attr-route="{{route("country.delete")}}">Удалить&nbsp;<i class="far fa-trash-alt"></i></button>
        @endif

    </div>
@endsection
@push('scripts')
    <script src="{{asset('/js/admin/countries/countriesList.js')}}"></script>
@endpush
