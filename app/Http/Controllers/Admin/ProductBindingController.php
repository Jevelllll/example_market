<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\ProductBinding;
use App\Products;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductBindingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $dataProduct = new Products();
        $dataProductAll = Products::with('bindings')->get();
        $productOther = [];

        foreach ($dataProductAll as $product) {
            if ($product->id == $id) {
                $dataProduct = $product;
            } else {
                $productOther[] = $product;
            }
        }

        if ($dataProduct instanceof Products) {
        } else {
            session()->flash('error', 'Ошибка товара');
        }
        return view('admin/product_binding/index', compact('dataProduct', 'productOther'))
            ->withTitle('Все связи');
    }

    public function save(Request $request)
    {
        $data = [];
        $requestData = $request->all();
        $product_id = $requestData['product_id'];

        DB::table("product_bindings")->where('product_id', (int)$product_id)->delete();
        if (isset($requestData['product_parent_id'])) {
            $product_parent_ids = $requestData['product_parent_id'];
            foreach ($product_parent_ids as $parent_id) {
                $productBinding = new ProductBinding();
                $productBinding->product_id = (int)$product_id;
                $productBinding->parent_id = (int)$parent_id;
                $productBinding->save($data);
            }
        }
    }
}
