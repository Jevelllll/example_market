@extends('landing.layouts.app')
@push('style')
    <link href="{{asset('css/shipping/shipping.css')}}?v=<?= filemtime('css/shipping/shipping.css') ?>"
          rel="stylesheet">
    <link href="{{asset('plugins/spinner/index.css')}}?v=<?= filemtime('plugins/spinner/index.css') ?>"
          rel="stylesheet">
    <link href="{{asset('plugins/phone-mask/css/intlTelInput.css')}}?v=<?= filemtime('plugins/phone-mask/css/intlTelInput.css') ?>"
          rel="stylesheet">
@endpush
@section('content')
    <section class="wrap box-s-box mt-5">
        @include('landing/particals/breadcrums', ['data' => ['cart' => [$cartBreadcrums]]])
        @if($orderModel instanceof \App\Orders)
            <div class="container base-shadow pb-3">
                <h1 class="pt-3 mb-3 pb-1 bottom-line">Checkout by PayPal</h1>
                <div class="table-responsive">
                    <table class="table">
                        <thead class="thead-light">
                        <tr>
                            <th>ORDER</th>
                            <th>{{$orderModel->order_nr}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>STATUS</td>
                            @if($orderModel->status == 1)
                                <td>NEW</td>
                            @endif
                        </tr>
                        <tr>
                            <td>DELIVERY STATUS</td>
                            <td>PENDING</td>
                        </tr>
                        <tr>
                            <td>CREATED</td>
                            <td>{{date_format($orderModel->created_at, 'Y/m/d')}}</td>
                        </tr>
                        <tr>
                            <td>STATUS UPDATE</td>
                            <td>{{date_format($orderModel->updated_at, 'Y/m/d')}}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="table-responsive">
                    <table class="table">
                        <thead class="thead-light">
                        <tr>
                            <th>ITEM</th>
                            <th>PRICE</th>
                            <th>QTY</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($orderModel->order_product as $order_product)
                            <tr>
                                <td>
                                    <div class="cart-purchase-item">
                                        <div class="purchase-img-wrap">
                                            <a href="<?=route('client-product',
                                                ['seoName' => $order_product->product->seo_name]) ?>"
                                               class="purchase-img responsive-img pl-3">
                                                @if(isset($order_product->product->pictures[0]->image_list))
                                                    <img class="rounded img-thumbnail hover-purple"
                                                         src="{!!asset('/images/product/pictures/thumbnails/').'/'.$order_product->product->pictures[0]->image_list!!}"
                                                         alt="{{$order_product->product->name}}">
                                                @else
                                                    <img
                                                        class="rounded img-thumbnail hover-purple"
                                                        src="{!!asset('/images/product/no_foto.png') !!}">
                                                @endif
                                            </a>
                                        </div>
                                        <div class="purchase-inner">
                                            <div class="purchase-title-wrap col-9">
                                                <a href="<?=route('client-product',
                                                    ['seoName' => $order_product->product->seo_name]) ?>"
                                                   class="purchase-title">{{$order_product->product->name}}</a>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <i class="fas fa-dollar-sign text-secondary"></i>&nbsp;{{$order_product->product->price}}
                                </td>
                                <td>1</td>
                            </tr>
                        @endforeach
                        <tr>
                            <td>
                                <span class="bg-secondary text-white p-2">SHIPPING (Standard Post International Air Shipping)</span>
                            </td>
                            <td>FREE</td>
                            <td>-</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td class="align-text-right">TOTAL:</td>
                            <td><i class="fas fa-dollar-sign text-secondary"></i>&nbsp;{{$productSum}}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="container pb-3">
                    <h2 class="pt-3 mb-3 pb-1 bottom-line">Shipping Address</h2>
                    @include('cart.cart-pay-form', compact('orderModel', 'customer', 'countryList'))
                </div>
            </div>

        @else
            <h4>Empty DATA</h4>
        @endif

    </section>
@endsection
@push('scripts')
    <script
        src="{{ asset('js/shipping/cart-pay-form.js') }}?v=<?= filemtime('js/shipping/cart-pay-form.js') ?>"></script>
    <script
        src="{{ asset('plugins/spinner/index.js') }}?v=<?= filemtime('plugins/spinner/index.js') ?>"></script>
    <script
        src="{{ asset('plugins/phone-mask/js/mask.min.js') }}?v=<?= filemtime('plugins/phone-mask/js/mask.min.js') ?>"></script>
@endpush
