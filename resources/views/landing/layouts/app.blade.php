<?php use Illuminate\Support\Facades\Route; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('favicon/apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('favicon/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('favicon/favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('favicon/site.webmanifest')}}">
    <link rel="mask-icon" href="{{asset('favicon/safari-pinned-tab.svg')}}" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-M209CTB4H9"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'G-M209CTB4H9');
    </script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ ($title) ? $title: env('APP_NAME') }}</title>
{!! Meta::show() !!}
<!-- Fonts -->
    <link rel="preload" href="{{ asset('css/bootstrap/bootstrap-grid.min.css') }}" as="style">
    <link href="{{ asset('css/bootstrap/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}?v=<?= filemtime('css/app.css') ?>" rel="stylesheet">

    {{--    font awesome--}}
    <link rel="stylesheet"
          href="{{asset('font-awesome/css/fontawesome-all.min.css')}}?v=<?= filemtime('font-awesome/css/fontawesome-all.min.css') ?>"
          media="none" onload="if(media!='all')media='all'">
    <noscript>
        <link rel="stylesheet"
              href="{{asset('font-awesome/css/fontawesome-all.min.css')}}?v=<?= filemtime('font-awesome/css/fontawesome-all.min.css') ?>">
    </noscript>
    {{-- ----- --}}
    {{--    button-up--}}
    <link rel="stylesheet"
          href="{{asset('button_up/button_up.css')}}?v=<?= filemtime('button_up/button_up.css') ?>"
          media="none" onload="if(media!='all')media='all'">
    <noscript>
        <link rel="stylesheet"
              href="{{asset('button_up/button_up.css')}}?v=<?= filemtime('button_up/button_up.css') ?>">
    </noscript>
    {{----}}
    {{--    swiper css--}}
    @if(Route::getFacadeRoot()->current()->uri() === "/")
    <link rel="stylesheet"
          href="{{asset('plugins/swiper/css/swiper.min.css')}}?v=<?= filemtime('plugins/swiper/css/swiper.min.css') ?>"
          media="none" onload="if(media!='all')media='all'">
    <noscript>
        <link rel="stylesheet"
              href="{{asset('plugins/swiper/css/swiper.min.css')}}?v=<?= filemtime('plugins/swiper/css/swiper.min.css') ?>">
    </noscript>
    @endif
    {{----}}
    @stack('style')
    {{--    fonts css--}}
    <link rel="stylesheet"
          href="{{asset('fonts/font.css')}}"
          media="none" onload="if(media!='all')media='all'">
    <noscript>
        <link rel="stylesheet"
              href="{{asset('fonts/font.css')}}">
    </noscript>
    {{----}}
    {{--    fatNav--}}
    <link rel="stylesheet"
          href="{{ asset('plugins/fatNav/jquery.fatNav.min.css') }}"
          media="none" onload="if(media!='all')media='all'">
    <noscript>
        <link rel="stylesheet"
              href="{{ asset('plugins/fatNav/jquery.fatNav.min.css') }}">
    </noscript>
    {{----}}
</head>
<body>
<!-- Back to top button -->
<a id="button-up"><i class="far fa-caret-square-up fa-lg"></i></a>
@if(session('danger'))
    <div class="error">{{ session('danger') }}</div>
@endif
@if(session('warning'))
    @include('components.modal.warning')
@endif
@if(session('success'))
    @include('components.modal.success')
@endif
@section('navbar')
    @include('landing.layouts.navbar')
@show

@yield('content')

@include('components.modal.purchare')
@include('landing.layouts.footer')

<script type="text/javascript" src="{{asset('plugins/jquery/jquery.js')}}"></script>
<script async src="{{ asset('js/purchase/purchase.js') }}?v=<?= filemtime('js/purchase/purchase.js') ?>"></script>
<script async
        src="{{ asset('js/shipping/shipping_cart.js') }}?v=<?= filemtime('js/shipping/shipping_cart.js') ?>"></script>
<script async src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script async src="{{ asset('button_up/button_up.js') }}?v=<?= filemtime('button_up/button_up.js') ?>"></script>
@if(Route::getFacadeRoot()->current()->uri() === "/")
    <script>
        $.getScript("{{ asset('plugins/swiper/js/swiper.min.js') }}?v=<?= filemtime('plugins/swiper/js/swiper.min.js') ?>", function(data, textStatus, jqxhr) {
            $.getScript("{{asset('js/landing/landing.js')}}", function (){
            });
        });
    </script>
@endif
<script async src="{{ asset('js/common.js') }}?v=<?= filemtime('js/common.js') ?>"></script>
<script async src="{{ asset('plugins/mui/mui.min.js') }}?v=<?= filemtime('plugins/mui/mui.min.js') ?>"></script>
<script async src="{{ asset('js/navbar.js') }}?v=<?= filemtime('js/navbar.js') ?>"></script>

<script>
    $.getScript("<?=asset('plugins/fatNav/jquery.fatNav.min.js')?>", function(data, textStatus, jqxhr) {
        $.fatNav();
    });

</script>

@stack('scripts')
</body>
</html>
