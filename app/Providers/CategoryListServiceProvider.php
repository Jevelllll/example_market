<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\View\View;
use App\Services\CategoryListService;

class CategoryListServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $cL = new CategoryListService();
        \Illuminate\Support\Facades\View::share('categoryList', $cL->getAll());
        \Illuminate\Support\Facades\View::share('categoryListLimit', $cL->getLimit());
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
    }
}
