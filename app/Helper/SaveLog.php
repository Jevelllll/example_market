<?php


namespace App\Helper;


use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class SaveLog
{
    public function savelog($data, $classCont, $method)
    {
        ob_start();
        var_dump($data);
        $out = ob_get_clean();
        Storage::disk('local')->put('history/'. Carbon::now()->timestamp .str_replace('\\', '_', get_class($classCont)) . '@method_' . $method . '.txt', $out);
    }

}
