<a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown"
   aria-haspopup="true" aria-expanded="false">
    <i class="fas fa-envelope fa-fw"></i>
    <!-- Counter - Messages -->
    <span class="badge badge-danger badge-counter"><span id="badgeOrder">+{{count(AnswerModel::all())}}</span>
</a>
{{--@dd(AnswerModel::all())--}}
{{--@dd(AnswerModel::all()[0])--}}
<!-- Dropdown - Messages -->
<div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in"
     aria-labelledby="messagesDropdown">
    <h6 class="dropdown-header">
        Message Center
    </h6>

    @foreach(AnswerModel::all() as $answerModel)
        @if($loop->index < 4)
            <a class="dropdown-item d-flex align-items-center"
               href="{{route('admin.answer.view',['id'=> $answerModel->id])}}">
                <div class="dropdown-list-image mr-3">
                    <img class="rounded-circle" src="{{asset('/images/product/no_foto.png')}}">
                    <div class="status-indicator bg-success"></div>
                </div>
                <div class="font-weight-bold">
                    @if($answerModel->product)
                        <div class="text-truncate">{{$answerModel->product->name}}</div>
                    @endif
                    <div class="text-truncate">{{$answerModel->message}}</div>
                    <div class="small text-gray-500">{{$answerModel->created_at}}</div>
                </div>
            </a>
        @endif
    @endforeach
    <a class="dropdown-item text-center small text-gray-500" href="{{route('admin.answer.list')}}">Все сообщения</a>
</div>
