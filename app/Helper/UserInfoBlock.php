<?php


namespace App\Helper;


use App\RulesForUsers;

class UserInfoBlock
{
    public function getFullUserRules()
    {
        $datas = RulesForUsers::select("title")->get();
        $finalRules = [];
        foreach ($datas as $data) {
            $std = new \stdClass();
            $std->title = $data->title;
            $std->shortTitle = str_replace(" ","-",mb_strtolower($data->title));
            $finalRules[] = $std;
        }
        return $finalRules;
    }
}
