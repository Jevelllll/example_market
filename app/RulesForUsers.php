<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RulesForUsers extends Model
{
    public $timestamps = false;
    protected $table = "rules_for_users";
    protected $fillable = ["title", "desc"];
}
