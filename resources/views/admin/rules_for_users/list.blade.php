@extends('admin.layouts.admin_template')
@section('content')
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary pt-3">Правила для пользователей</h6>
        </div>
        <div class="card-body">
{{--        @dd($rulesAll)--}}
            <div class="table-responsive">
                <table class="table table-bordered" id="dataRulesTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        @foreach($rulesTitle as $titles)
                            <th>{{Translators::translateRow()[$titles]}}</th>
                        @endforeach
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        @foreach($rulesTitle as $key => $titles)
                            <th>{{Translators::translateRow()[$titles]}}</th>
                        @endforeach
                    </tr>
                    </tfoot>
                    <tbody>
                    @foreach($rulesAll as $rules)
                        <tr data-id="{{$rules->id}}">
                            @foreach($rulesTitle as $key => $titles)
                                    <td>{{$rules->$titles}}</td>
                            @endforeach
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script src="{{asset('/js/admin/rules_for_users/rules_for_users.js')}}"></script>
@endpush
