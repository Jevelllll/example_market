const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css/app.css')
    .sass('resources/sass/more-details.scss', 'public/css/more-details.css')
    .sass('resources/sass/shipping/shipping.scss', 'public/css/shipping/shipping.css')
    .sass('resources/sass/errors/error-404.scss', 'public/css/errors/error-404.css')
    .sass('resources/sass/admin/app.scss', 'public/css/admin/app.css')
    .sass('resources/sass/admin/product.scss', 'public/css/admin/product.css')
    .sass('resources/sass/product-rate.scss', 'public/css/product-rate.css')
    .sass('resources/sass/admin/productBinding.scss', 'public/css/admin/productBinding.css')
    .sass('resources/sass/admin/order_truns.scss', 'public/css/admin/order_truns.css')
    .sass('resources/my-component/spinner/index.scss', 'public/plugins/spinner/index.css')
    .js('resources/js/common.js', 'public/js/common.js')
    .js('resources/js/base_front.js', 'public/js/base_front.js')
    .js('resources/js/ask_question.js', 'public/js/ask_question.js')
    .js('resources/js/navbar.js', 'public/js/navbar.js')
    .js('resources/js/purchase/purchase.js', 'public/js/purchase/purchase.js')
    .js('resources/js/shipping/shipping_cart.js', 'public/js/shipping/shipping_cart.js')
    .js('resources/js/shipping/cart-pay-form.js', 'public/js/shipping/cart-pay-form.js')
    .js('resources/js/product/product.js', 'public/js/product/product.js')
    .js('resources/js/product/rate.js', 'public/js/product/rate.js')
    .js('resources/js/landing/landing.js', 'public/js/landing/landing.js')
    .js('resources/js/product/details/more-details.js', 'public/js/product/details/more-details.js')
    .js('resources/js/admin/base.js', 'public/js/admin/base.js')
    .js('resources/js/admin/admin_base.js', 'public/js/admin/admin_base.js')
    .js('resources/js/admin/admin_navbar.js', 'public/js/admin/admin_navbar.js')
    .js('resources/js/admin/brs/bList.js', 'public/js/admin/brs/bList.js')
    .js('resources/js/admin/countries/countriesList.js', 'public/js/admin/countries/countriesList.js')
    .js('resources/js/admin/orders/ordersList.js', 'public/js/admin/orders/ordersList.js')
    .js('resources/js/admin/answer/index.js', 'public/js/admin/answer/index.js')
    .js('resources/js/admin/seo/index.js', 'public/js/admin/seo/index.js')
    .js('resources/js/admin/transactions/transactionsList.js', 'public/js/admin/transactions/transactionsList.js')
    .js('resources/js/admin/rules_for_users/rules_for_users.js', 'public/js/admin/rules_for_users/rules_for_users.js')
    .js('resources/js/admin/categories/categoryList.js', 'public/js/admin/categories/categoryList.js')
    .js('resources/js/admin/products/productList.js', 'public/js/admin/products/productList.js')
    .js('resources/js/admin/products/productBinding.js', 'public/js/admin/products/productBinding.js')
    .js('resources/js/admin/products/position.js', 'public/js/admin/products/position.js')
    .js('resources/js/admin/subcategories/subcategoryList.js', 'public/js/admin/subcategories/subcategoryList.js')
    .js('resources/my-component/spinner/index.js', 'public/plugins/spinner/index.js')
    .disableNotifications()
    .options({ processCssUrls: false });

if (mix.inProduction()) {
    mix.version();
}
