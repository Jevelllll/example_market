<footer class="mt-1 pb-5 p-r wrap">
    <div class="container">
        <div class="row pt-4">
            <div class="col-12 text-center">
                <div class="logo">
                    <a href="/" class="logo-link hover-logo">
                        <img src="{{asset('/images/logo.png')}}" alt="Logo {{env('APP_NAME')}}">
                    </a>
                </div>
            </div>
            <div class="col-12 mt-4 mb-4 d-flex justify-content-center">
                <div class="divider"></div>
            </div>
            <div class="col-12">
                <div class="row pb-3 text-center">
                    @foreach(Rulesuslist::getFullUserRules() as $rule)
                        @if(!empty($rule))
                            <div class="col-12 col-lg-4">
                                <a href="{{route('rfu.show', ['name' => $rule->shortTitle])}}" title="{{$rule->title}}" class="cursor-pointer text-white">{{$rule->title}}</a>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>

            <div class="col-12">
                <div class="d-flex justify-content-center">
                    <div class="m-1">
                        <a href="#" title="facebook">
                            <img class="icon-contacts" src="/images/icons/facebook.png" alt="facebook">
                        </a>
                    </div>
                    <div class="m-1">
                        <a href="#" title="instagram">
                            <img class="icon-contacts" src="/images/icons/instagram.png" alt="instagram">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
