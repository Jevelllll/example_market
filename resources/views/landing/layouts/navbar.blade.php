<?php
//dd($categoryList);
?>
<header>
    <div class="site-header__mini">
    </div>
    <div class="global-header">
        <div class="nav-pc">
            <nav class="navbar navbar-expand-sm bg-light navbar-light d-flex justify-content-betweend">
                <div id="menu-wrap" class="menu-wrap">
                    <button class="btn"><i class="fas fa-bars"></i>&nbsp;BRENDS</button>
                </div>
                <div class="w-100 d-flex justify-content-between">
                    <a href="/" class="logo-link__nav__pc">
{{--                        <img src="{{asset('/images/logo.png')}}" alt="Logo {{env('APP_NAME')}}">--}}
{{--                        <img src="{{asset('/images/backinbag-logo.png')}}" class="hover-logo" alt="Logo {{env('APP_NAME')}}">--}}
                    </a>
                    <ul class="navbar-nav">
                        <li class="shopping" data-image="{{asset('images/icons')}}">
{{--                            <span id="orderCart"><i class="fa fa-shopping-cart" aria-hidden="true">&nbsp;</i>0</span>--}}
                            <span id="orderCart"><img class="shopping-bag hover-logo" src="{{asset('images/icons/shopping-bag.png')}}">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        </li>
                    </ul>
                </div>
            </nav>

            <div class="menu-header" id="menu-header">
                <ul class="m-3 category" id="categoryContainer">

                    @foreach($categoryList as $category)
                        @if($category)
                            @if(count($category->productfc) > 0)
                                <li class="pl-3 categoryLink purple-gr">
                                    <a href="/catalog/{{$category["seo_name"]}}">{{$category["category_name"]}}</a>
                                    <ul style="display: none" class="subcategory">
                                        @foreach($category["subcategory"] as $subcategory)
                                            @if($subcategory->products)
                                                <li class="subcategory_item">
                                                    <a href="/catalog/{{$category["seo_name"]}}/{{$subcategory["seo_name"]}}">
                                                        {{--                                                    <img class="subcategory_item_img--}}
                                                        {{--                                                img-fluid h-100 w-100"--}}
                                                        {{--                                                         src="{!!asset('images/subcategory/thumbnails/').'/'.$subcategory["img"]!!}"--}}
                                                        {{--                                                         alt="{{$subcategory["name"]}}">--}}
                                                        <span
                                                            class="subcategory_item_text">{{$subcategory->name}}</span>
                                                    </a>
                                                </li>
                                            @endif
                                        @endforeach
                                    </ul>
                                </li>
                            @endif
                        @endif

                    @endforeach
                </ul>
            </div>
        </div>
        {{--        @dd($categoryList)--}}
        <div class="nav-mobile">
            <div class="fat-nav">
                <div class="fat-nav__wrapper">
                    <ul>
                        @foreach($categoryList as $category)
                            @if($category)
                                <li>
                                    <button data-open-p="{{$loop->index}}" data-href="/catalog/{{$category["seo_name"]}}"
                                            class="menu-parent-btn">{{$category["category_name"]}} <i
                                            class="fas fa-sort-down"></i></button>
                                </li>
                                <li class="menu-child p-0" data-open-child="{{$loop->index}}">
                                    <span>
                                        <a href="/catalog/{{$category["seo_name"]}}">All</a>
                                    </span>
                                    @foreach($category["subcategory"] as $subcategory)
                                        @if($subcategory->products)
                                            <span>
                                                <a href="/catalog/{{$category["seo_name"]}}/{{$subcategory["seo_name"]}}">{{$subcategory->name}}</a>
                                            </span>
                                        @endif
                                    @endforeach
                                </li>
                            @endif
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="menu_container global-header navbar">
                <div class="w-100 d-flex justify-content-end pr-3">
                    <div class="empty-width"></div>
                    <a href="/" class="logo-link__nav__mobile m-auto hover-logo">
{{--                        <img src="{{asset('/images/backinbag-logo.png')}}" alt="Logo {{env('APP_NAME')}}">--}}
                    </a>
                    <ul class="navbar-nav">
                        <li class="shopping shopping__mobile-size border-0">

                            <span id="orderCart1"><img class="shopping-bag hover-logo" src="{{asset('images/icons/shopping-bag.png')}}">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        </li>
                    </ul>
                </div>

            </div>
        </div>


    </div>
</header>
