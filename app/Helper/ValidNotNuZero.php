<?php


namespace App\Helper;


use Illuminate\Contracts\Validation\Rule;

class ValidNotNuZero implements Rule

{  // Should return true or false depending on whether the attribute value is valid or not.
    public function passes($attribute, $value)
    {
        return $value > 0;
    }

    // This method should return the validation error message that should be used when validation fails
    public function message()
    {
        return 'Обязательно к заполнению';
    }

}
