<?php

namespace App\Http\Controllers;

use App\Answers;
use App\Countries;
use App\Customers;
use App\Http\Controllers\Message\MessageGmailController;
use App\Http\Requests\CartPayRequest;
use App\Orders;
use App\PaymentStatuses;
use App\State;
use App\Transactions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Savemylog;

class PayPalController extends Controller
{

    public function validatePayment(CartPayRequest $request)
    {
        $errorClass = new \stdClass();
        $errorClass->errors = new \stdClass();
        $errorList = new \stdClass();
        $countryId = $request->input('address_country');
        $staleList = State::select('id', 'name')->with('city')->where('country_id', $countryId)->get()->toArray();
        $this->customValidStateCity($request, $staleList, $errorList);
        $errorClass->errors = $errorList;
        if (count((array)$errorList) > 0) return \response()->json($errorClass, 422);
        return \response()->json(['success' => true]);
    }

    public function payment(CartPayRequest $request)
    {
        $errorClass = new \stdClass();
        $errorClass->errors = new \stdClass();
        $errorList = new \stdClass();
        $countryId = $request->input('address_country');
        $staleList = State::select('id', 'name')->with('city')->where('country_id', $countryId)->get()->toArray();
        $requestAll = $request->all();
        $this->customValidStateCity($request, $staleList, $errorList);
        $errorClass->errors = $errorList;
        if (count((array)$errorList) > 0) return \response()->json($errorClass, 422);

        $testmode = true;
        $paypalurl = $testmode ? 'https://shop.westernbid.info' : 'https://shop.westernbid.info';

        if ($request->post('order_id')) {
            $comment = $request->post('comment');
            $orderId = (int)$request->post('order_id');
            $orderModel = Orders::with(['order_product' => function ($query) {
                return $query->with(['product' => function ($qw) {
                    return $qw->with(['pictures']);
                }]);
            }])->where('id', $orderId)->first();
            $productSum = collect($orderModel->order_product)->sum(function ($product) {
                return $product->product->price;
            });
            if ($comment) {
            }
            $countryModel = Countries::where('idMap', $countryId)->first();
            $merchantAccount = env('MERCHANTACCOUNT');
            $secretKey = env('SECRETKEY');
            $invoice = $merchantAccount . '-' . $orderModel->order_nr;
            $data = [
                'wb_login' => $merchantAccount,
                'wb_hash' => md5($merchantAccount . $secretKey . $productSum . $invoice),
                'invoice' => $invoice,
            ];
            $data['email'] = $request->post('email');
            $data['phone'] = $request->post('phone');
            $data['first_name'] = $request->post('first_name');
            $data['last_name'] = $request->post('last_name');
            $data['address1'] = $request->input('address_house_number') . ' ' . $request->input('address_street');
            $data['country'] = $countryModel->value;
            if ($request->post('address_city')) {
                $data['city'] = $request->post('address_city');
            }
            if ($request->post('address_state')) {
                $data['state'] = $request->post('address_state');
            }
            $data['zip'] = $request->post('address_zip_code');
            $data['item_name'] = $orderModel->order_nr;
            $data['amount'] = (string)$productSum;
            $data['shipping'] = '0';
            $data['currency_code'] = 'USD';
//            $data['business'] = 'sb-xhqiv2849410@business.example.com';
            $data['cancel_return'] = route('payment.cancel');
            $data['address_override'] = '1';
            $data['notify_url'] = route('payment.notify');
            $data['return'] = route('payment.success');

            $itemLoop = 1;
            $itemProductList = [];
            foreach ($orderModel->order_product as $ordProduct) {
                $product = $ordProduct->product;
                if ($product) {
                    $itemProductList['amount_' . $itemLoop] = (string)$product->price;
                    $itemProductList['item_name_' . $itemLoop] = $product->name;
                    $itemProductList['item_number_' . $itemLoop] = (string)$product->id;
                    $itemProductList['quantity_' . $itemLoop] = (string)1;
                    $itemProductList['description_' . $itemLoop] = $product->desc;
                    $itemProductList['url_' . $itemLoop] = route('client-product',
                        ['seoName' => $product->seo_name]);
                }
                $itemLoop++;
            }

            if (count($itemProductList) > 0) {
                $commonOrder = array_merge($data, $itemProductList);
                $requestAll['address_country'] = $countryModel->name_en;
                $customer = new Customers();
                if ($customer->create($requestAll)) {
                }
                return $this->wbPost($commonOrder, $paypalurl);
            }
        }
    }

    public function wbPost($data, $url)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
//        curl_setopt($ch, CURLOPT_ENCODING, "");

        header('Content-Type: text/html');
        $data = curl_exec($ch);
        echo $data;
    }

    public function cancel()
    {
        return redirect()->route('welcome')->with('warning', 'Your payment is canceled.');
    }

    public function success()
    {
        return redirect()->route('welcome')->with('success', 'Your payment was successfully.');
    }

    public function notify()
    {
        $raw_post_data = file_get_contents('php://input');
        $raw_post_array = explode('&', $raw_post_data);
        $myPost = array();
        $transaction = new Transactions();
        foreach ($raw_post_array as $keyval) {
            $keyval = explode('=', $keyval);
            if (count($keyval) == 2) {
                $myPost[$keyval[0]] = urldecode($keyval[1]);
            }

        }
        Savemylog::savelog($myPost, $this, __FUNCTION__);
        $invoice = $myPost['invoice'];
        preg_match('/#(.*)-/', $invoice, $matches);
        $orderId = $matches[1];
        $myPost["order_id"] = $orderId;
        $transaction->create($myPost);
        $statusPayment = PaymentStatuses::where('id', 'like', "%{$myPost['payment_status']}%")->first();
        if ($statusPayment instanceof PaymentStatuses) {
            $isSold = $statusPayment->sold;
            $orderModel = Orders::where('id', $orderId)->first();
            if ($orderModel instanceof Orders) {
                $orderModel->is_order_read = '0';
                $orderModel->status = $statusPayment->id;
                $orderModel->save();
                if ((boolean)$isSold) {
                    foreach ($orderModel->order_product as $ordProd) {
                        $product = $ordProd->product;
                        $product->is_sold = '1';
//                        $product->status_available = '0';
                        $product->update();
                    }
                    if (isset($orderModel->customer)) {
                        $messageGmail = new MessageGmailController(
                            '',
                            $orderModel->customer->email,
                            $orderModel, 'Purchase details'
                        );
                        $messageGmail->actionSend();
                    }
                }
            }
        }
    }

    public function actionSendAnswer(Request $request)
    {
        $requestData = $request->all();
        $messages = [
            'from.required' => 'This field is required.',
            'from.email' => 'This field is invalid',
            'message.required' => 'This field is required.'
        ];
        Validator::make($requestData, [
            'from' => 'required|email',
            'message' => 'required|min:5',
            'product_id' => 'required'
        ], $messages)->validate();
        $answerModel = new Answers();
        if ($answerModel->create($requestData)) {
            return response()->json([
                'msg' => true,
            ], 200);
        } else {
            return response()->json([
                'msg' => false,
            ], 422);
        }
    }

    /**
     * @param CartPayRequest $request
     * @param $staleList
     * @param \stdClass $errorList
     */
    private function customValidStateCity(CartPayRequest $request, $staleList, \stdClass $errorList): void
    {
        if (empty($request->input('address_state'))) {
            if (count($staleList) > 0) {
                $errorList->address_state = ['is required'];
            }
        }
        if (empty($request->input('address_city'))) {
            if (count($staleList) > 0) {
                if (count($staleList[0]['city']) > 0) {
                    $errorList->address_city = ['is required'];
                }
            }
        }
    }

}
