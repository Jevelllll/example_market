<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\RulesForUsers;
use App\Seo;
use Djmitry\Meta\Meta;
use Illuminate\Http\Request;


class RulesForUsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rulesAll = RulesForUsers::all();
        $rulesTitle = collect($rulesAll[0])->toArray();
        $rulesTitle = array_keys($rulesTitle);

        return view('admin/rules_for_users/list', compact("rulesTitle", "rulesAll"))
            ->withTitle('Правила для пользователей');
    }


    public function edit($id)
    {
        $model = RulesForUsers::where("id", $id)->first();
        if ($model instanceof RulesForUsers) {
            return view('admin/rules_for_users/edit', compact("model"))->withTitle('Редактировать');
        } else {
            return view('errors.503');
        }
    }

    public function show($name) {


            Meta::add(
                [
                    'title' => $name,
                    'robots' => 'index, follow'
                ]);

        $model = RulesForUsers::where("title", "like", '%' .  str_replace("-"," ", $name). '%')->first();
        if ($model instanceof RulesForUsers) {
            return view('user-info/show', compact("model"))->withTitle($model->title);
        } else {
            return view('errors.404');
        }

    }

    public function update(Request $request, $id)
    {

        $requestAll = $request->all();
        $model = RulesForUsers::where("id", $id)->first();
        if ($model instanceof RulesForUsers) {
            if ($model->update($requestAll)) {
                return redirect(route('rfu.list'));
                return response()->json([
                    'msg' => true,
                ], 200);
            } else {
                return redirect(route('rfu.list'));
                return response()->json([
                    'msg' => false,
                ], 404);
            }
        } else {
            return redirect(route('rfu.list'));
            return response()->json([
                'msg' => false,
            ], 404);
        }
    }

}
