
@extends('landing.layouts.app')
@section('content')
    <div class="wrap">
        @include('landing/particals/breadcrums', ['data' => ['categories' => $category, 'subcategories' => $subcategory, 'hName' => $hName]])
        @include('product/product-list',
        [
        'data' => [
        'categories' => $category, 'subcategories' => $subcategory, 'hName' => $hName, 'productsList' => $productsList
        ]
        ])
    </div>
@endsection
