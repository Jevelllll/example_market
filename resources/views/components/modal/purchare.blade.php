<div id="myModal-purchare" class="modal-purchare">

    <div class="modal-content-purchare">
        <span class="close-purchare">&times;</span>
        <div class="purchase-bottom-line">
            <h4>Your Cart</h4>
        </div>
        <div class="cart-inner">
            <div id="purchase-content">

            </div>
            <div class="purchase-total">
                <div class="purchase-total-label">Total: <span id="total-sum"></span></div>
                <div class="cart-check-price row">
                    <button class="button-add-cart col purple-gr" onclick="window.location.href = '/';">SHOP FOR MORE
                    </button>
                    &nbsp;
                    <button class="button-add-cart col proceedToCheckout purple-gr" data-attr-r-checkout="<?=route('cart-checkout')?>">
                        checkout
                    </button>
                </div>

            </div>
        </div>

    </div>


</div>
