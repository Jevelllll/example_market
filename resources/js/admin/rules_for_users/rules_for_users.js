import {detectTrTable, formSend, translatorTableColumn} from "../base";

$(document).ready(function () {

    setTimeout(function () {
        $('#dataRulesTable').DataTable(translatorTableColumn());
    }, 0);
    detectTrTable('dataRulesTable', '/admin/rules/edit/');
    // formSend('#rulesUpdate', '/admin/rules/list');
});
