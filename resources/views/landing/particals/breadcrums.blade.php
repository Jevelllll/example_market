<?php $markupStr = []; $index = 1 ?>
<?php $markupStr[] =  [
    '@type' => 'ListItem',
    'position'=> $index,
    'name' => 'Home',
    'item' => trim(env('APP_URL')) .'/'
];
$index++;
?>
@if(!empty($data["cart"]))
    <div id="breadcrumbs" class="d-flex align-items-center m-2 p-2">
        <div class="row">
            <ul class="breadcrumbs">
                <li>
                    <a href="/">
                        <span itemprop="name">Home</span>

                    </a>
                </li>
                @foreach($data["cart"] as $cart)
                    <li class="current">
                        <span class="line-divider">&nbsp;/</span>
                        @if($loop->last)
                            <span class="current_line" itemprop="name">{{$cart["title"]}}</span>
                        @else
                            <a href="/{{strtolower($cart["href"])}}">
                                <span itemprop="name">{{$cart["title"]}}</span>
                            </a>
                            <?php $markupStr[] =  [
                                '@type' => 'ListItem',
                                'position'=> $index,
                                'name' => trim($cart["title"]),
                                'item' => trim(env('APP_URL'). '/'. strtolower($cart["href"]))
                            ];
                            $index++;
                            ?>
                        @endif
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
@elseif(!empty($data["categories"]) && count($data["categories"]) > 0)
    <div id="breadcrumbs" class="d-flex align-items-center m-2 p-2">

        <div class="row">
            <ul class="breadcrumbs">
                <li>
                    <a href="/">
                        <span itemprop="name">Home</span>
                    </a>
                </li>
                @foreach($data["categories"] as $category)

                    <li class="current">
                        <span class="line-divider">&nbsp;/</span>
                        <a href="/catalog/{{strtolower($category->seo_name)}}">
                            <span itemprop="name">{{$category->category_name}}</span>
                        </a>
                        <?php $markupStr[] =  [
                            '@type' => 'ListItem',
                            'position'=> $index,
                            'name' => trim($category->category_name),
                            'item' => trim(env('APP_URL'). '/catalog/'. strtolower($category->seo_name))
                        ];
                        $index++;
                        ?>
                    </li>
                @endforeach

                @if(!empty($data["subcategories"]) && count($data["subcategories"]) > 0)
                    @if($data["subcategories"][0])
                        @foreach($data["subcategories"] as $subcategory)

                            <li class="current">
                                <span class="line-divider">&nbsp;/</span>
                                <a href="/catalog/{{strtolower($data["categories"][0]->seo_name)}}/{{strtolower($subcategory->seo_name)}}">
                                    <span itemprop="name">{{$subcategory->name}}</span>
                                </a>
                                <?php $markupStr[] =  [
                                    '@type' => 'ListItem',
                                    'position'=> $index,
                                    'name' => trim($subcategory->name),
                                    'item' => trim(env('APP_URL'). '/catalog/'.strtolower($data["categories"][0]->seo_name). '/'. strtolower($category->seo_name))
                                ];
                                $index++;
                                ?>
                            </li>
                        @endforeach
                    @endif
                @endif
                @if(!empty($data['productName']))

                    <li class="current">
                        <span class="line-divider">&nbsp;/</span>
                        <span class="current_line" itemprop="name">{{$data['productName']}}</span>
                        <?php $markupStr[] =  [
                            '@type' => 'ListItem',
                            'position'=> $index,
                            'name' => trim($data['productName']),
                            'item' => trim(url()->current())
                        ];
                        $index++;
                        ?>
                    </li>
                @endif
            </ul>
        </div>
    </div>
@endif
@include('markup.breadcrumb', compact('markupStr'))
