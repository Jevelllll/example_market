import {detectTrTable, formSend, removeCol, translatorTableColumn} from "../base";

document.addEventListener('DOMContentLoaded', function () {
    function extracted() {
        $(document).ready(function () {
            $('#dataTable').DataTable(translatorTableColumn());
        });
        formSend('#countryForm', '/admin/country/list');
        let urlTegs = document.querySelector('.tegs');
        if (urlTegs) {
            detectTrTable('dataTable', urlTegs.getAttribute('tegs'));
        }
        (() => {
            if (document.getElementsByClassName('delete')[0]) {

                let elementsByClassName = document.getElementsByClassName('delete');
                elementsByClassName[0].addEventListener('click', (event) => {
                    let eventTarget = event.target;
                    let deleteId = eventTarget.getAttribute('attr-delete');
                    let route = eventTarget.getAttribute('attr-route');
                    removeCol(route, {id: deleteId}, "POST", "Успешно удаленно!", '/admin/country/list');
                });
            }
        })();
    }

    extracted();

    /****************for city List by state id*************************/
    function insertCityAfterChangeState() {
        let cityContainer = document.getElementById('citiesList');
        if (cityContainer) {
            let controlS = document.getElementById('controlS');
            controlS.value = 'null';
            controlS.addEventListener('change', (ev) => {
                let route = cityContainer.getAttribute('attr-route');
                let routeRemoveStr = route.split('/').pop();
                route = route.replace(routeRemoveStr, '');
                const value = ev.target.value;
                if (value === 'null') {
                    cityContainer.innerHTML = '';
                } else {
                    route = `${route}${value}`;
                    let httpClient = new XMLHttpRequest();
                    httpClient.open('GET', route);
                    httpClient.responseType = 'text';
                    httpClient.send();
                    httpClient.onload = (resp) => {
                        if (httpClient.status === 200) {
                            cityContainer.innerHTML = httpClient.response;
                            extracted();
                        }

                    }
                    httpClient.onerror = (err) => console.log(err);
                }


            });
        }
    }

    insertCityAfterChangeState()
});
