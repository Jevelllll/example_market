<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class OrderProduct
 * @package App
 * @property integer order_id
 * @property integer product_id
 */
class OrderProduct extends Model
{
    protected $table = "order_products";
    protected $fillable = ['order_id', 'product_id'];
    public  function getTableName() {
        return $this->table;
    }
    public function order() {
        return $this->hasOne('App\Orders', 'id', 'order_id');
    }
    public function product() {
        return $this->hasOne('App\Products', 'id', 'product_id');
    }
}
