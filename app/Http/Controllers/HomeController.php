<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Message\MessageGmailController;
use App\Orders;
use App\Seo;
use App\Transactions;
use Djmitry\Meta\Meta;
use Illuminate\Support\Facades\App;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        dd();
//        return view('landing.index');
    }

    public function welcome()
    {

//        $orderModel = Orders::where('id', 25)->first();

//        $body = 'wwww';
        $seo = Seo::select('meta_title', 'meta_desc', 'meta_keywords')
            ->whereNull('product_id')
            ->whereNull('subcategory_id')
            ->whereNull('product_category_id')
            ->first();
        if ($seo instanceof Seo) {
            Meta::add(
                [
                    'title' => env('APP_NAME'),
                    'description' => $seo->meta_desc,
                    'keywords' => $seo->meta_keywords,
                    'og:image:width' => 1200,
                    'og:image:height' => 630,
                    'robots' => 'index, follow'
                ]);
        }
        return view('landing.index')->withTitle(env('APP_NAME'));
//        $messageGmail = new MessageGmailController(
//            '',
//            'wane.bronx@gmail.com',
//            $orderModel, 'Purchase details'
//        );
//        $messageGmail->actionSend();

//        return view('emails.mail', compact('name', 'orderModel'));
    }
}
