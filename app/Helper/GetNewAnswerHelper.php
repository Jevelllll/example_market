<?php

namespace App\Helper;
use App\Answers;

class GetNewAnswerHelper
{
    public function all()
    {
        return Answers::where('viewed', '0')->orderBy('created_at', 'desc')->get();
    }

}
