<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\QuickOffer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class BaseController extends Controller {

    public $currentModel;

    public function constructor() {
    }

    /********************dropBox remove tmp************************************/
    public function dataImgDelete(Request $request)
    {
        $path = storage_path('tmp/uploads');
        $fileResp = $request->request->get('name');
        $filePath = $request->request->get('filePath');
        $collection = collect(json_decode($fileResp, true));
        if (!empty($filePath)) {

            $fileThumbnails = public_path() . $filePath;
            $filePublic = public_path() . $filePath;
            $fileName = array_reverse(explode("/", $filePath));
            $model =  $this->currentModel::where('gallery', $fileName[0])->first();
            if ($model instanceof  $this->currentModel) {
                $model->gallery = "";
                if ($model->update()) {
                    if (file_exists($fileThumbnails)) {
                        File::delete($fileThumbnails);
                    }
                    if (file_exists($filePublic)) {
                        File::delete($filePublic);
                    }
                }
                return response()->json(['status' => $fileName[0]]);
            }

        } else {
            if ($collection['name']) {
                File::delete($path . '/' . $collection['name']);
            }
            return $collection['name'];
        }

    }

    /*****************dropBox create tmp**************************************/
    public function dataImgCreate(Request $request)
    {
        $path = storage_path('tmp/uploads');

        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }
        $this->validate($request, [
            'file' => 'image'
        ]);
        $file = $request->file('file');

        $date = date_create('now');
        $unicaleDate =  date_format($date, 'm-d-H-i');
        $name = $file->getClientOriginalName();
        $explodeRemove = '.'.explode(".", $name)[1];
        $replaceName = str_replace($explodeRemove, "",  $name);
        $replaceName = $replaceName . '-'. $unicaleDate . $explodeRemove;
        $img = Image::make($file);
        $img->orientate();
        $img->save($path .'/'. $replaceName);
        return response()->json([
            'name' => $replaceName,
            'original_name' => $replaceName,
        ]);



    }

    /**
     * Перемешение изображения из tmp в public
     * @param array $galleryList
     * @param string $wherePatch
     * @return bool
     */
    public function saveImageMakeThumbnails(array $galleryList, $wherePatch = 'images/banners/')
    {
        $path = storage_path('tmp/uploads');
        foreach ($galleryList as $gallery) {
            if (file_exists($path . '/' . $gallery)) {
                $move = File::move($path . '/' . $gallery, $wherePatch . $gallery);
                if (!$move) {
//                    return false;
                } else {

                    $img = Image::make(file_get_contents(public_path() . '/' . $wherePatch . $gallery));
                    $img->fit(320, 240, null, 'top');
                    $imgPreview = Image::make(file_get_contents(public_path() . '/' . $wherePatch . $gallery));

//                    $img->resize(null, 240, function ($constraint) {
//                        $constraint->aspectRatio();
//                        $constraint->upsize();
//                    });
                    $imgPreview->resize(null, 618, function ($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    });

                    $imgPreview->save(public_path() . '/' . $wherePatch . 'preview/' . $gallery);

                    if ($img->save(public_path() . '/' . $wherePatch . 'thumbnails/' . $gallery)) {
//                        return true;
                    } else {
//                        return false;
                    }
                }
            }
        }
    }
}
