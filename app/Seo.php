<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer id
 * @property integer product_id
 * @property integer subcategory_id
 * @property integer product_category_id
 * @property string meta_title
 * @property string meta_desc
 * @property string meta_keywords
 */
class Seo extends Model
{
    protected $table = 'seo';
    protected $fillable = [
        "created_at", "updated_at", "product_id", "subcategory_id",
        "product_category_id", "meta_title", "meta_desc", "meta_keywords"
    ];

    public function product()
    {
        return $this->hasOne('App\Products', 'id', 'product_id');
    }

    public function subcategory()
    {
        return $this->hasOne('App\Subcategories', 'id', 'subcategory_id');
    }

    public function category()
    {
        return $this->hasOne('App\ProductCategory', 'id', 'product_category_id');
    }

    public static function convertedBase64($data)
    {
        if (base64_encode(base64_decode($data)) === $data) {
            return base64_decode($data);
        } else {
            return $data;
        }
    }
}
