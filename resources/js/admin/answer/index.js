document.addEventListener('DOMContentLoaded', function () {
    let askAnswerForm = document.getElementById('askAnswer');
    let submitButton = document.getElementById('sAns');
    let message = document.getElementById('message');
    if (message) {
        message.value = "";
    }
    let postRequest = () => {
        let url = askAnswerForm.getAttribute('action');
        let method = askAnswerForm.getAttribute('method');
        let data = askAnswerForm.serialize();
        let httpRequest = new XMLHttpRequest();
        httpRequest.open(method, url, true);
        httpRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        httpRequest.setRequestHeader(
            'X-CSRF-TOKEN',
            document.querySelector("meta[name='csrf-token']").getAttribute('content'));
        httpRequest.setRequestHeader("X-Requested-With", "XMLHttpRequest");
        httpRequest.responseType = 'json';
        httpRequest.send(data);
        httpRequest.onload = () => {
            if (httpRequest.status === 200) {
                swal({
                    title: 'Успех!',
                    text: 'Сообщение оставлено!',
                    type: "success",
                    confirmButtonText: "OK",
                    showCancelButton: false
                }).then(() => {
                    console.log(httpRequest.response);
                    window.location.reload(true);
                });
            } else {
                swal({
                    title: 'Ошибка!',
                    text: 'Перезагрузите страницу и повторите операцию!',
                    type: "error",
                    confirmButtonText: "OK",
                    showCancelButton: false
                }).then(() => {
                    window.location.reload(true);
                });
            }
        };
        httpRequest.onerror = () => {
            swal({
                title: 'Ошибка!',
                text: 'Перезагрузите страницу и повторите операцию!',
                type: "error",
                confirmButtonText: "OK",
                showCancelButton: false
            }).then(() => {
                window.location.reload(true);
            });
        }
    }
    let validateLengthMessage = () => {
        if (message) {
            message.addEventListener('keyup', () => {
                if (message.value.length > 0) {
                    submitButton.disabled = false;
                } else {
                    submitButton.disabled = true;
                }
            })
        }
        return false;
    };
    let listenerSendForm = () => {
        if (askAnswerForm) {
            askAnswerForm.addEventListener('submit', (e) => {
                e.preventDefault();
                postRequest();
            });
        }
        return false;
    };
    validateLengthMessage();
    listenerSendForm();
});
