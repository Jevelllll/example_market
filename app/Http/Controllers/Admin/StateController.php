<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\State;
use Illuminate\Http\Request;

class StateController extends Controller
{
    public function save()
    {
    }

    public function index()
    {
        $stateList = State::with('countries')->select('id', 'name', 'country_id')->orderBy('country_id', 'ASC')->get()->toArray();
        $newSList = [];
        foreach ($stateList as $key => $value) {
            $newState = new State();
            foreach ($value as $k => $v) {
                if ($k == 'countries') {
                    $newState->country_id = $v['name_ru'];
                } else {
                    $newState->$k = $v;
                }

            }
            $newSList[] = $newState;
        }
        $stateList = $newSList;
        if ($stateList) {
            if ($stateList[0] instanceof State) {
                $stateTitle = collect($stateList)[0]->toArray();
                $stateTitle = array_keys($stateTitle);
                return view('admin.state.list', compact('stateList', 'stateTitle'))
                    ->withTitle('Регионы PayPal');
            } else {
                dd(503);
            }
        } else {
            $data = ["stateTitle" => null, "stateList" => null];
            return view('admin.state.list', $data)
                ->withTitle("Регионы PayPal");
        }

    }

    public function edit($id)
    {
        $isCreate = false;
        $model = State::where('id', (int)$id)->first();

        if ($model instanceof State) {
            return view('admin/state/create', compact("isCreate", "model"))->withTitle('Редактировать регион');
        } else {
            dd(503);
        }
    }

    public function update(Request $request, $id)
    {
        $requestAll = $request->all();
        $model = State::where('id', (int)$id)->first();

        if ($model instanceof State) {
            if ($model->update($requestAll)) {
                return response()->json([
                    'msg' => true,
                ], 200);
            } else {
                return response()->json([
                    'msg' => false,
                ], 404);
            }
        } else {
            return response()->json([
                'msg' => false,
            ], 404);
        }

    }

    public function delete(Request $request)
    {
        $requestData = $request->all();
        $model = State::find($requestData['id']);
        if ($model instanceof State) {
            if ($model->delete()) {
                return response()->json([
                    'msg' => true,
                ], 200);
            } else {
                return response()->json([
                    'msg' => false,
                ], 404);
            }
        } else {
            return response()->json([
                'msg' => false,
            ], 404);
        }
    }

}
