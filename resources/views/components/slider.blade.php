<div class="swiper-container">
    <h1 class="title-slider">
        <span class="title-slider__t">{{env('APP_NAME')}}</span>
        <span class="title-slider__subtitle">we sell designer luxury preowned items.</span></h1>
    <div class="swiper-wrapper">
        @foreach($bannerList as $banner)
{{--            asset('images/banners/' .$banner->gallery--}}
        <div class="swiper-slide">
            <img alt="{{$banner->name}}" class="h-100"
                style="min-height: 302px;width: 100%; object-fit: cover"
                 src="{{asset('images/banners') .'/'.$banner->gallery}}">
            <div class="card text-white bg-dark mb-3 card-container" style="max-width: 20rem;">
                <div class="card-header">{{$banner->name}}</div>
                <div class="card-body">
                    <p class="card-text">{{$banner->desc}}</p>
                    <a href="{{$banner->redirect_url}}" class="btn btn-light float-right">Check this</a>
                </div>
            </div>
        </div>
        @endforeach;
    </div>
    <!-- Add Pagination -->
    <div class="swiper-pagination"></div>
</div>
<?php // dd($bannerList);?>
@push('scripts')
@endpush
