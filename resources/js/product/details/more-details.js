(function ($, undefined) {

    document.addEventListener('touchstart', (el) => {
        el.preventDefault();
        let close = document.querySelector('.fancybox-button--close');
        let isHide = !el.target.classList.contains('fancybox-image');
        if (isHide === true) {
            if (el.target.classList.value.length > 0) {
                if (close) {
                    close.click();
                }
            }
        }
    })


    // $("#zoom_01").elevateZoom({constrainType:"height", constrainSize:274, zoomType: "lens", containLensZoom: true, gallery:'gallery_01', cursor: 'pointer', galleryActiveClass: "active"});
})(jQuery);

