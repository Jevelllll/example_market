@extends('admin.layouts.admin_template')
@section('content')
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary pt-3">Список товаров</h6>
        </div>
        @if($products)
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        @foreach($productsTitle as $titles)
                            @if($titles === 'img')
                                <th class="image_thumbnails">{{Translators::translateRow()[$titles]}}</th>
                            @else
                                <th>{{Translators::translateRow()[$titles]}}</th>
                            @endif
                        @endforeach
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        @foreach($productsTitle as $key => $titles)
                            @if($titles === 'img')
                                <th class="image_thumbnails">{{Translators::translateRow()[$titles]}}</th>
                            @else
                                <th>{{Translators::translateRow()[$titles]}}</th>
                            @endif
                        @endforeach
                    </tr>
                    <tfoot>
                    <tbody>
                    @foreach($products as $product)
                        <tr data-id="{{$product->id}}">
                            @foreach($productsTitle as $key => $title)
                                @if($title === 'pictures')
                                    <td>
                                        @if(count($product->$title) > 0)
                                            @foreach($product->$title as $img)
                                                @if(file_exists(public_path().'/images/product/pictures/thumbnails/'. $img["image_list"]))
                                                    <img
                                                        style="width: {{ 95 / count($product->$title).'%'}}; max-width: 110px;"
                                                        src="{!! asset('/images/product/pictures/thumbnails/'. $img["image_list"]) !!}"
                                                        class="img-fluid rounded">
                                                @else
                                                    <img
                                                        style="width: {{ 100 / count($product->$title).'%'}} ; max-width: 110px;"
                                                        src="{!! asset('images/product/no_foto.png') !!}"
                                                        class="img-fluid rounded">
                                                @endif
                                            @endforeach
                                        @else
                                            <img
                                                style="width: 100% ; max-width: 110px;"
                                                src="{!! asset('images/product/no_foto.png') !!}"
                                                class="img-fluid rounded">
                                        @endif
                                    </td>
                                @elseif($title === 'is_sold')
                                    @if($product->is_sold == 1)
                                        <td>Да</td>
                                    @else
                                        <td>Нет</td>
                                    @endif
                                @else
                                    <td>{{$product->$title}}</td>
                                @endif

                            @endforeach
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        @else
            <div style="font-size: 2.5rem" class="text-center">Отсутствуют товары</div>
        @endif
        <?php $title = 'Список товаров';?>
    </div>
@endsection
@push('scripts')
    <script src="{{asset('/js/admin/products/productList.js')}}"></script>
@endpush
