function ajaxRate(url, productId, rate) {
    let httpClient = new XMLHttpRequest();
    httpClient.open('POST', url, true);
    httpClient.setRequestHeader(
        'X-CSRF-TOKEN',
        document.querySelector("meta[name='csrf-token']").getAttribute('content'));
    httpClient.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    httpClient.setRequestHeader("X-Requested-With", "XMLHttpRequest");
    httpClient.responseType = 'json';
    httpClient.send(`product_id=${productId}&value=${rate}`);
    httpClient.onload = () => {
        if (httpClient.status === 200) {
            let response = httpClient.response;
        }
    };
    httpClient.onerror = () => {
    }
}

function getDataIndex() {
    let index = this.dataset.indexRate;
    let productId = this.closest('[data-product-id]').dataset.productId;
    let productUrl = this.closest('[data-product-id]').dataset.productUrl;
    return {index, productId, productUrl};
}

function clearActive(parentElement, maxIndex = 0, currentClickElement) {

    function compareActive(currentClickElement, cd) {
        let lastAct;
        for (let ind = 0; ind < parentElement.length; ind++) {
            if (parentElement[ind].classList.contains('active')) {
                lastAct = parentElement[ind];
            }
        }
        cd(
            {
                currentClickElement: currentClickElement,
                lastActiveElement: lastAct
            });
    }

    compareActive(currentClickElement, (result) => {
        const curClEl = (result.currentClickElement) ? result.currentClickElement : null;
        const lastActive = (result.lastActiveElement) ? result.lastActiveElement : null;
        if (curClEl === lastActive) {
            for (let pIndex = 0; pIndex < parentElement.length; pIndex++) {
                parentElement[pIndex].classList.remove('active');
            }
        } else {
            for (let pIndex = 0; pIndex < parentElement.length; pIndex++) {
                parentElement[pIndex].classList.remove('active');
            }
            for (let pIndex = 0; pIndex < maxIndex; pIndex++) {
                parentElement[pIndex].classList.add('active');
            }
        }
    })

}

function ratMouseenter() {

    let {index} = getDataIndex.call(this);
    const parentElement = this.parentElement.querySelectorAll('i');
    for (let pIndex = 0; pIndex < index; pIndex++) {
        parentElement[pIndex].classList.remove('far');
        parentElement[pIndex].classList.remove('fas');
        parentElement[pIndex].classList.add('fas');
    }
}

function ratMouseleave() {
    let {index} = getDataIndex.call(this);
    const parentElement = this.parentElement.querySelectorAll('i');
    for (let pIndex = 0; pIndex < index; pIndex++) {
        parentElement[pIndex].classList.remove('far');
        parentElement[pIndex].classList.remove('fas');
        parentElement[pIndex].classList.add('far');
    }
}

function click() {
    const parentElement = this.parentElement.querySelectorAll('i');
    let {index, productId, productUrl} = getDataIndex.call(this);
    ajaxRate(productUrl, productId, index);
    clearActive(parentElement, index, this);
}

function sendRate() {
    let queryRateAll = document.querySelectorAll('i[data-index-rate]');
    for (let index = 0; index < queryRateAll.length; index++) {
        const rate = queryRateAll[index];
        rate.addEventListener('mouseenter', ratMouseenter)
        rate.addEventListener('mouseleave', ratMouseleave)
        rate.addEventListener('click', click)
    }
}


sendRate();
