@push('style')
    <link rel="stylesheet"
          href="{{asset('css/product-rate.css')}}?v=<?= filemtime('css/product-rate.css') ?>">
@endpush
@if(!empty($product->rate))

@else
    <div class="back-product__rate-list" data-product-id="{{$product->id}}" data-product-url="{{route('product-rate.store')}}">
        <i data-index-rate="1" class="far fa-star active"></i>
        <i data-index-rate="2" class="far fa-star active"></i>
        <i data-index-rate="3" class="far fa-star active"></i>
        <i data-index-rate="4" class="far fa-star active"></i>
        <i data-index-rate="5" class="far fa-star active"></i>
    </div>
@endif
@push('scripts')
    <script async src="{{ asset('js/product/rate.js') }}?v=<?= filemtime('js/product/rate.js') ?>"></script>
@endpush
