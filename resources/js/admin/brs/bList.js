import {removeCol, translatorTableColumn} from "../base";

$(document).ready(function () {
    // document.getElementById('loading').classList.remove('hide')
    $("#bannerSave").submit(function (e) {
        e.preventDefault();
        $(".alert").removeClass("d-block").addClass("d-none");
        $(".infoError").text("");
        let actionUrl = e.currentTarget.action;
        $.ajax({
            async: false,
            method: 'POST',
            url: actionUrl,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: $("#bannerSave").serialize(),
            beforeSend: function () {
            },
            success: function (data) {
                if (data.status === "success") {
                    swal({
                        title: 'Успех!',
                        text: 'Запись успешно добавлена!',
                        type: "success",
                        confirmButtonText: "OK",
                        showCancelButton: false
                    }).then((result) => {
                        $("#bannerSave")[0].reset();
                        window.location.href = '/admin/banner/list';
                    });

                }

            },
            error: function (data) {
                try {
                    errorsToDb(data);
                } catch (e) {

                }
            }
        });

    });

    /******************обновление баннера******************************************************************************/
    $("#bannerUpdate").submit(function (e) {
        e.preventDefault();
        $(".alert").removeClass("d-block").addClass("d-none");
        $(".infoError").text("");
        $.ajax({
            method: 'PUT',
            url: e.currentTarget.action,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: $("#bannerUpdate").serialize(),
            beforeSend: function () {
            },
            success: function (data) {
                swal({
                    title: 'Успех!',
                    text: 'Запись успешно добавлена!',
                    type: "success",
                    confirmButtonText: "OK",
                    showCancelButton: false
                }).then((result) => {
                    $("#bannerUpdate")[0].reset();
                    try {
                        window.location.href = e.currentTarget.action.replace('update', 'edit');
                    } catch (e) {
                    }

                });
            },
            error: function (data) {
                try {
                    errorsToDb(data);
                } catch (e) {

                }


            }
        });

    });

    /******************быстрые уведомления******************************************************************************/
    formSend("#QOfferCR");

    $(document).on("click", ".close", function (e) {
        e.preventDefault();
        $(this).parent().removeClass("d-block").addClass('d-none')
    });

    // Call the dataTables jQuery plugin
    $(document).ready(function () {
        $('#dataTable').DataTable(translatorTableColumn());
    });

    function detectTrTable() {

        let elementById = document.getElementById('dataTable');

        if (elementById) {
            let elementTbody = elementById.querySelector('tbody');
            let elementAllTr = elementTbody.querySelectorAll('tr');

            let clickToTr = function (e) {
                let trElement = e.target.parentElement;
                if (trElement.classList.contains('even') || trElement.classList.contains('odd')) {

                    if (trElement.getAttribute('data-id')) {
                        let bannerId = trElement.getAttribute('data-id');
                        const url = `/admin/banner/edit/${bannerId}`;
                        document.location.href = url
                    }
                }
            };
            [].slice.call(elementAllTr).forEach((element) => element.addEventListener('dblclick', clickToTr));
        }

    }

    detectTrTable();
});


function errorsToDb(data) {
    let datum = data["responseJSON"].errors;
    Object.keys(datum).map((fields) => {

        let htmlElement = document.getElementById(fields);
        if (fields === 'active') {
            let active = htmlElement.parentElement.parentElement;
            let alert = $(active).find('.alert');
            $(alert).find(".infoError").text(datum[fields]);
            $(alert).addClass("d-block");
        } else if (fields === 'gallery') {
            let mainGallery = $('#document-dropzone');
            let alert = $(mainGallery).siblings(".alert");
            $(alert).find(".infoError").text(datum[fields]);
            $(alert).addClass("d-block");
        } else {
            if (htmlElement) {
                let alert = $(htmlElement).siblings(".alert");
                $(alert).find(".infoError").text(datum[fields]);
                $(alert).addClass("d-block");
            }
        }
    });
}

function formSend(idForm) {

    $(idForm).submit(function (e) {
        e.preventDefault();
        $(".alert").removeClass("d-block").addClass("d-none");
        $(".infoError").text("");
        $.ajax({
            method: e.currentTarget.method,
            url: e.currentTarget.action,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: $(this).serialize(),
            beforeSend: function () {
            },
            success: function (data) {
                swal({
                    title: 'Успех!',
                    text: 'Запись успешно добавлена!',
                    type: "success",
                    confirmButtonText: "OK",
                    showCancelButton: false
                }).then((result) => {
                    $("#QOfferCR")[0].reset();
                    console.log(result);
                    // try {
                    //     window.location.href = e.currentTarget.action.replace('update', 'edit');
                    // } catch (e) {
                    // }

                });
            },
            error: function (data) {
                try {
                    errorsToDb(data);
                } catch (e) {

                }


            }
        });
    });

    (() => {
        if (document.getElementsByClassName('delete')[0]) {
            let elementsByClassName = document.getElementsByClassName('delete');
            elementsByClassName[0].addEventListener('click', (event) => {
                let eventTarget = event.target;
                let deleteId = eventTarget.getAttribute('attr-delete');
                let route = eventTarget.getAttribute('attr-route');
                removeCol(route, {id: deleteId}, "POST", "Успешно удаленно!", '/admin/banner/list');
            });
        }
    })();
}

