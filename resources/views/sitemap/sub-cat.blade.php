<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    @foreach ($subCats as $subCat)
        @if($subCat instanceof \App\Subcategories)
            @if($subCat->category)
                <url>
                    <loc>{{url("/catalog")}}/{{$subCat->category->seo_name}}/{{ $subCat->seo_name }}</loc>
                    <lastmod>{{ $subCat->updated_at->toAtomString() }}</lastmod>
                    <changefreq>monthly</changefreq>
                    <priority>0.8</priority>
                </url>
            @endif
        @endif
    @endforeach
</urlset>
