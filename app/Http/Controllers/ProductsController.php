<?php

namespace App\Http\Controllers;

//use App\Helper\Translator;
use App\Answers;
use App\Helper\ValidNotNuZero;
use App\Http\Controllers\Admin\BaseController;
use App\Pictures;
use App\ProductCategory;
use App\Products;
use App\Rules\SelectChange;
use App\Seo;
use App\Subcategories;
use Djmitry\Meta\Meta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Translators;

class ProductsController extends BaseController
{
    const PUBLIC_PATCH = "images/product/pictures/";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $isCreate = true;

        $items = ProductCategory::pluck('category_name', 'id');
        $items[0] = 'Выбрать бренд';
//        $items->prepend('Выбрать бренд');
//
        $categories = $items;

        $model = new Products();
        return view('admin/product/create', compact('isCreate', 'model', 'categories'))->withTitle('Добавить новый товар');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function checkByIds(Request $request)
    {
        if ($request->ajax()) {
            $ids = $request->input('productId');
            $prodList = DB::table('products')->select('id')->where(function ($q) use ($ids) {
                $q->whereIn('id', $ids)->where('is_sold', '0')->where('status_available', '1');
            })->get();
            $collectIds = collect($prodList);
            $collectIds = $collectIds->map(function ($item, $key) {
                return "productId:" . $item->id;
            })->all();
            return response()->json($collectIds);
        }
        return response()->json(['data' => false]);
    }

    public function by_ids(Request $request)
    {

        if ($request->ajax()) {
            if ($request->isMethod('post')) {
//                return view('/components/purchase-item');
                $rules = [
                    'data' => 'required|min:5',
                ];
                $validator = Validator::make($request->all(), $rules);
                if ($validator->fails()) {
                    return response()->json(['data' => [], 'msg' => 'empty']);
                } else {

                    $result = $request->all();
                    $data = $result["data"];
                    $collection = collect(json_decode($data));
                    $newCollection = $collection->map(function ($productID) {
                        return (int)explode(":", $productID)[1];
                    })->toArray();;

                    $productsList = Products::whereIn('id', $newCollection)
                        ->where('is_sold', '0')
                        ->where('status_available', '1')
                        ->with("pictures")->get();
//                    if ($productsList[0] instanceof Products) {
                    if (count($productsList) > 0) {
                        return view('/components/purchase-item', compact('productsList'));
                    } else {
                        return response()->json(['data' => [], 'msg' => 'empty']);
                    }
                }

            }
        }
        return view('errors.503');

    }

    /**
     * Display the specified resource.
     *
     * @param string $seoName
     * @return \Illuminate\Http\Response
     */
    public function show($seoName)
    {

        $title = $seoName;
        $askModel = new Answers();
        $product = Products::with(['pictures' => function($query) {
            $query->orderBy('id', 'desc');
        }, 'category', 'subcategory', 'bindings' => function ($query) {
            $query->with('prodbind')->get();
        }])
            ->where('seo_name', $seoName)->first();
        if ($product instanceof Products) {
            $seo = Seo::select('meta_title', 'meta_desc', 'meta_keywords')
                ->where('product_id', $product->id)
                ->get()
                ->first();
            $metaTitle = str_replace(' ', '-', $product->name);
            $meta = [
                'title' => $metaTitle,
                'og:title' => $metaTitle,
                'og:url' => url()->current(),
                'og:site_name' => env('APP_NAME'),
                'og:image:width' => 1200,
                'og:image:height' => 630,
                'robots' => 'index, follow'
            ];
            if ($seo instanceof Seo) {

                $meta = array_merge($meta, [
                    'description' => Seo::convertedBase64($seo->meta_desc),
                    'keywords' => Seo::convertedBase64($seo->meta_keywords),
                    'og:description' => Seo::convertedBase64($seo->meta_desc),
                ]);
                if (count($product->pictures) > 0) {
                    $meta = array_merge($meta, [
                        'og:image' => asset('/images/product/pictures') . '/thumbnails/' . $product->pictures[0]
                                ->image_list
                    ]);
                }
            }
            Meta::add($meta);
            return view('product.details.more-details', compact('title', 'product', 'askModel'));
        } else {
            return view('errors.404-errors')->withTitle('Page not found!');
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $isCreate = false;
        $items = ProductCategory::pluck('category_name', 'id');
        $items[0] = 'Выбрать бренд';
//        $items->prepend('Выбрать бренд');
        $categories = $items;
        $model = Products::where('id', $id)->first();

        if ($model instanceof Products) {
            return view('admin/product/create', compact('model', 'categories', 'isCreate'))
                ->withTitle('Добавить новый товар');

        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function update(Request $request, $id)
    {
//        $serviceFirst = App::make('saveLog');
        $messages = [
            'category_id.required' => 'Поле "Категория.',
            'name.required' => 'Поле "Название" обязательно.',
            'seo_name.required' => 'Поле "SEO" обязательно.',
            'desc.required' => 'Поле "Описание" обязательно.',
            'price.required' => 'Поле "Цена" обязательно.',
            'name.min' => 'Минимум 3 символа.',
            'seo_name.min' => 'Минимум 3 символа.',
        ];
        $requestData = $request->all();
        if ($requestData["subcategory_id"] === "null") {
            $requestData["subcategory_id"] = null;
        }
        Validator::make($requestData, [
            'category_id' => ['required', new ValidNotNuZero],
            'desc' => 'required',
            'price' => 'required',
            'name' => 'required|min:3',
            'seo_name' => 'required|min:3'
        ], $messages)->validate();
        $model = Products::where('id', (int)$id)->first();
        if ($model instanceof Products) {
            $pictureModels = Pictures::where('product_id', (int)$id)->get();
            if (isset($requestData["gallery"])) {
                $galleryLists = $requestData["gallery"];
                $indexCollect = [];
                foreach ($pictureModels as $pictureModel) {
                    $indexCollect[$pictureModel->image_list] = $pictureModel->position_index;
                    if ($pictureModel instanceof Pictures) {
                        if (in_array($pictureModel->image_list, $galleryLists)) {
                            $pictureModel->delete();
                        } else {
                            $this->pictureDeleteTbl($pictureModel);
                        }
                    }
                }
                if (is_array($galleryLists)) {
                    foreach ($galleryLists as $galleryList) {
                        $pictures = new Pictures();
                        $pictures->product_id = $model->id;
                        if (!empty($indexCollect[$galleryList]) && isset($indexCollect[$galleryList])) {
                            $pictures->position_index = $indexCollect[$galleryList];
                        } else {
                            $pictures->position_index = rand(100, 150);
                        }

                        $pictures->image_list = $galleryList;
                        if ($pictures->save()) {
                            $this->saveImageMakeThumbnails([$galleryList], self::PUBLIC_PATCH);
                        }
                    }
                }
            } else {
                foreach ($pictureModels as $pictureModel) {
                    $this->pictureDeleteTbl($pictureModel);
                }
            }
            if ($model->options === $requestData["options"]) {
                unset($requestData["options"]);
            }
            if ($model->update($requestData)) {
                return response()->json([
                    'msg' => true,
                ], 200);
            } else {
                return response()->json([
                    'msg' => false,
                ], 404);
            }
        }
    }

    public function positionPictures($product_id)
    {
        $pictures = Pictures::where('product_id', $product_id)->orderBy('position_index', 'ASC')->get();
        return view('admin.product.position', compact('pictures'))->withTitle("Позиционирование изображений");
    }

    public function positionPUpdate(Request $request)
    {
        $dataLists = $request->all();
        foreach ($dataLists as $pictureId => $position_index) {
            $pictureModel = Pictures::where('id', $pictureId)->first();
            if ($pictureModel instanceof Pictures) {
                $pictureModel->position_index = $position_index;
                $pictureModel->update();
            }
        }
        return response()->json([
            'msg' => true,
        ], 200);
    }

    public function productSave(Request $request)
    {
        $messages = [
            'category_id.required' => 'Поле "Категория.',
            'name.required' => 'Поле "Название" обязательно.',
            'seo_name.required' => 'Поле "SEO" обязательно.',
            'desc.required' => 'Поле "Описание" обязательно.',
            'price.required' => 'Поле "Цена" обязательно.',
            'subcategory_id.required' => 'Поле "Подкатегория" обязательно.',
            'name.min' => 'Минимум 3 символа.',
            'seo_name.min' => 'Минимум 3 символа.',
        ];
        $requestData = $request->all();
        if ($requestData["subcategory_id"] === "null") {
            $requestData["subcategory_id"] = null;
        }

        Validator::make($requestData, [
            'category_id' => ['required', new ValidNotNuZero],
            'desc' => 'required',
            'price' => 'required',
            'name' => 'required|min:3',
            'seo_name' => 'required|min:3',
            'subcategory_id' => ['required', new SelectChange]
        ], $messages)->validate();
        $product = new Products();
        if ($id = $product->create($requestData)->id) {
            if (isset($requestData["gallery"])) {
                $data = [];
                $galleryLists = $requestData["gallery"];
                $ind = 1;
                foreach ($galleryLists as $galleryList) {
                    $pictures = new Pictures();
                    $pictures->product_id = $id;
                    $pictures->image_list = $galleryList;
                    $pictures->position_index = $ind;
                    $data[] = $pictures;
                    if ($pictures->save()) {
                    }
                    $ind++;
                }
                $this->saveImageMakeThumbnails($galleryLists, self::PUBLIC_PATCH);
            }
            return response()->json([
                'status' => 'success',
                'msg' => 'Запись успешно добавлена!'
            ]);
        }
    }

    /*********************** список товаров ********************************/
    public function productList()
    {
        $skipFields = collect([
            "category_id", "data", "category_id", "subcategory_id", "condition", "shipping", "options", "video_url",
            "created_at", "updated_at"
        ]);
        $productsModels = Products::with(['pictures', 'category', 'subcategory'])
            ->orderBy('products.id')->get()->toArray();
        $products = [];
        $i = 0;
        if ($productsModels) {
            foreach ($productsModels as $productsModel) {
                $std = new Products();
                foreach ($productsModel as $key => $sModel) {
                    if (!$skipFields->search($key)) {
                        if ($key === "category") {
                            $std->$key = $sModel["category_name"];
                        } else if ($key === "subcategory") {
                            $std->$key = $sModel["name"];
                        } else if ($key == "pictures") {
                            $std->$key = $sModel;
                        } else {
                            $std->$key = $sModel;
                        }
                    }
                }

                $products[$i] = $std;
                $i++;
            }
            if ($products[0] instanceof Products) {
                $productsTitle = collect($products[0])->toArray();
                $productsTitle = array_keys($productsTitle);
                return view('admin/product/list', compact("productsTitle", "products"))
                    ->withTitle("Список товаров");
            } else {
                return view('errors.404-errors')->withTitle('Page not found!');
            }
        } else {
            $data = ["productsTitle" => null, "products" => null];
            return view('admin/product/list', $data)
                ->withTitle("Список товаров");
        }

    }

    public function destroy($id)
    {
        return response()->json(['id' => $id]);
    }

    public function dataImgDelete(Request $request)
    {
        $path = storage_path('tmp/uploads');
        $fileResp = $request->request->get('name');
        $productId = $request->request->get('product_id');
        $filePath = public_path() . "/" . self::PUBLIC_PATCH;
        $fileThumbnails = $filePath . 'thumbnails/';
        $filePreview = $filePath . 'preview/';
        $collection = collect(json_decode($fileResp, true));
        if ($collection['name']) {
            File::delete($path . '/' . $collection['name']);
            if (isset($productId)) {
                $productPictures = Pictures::where('product_id', $productId)
                    ->where('image_list', $collection['name'])
                    ->orWhere('image_list', $collection['original_name'])
                    ->one();
                if ($productPictures instanceof Pictures) {
                    if ($productPictures->delete()) {
                        if (file_exists($filePreview . $collection['name'])) {
                            File::delete($filePreview . $collection['name']);
                        }
                        if (file_exists($fileThumbnails . $collection['name'])) {
                            File::delete($fileThumbnails . $collection['name']);
                        }
                        if (file_exists($filePath . $collection['name'])) {
                            File::delete($filePath . $collection['name']);
                        }
                    }
                }
            }
            return response()->json(['status' => $collection['name']]);
        }
        return response()->json(['status' => false]);
    }

    public function findFilesByProductId($productId)
    {
        $path = '/' . self::PUBLIC_PATCH;
        $pictures = Pictures::where("product_id", $productId)->get();
        $myFile = [];
        if ($pictures[0] instanceof Pictures) {
            foreach ($pictures as $key => $model) {

                if (strlen($model->image_list) > 3) {
                    if (file_exists(public_path() . $path . $model->image_list)) {

                        $fileName = $path . $model->image_list;
                        $shortName = $model->image_list;
                        $model->gallery = public_path() . $path . $model->image_list;
                        $myFile[$key]['name'] = $fileName;
                        $myFile[$key]['shortName'] = $shortName;
                        $myFile[$key]['size'] = filesize($model->gallery);
                    }
                }
            }
            return response()->json($myFile);
        }
        return false;

    }

    /************запрос для наполения полкатегорий ************************
     * @param $categoryId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSubcategoryByCategoryId($categoryId)
    {
        $subcategory = Subcategories::select('id', 'name')->where('category_id', (int)$categoryId)->get()->toArray();
        return response()->json($subcategory);
    }

    public function delete(Request $request)
    {
        $requestData = $request->all();
        $model = Products::find($requestData['id']);
        if ($model instanceof Products) {
            $pictureModels = Pictures::where('product_id', (int)$model->id)->get();
            foreach ($pictureModels as $pictureModel) {
                $this->pictureDeleteTbl($pictureModel);
            }
            if ($model->delete()) {
                return response()->json([
                    'msg' => true,
                ], 200);
            } else {
                return response()->json([
                    'msg' => false,
                ], 404);
            }
        }
    }

    /**
     * @param Pictures $pictureModel
     * @throws \Exception
     */
    private function pictureDeleteTbl(Pictures $pictureModel): void
    {
        if ($pictureModel->delete()) {
            $filePath = public_path() . "/" . self::PUBLIC_PATCH;
            $fileThumbnails = $filePath . 'thumbnails/';
            $filePreview = $filePath . 'preview/';
            if (file_exists($filePreview . $pictureModel->image_list)) {
                File::delete($filePreview . $pictureModel->image_list);
            }
            if (file_exists($fileThumbnails . $pictureModel->image_list)) {
                File::delete($fileThumbnails . $pictureModel->image_list);
            }
            if (file_exists($filePath . $pictureModel->image_list)) {
                File::delete($filePath . $pictureModel->image_list);
            }
        }
    }
}
