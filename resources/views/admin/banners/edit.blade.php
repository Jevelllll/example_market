@extends('admin.layouts.admin_template')
@section('content')
    <!-- Modal body -->
    <div class="modal-body">

        <div class="card shadow mb-4">
            <h1 class="h3 mb-4 text-gray-800 text-center pt-3">Редактирование {{$titleName}}</h1>
            <div class="row justify-content-center">

                {{Form::model($banner, array('route' => array('banner.update', $banner->id), 'id'=>'bannerUpdate', 'method' => 'PUT', 'enctype'=>'multipart/form-data')) }}
                <div class="form-group">
                    {{Form::label('name', 'Название', ['for' => 'name'])}}
                    {{Form::text('name',  null, ['id' => 'name', 'class' => 'form-control'])}}
                    <div class="alert alert-danger alert-dismissible d-none">
                        <button type="button" class="close">&times;</button>
                        <strong class="infoError"></strong>
                    </div>
                </div>
                <div class="form-group">
                    {{Form::label('redirect_url', 'Адресс переадресации')}}
                    {{Form::text('redirect_url', null, ['id' => 'redirect_url', 'class' => 'form-control'])}}

                    <div class="alert alert-danger alert-dismissible d-none">
                        <button type="button" class="close">&times;</button>
                        <strong class="infoError"></strong>
                    </div>
                </div>

                <div class="form-group active updateActive">
                    {{Form::label('Статус')}}

                    <div class="form-check">
                        <input type="radio" id="active" name="active"
                               value="1" {{ ($banner->active==1)? "checked" : "" }} >
                        {{Form::label('active', 'Активен')}}
                    </div>
                    <div class="form-check">
                        <input type="radio" id="not_active" name="active"
                               value="0" {{ ($banner->active==0)? "checked" : "" }} >
                        {{Form::label('not_active', 'Не активно')}}
                    </div>
                    <div class="alert alert-danger alert-dismissible d-none">
                        <button type="button" class="close">&times;</button>
                        <strong class="infoError"></strong>
                    </div>
                </div>
                <div class="form-group">
                    {{Form::label('desc', 'Описание', ['for' => 'description'])}}
                    {{Form::textarea('desc', null, ['id' => 'desc','class'=> 'form-control', 'rows' => 3])}}
                    <div class="alert alert-danger alert-dismissible d-none">
                        <button type="button" class="close">&times;</button>
                        <strong class="infoError"></strong>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label for="gallery">Изображение</label>
                    <div class="needsclick dropzone" id="document-dropzone">
                    </div>
                    <div class="alert alert-danger alert-dismissible d-none">
                        <button type="button" class="close">&times;</button>
                        <strong class="infoError"></strong>
                    </div>
                </div>

                {{Form::submit('Обновить', ['class' => 'btn btn-form btn-success shadow-custom text-uppercase border-radius-50 btn-lg w-100 mt-2 mb-3'])}}
                {{ Form::close() }}
            </div>
            <button class="delete ml-auto btn btn-danger mb-5 float-right"
                    attr-delete="{{$banner->id}}"
                    attr-route="{{route("banner.delete")}}">Удалить&nbsp;<i class="far fa-trash-alt"></i></button>
        </div>
    </div>


@endsection



@push('scripts')
    <script src="{{asset('/js/admin/brs/bList.js')}}"></script>
    <script>
        var uploadedDocumentMap = {}
        Dropzone.options.documentDropzone = {

            url: '{{ route('projects.storeMedia') }}',
            maxFilesize: 2, // MB
            maxFiles: 1,
            uploadMultiple: false,
            addRemoveLinks: true,
            // acceptedFiles: ".jpeg,.jpg,.png,.gif",
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            success: function (file, response) {
                $('#bannerUpdate').append('<input type="hidden" name="gallery[]" value="' + response.name + '">')
                uploadedDocumentMap[file.name] = response.name
            },
            sending: function () {
                $('.dz-image-preview').remove();
            },
            init: function () {
                    @if(isset($project) && $project->gallery)
                var files =
                {!! json_encode($project->gallery) !!}
                    for (var i in files) {
                    var file = files[i]
                    console.log(file);
                    this.options.addedfile.call(this, file)
                    file.previewElement.classList.add('dz-complete')
                    $('#bannerUpdate').append('<input type="hidden" name="gallery[]" value="' + file.file_name + '">')
                }
                @endif
                    this.on("maxfilesexceeded", function (file) {
                    this.removeAllFiles();
                    this.addFile(file);
                });


                this.on('addedfile', function (file) {

                    var preview = document.getElementsByClassName('dz-preview');
                    preview = preview[preview.length - 1];

                    var imageName = document.createElement('span');
                    imageName.innerHTML = file.name;

                    // preview.insertBefore(imageName, preview.firstChild);

                });
                let url = '/admin/banner/banners-files/{{$banner->id}}';
                var thisDropzone = this;
                $.getJSON(url, function (data) { // get the json response
                    $.each(data, function (key, value) { //loop through it
                        if (value.hasOwnProperty('shortName')) {
                            var mockFile = {name: value.shortName, size: value.size}; // here we get the file name and size as response
                            thisDropzone.options.addedfile.call(thisDropzone, mockFile);
                            thisDropzone.options.thumbnail.call(thisDropzone, mockFile, value.name);//uploadsfolder is the folder where you have all those uploaded files
                        }
                    });

                });
            },
            removedfile: function (file) {
                var foolFileName = file.xhr.responseText;
                file.previewElement.remove()
                var name = ''
                if (typeof file.file_name !== 'undefined') {
                    name = file.file_name
                } else {
                    name = uploadedDocumentMap[file.name]
                }

                let mainGallery = $('#document-dropzone');
                let alert = $(mainGallery).siblings(".alert");
                $(alert).find(".infoError").text('Тип файла не изображение');
                $(alert).removeClass("d-block");

                $.ajax({
                    url: '{{ route('projects.delete')}}',
                    type: 'post',
                    data: {
                        name: foolFileName
                    },
                    headers: {
                        'X-CSRF-TOKEN': "{{ csrf_token() }}"
                    },
                    dataType: 'json',
                    success: function (data) {
                        console.info(data);
                    }
                });

                $('form').find('input[name="gallery[]"][value="' + name + '"]').remove()
            },
        }
        Dropzone.prototype.defaultOptions.dictRemoveFile = "Удалить файл";
    </script>
@endpush


