@extends('landing.layouts.app')
@section('content')
    <div class="container info-rule pb-1 mb-3">
        <h1 class="text-center h-90 mt-5 mb-5 product-landing_picture purple-gr">{{$model->title}}</h1>
        <div class="info-rule-content mb-5">
            {!!$model->desc!!}
        </div>
    </div>
@endsection
