<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Orders;
use Illuminate\Support\Facades\Auth;
class AdminController extends Controller
{
    public function index()
    {
        $orderModel = Orders::getNewOrder();
        return view('admin/main/index',  compact('orderModel'))->withTitle('Main');
    }
    public function getNewOrder() {
        return response()->json(
            [
                'navbars_alerts' => view('admin.particals.navbas_alerts')->render(),
                'navbar_answer' => view('admin.particals.navbar_answer')->render()
            ], 200);
    }
    public function logout()
    {
        Auth::logout();
        return redirect('/admin');
    }

}
