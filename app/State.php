<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer id
 * @property integer country_id
 * @property string name
 */
class State extends Model
{
    protected $table = 'states';
    protected $fillable = [
        "created_at", "updated_at", "name", "country_id"
    ];
    public function countries() {
        return $this->hasOne('App\Countries', 'idMap', 'country_id');
    }
    public function city() {
        return $this->hasMany('App\City', 'state_id', 'id');
    }
}
