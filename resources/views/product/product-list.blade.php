<div class="product-wrap">
    <div class="product-landing">
        <div class="product-landing_picture p-4 purple-gr">
            @if($data['productsList']->currentPage() == 1)
                <h1>{{$data['hName']['name']}}</h1>
            @else
                <h1>{{$data['hName']['name']}} - Page {{$data['productsList']->currentPage()}}</h1>
            @endif

        </div>
        <div class="product-landing_menu justify-content-center" id="menuProduct">
            @foreach($data['categories'][0]->subcategory as $subcategory)

                @if($subcategory->seo_name == $data['hName']['seo_name'])
                    <div class="product-landing_menu_item active">
                        <a href="/catalog/{{strtolower($data["categories"][0]->seo_name)}}/{{strtolower($subcategory->seo_name)}}">
                            {{$subcategory->name}}
                        </a>
                    </div>
                @else
                    @if($subcategory->products)
                        <div class="product-landing_menu_item">
                            <a href="/catalog/{{strtolower($data["categories"][0]->seo_name)}}/{{strtolower($subcategory->seo_name)}}">
                                {{$subcategory->name}}
                            </a>
                        </div>
                    @endif
                @endif
            @endforeach
        </div>

        <div class="row w-100 product-landing_max-width mb-5" id="fool-products">
            @foreach($data['productsList'] as $product)
                @include('/product/product-card', compact('product'))
            @endforeach
        </div>

        @if(isset($data["categories"][0]->down_desc) && !empty($data["categories"][0]->down_desc))
        <div class="pl-3 pr-3">
            <div class="divider"></div>
            <h3 class="down-desc">
               {!! $data["categories"][0]->down_desc !!}
            </h3>
            <div class="divider"></div>
        </div>
        @endif
        <div id="paginationContainer">
            {{ $data['productsList']->links() }}
        </div>
    </div>

</div>
@push('scripts')
    <script async src="{{ asset('js/product/product.js') }}?v=<?= filemtime('js/product/product.js') ?>"></script>
@endpush
