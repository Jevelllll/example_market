<?php

namespace App\Http\Controllers\site_map;

use App\Http\Controllers\Controller;
use App\ProductCategory;
use App\Products;
use App\Subcategories;

class SitemapController extends Controller
{
    public function index()
    {
        $brand = ProductCategory::orderBy('updated_at', 'desc')->select('updated_at', 'seo_name')->first();
        $subCat = Subcategories::orderBy('updated_at', 'desc')->select('updated_at', 'seo_name')->first();
        $product = Products::orderBy('updated_at', 'desc')->select('updated_at', 'seo_name')->first();
        return response()->view('sitemap.index', compact('brand', 'subCat', 'product'))
            ->header('Content-Type', 'text/xml');
    }

    public function brand()
    {
        $brands = ProductCategory::orderBy('updated_at', 'desc')->select('updated_at', 'seo_name')->get();
        return response()->view('sitemap.brand', compact('brands'))
            ->header('Content-Type', 'text/xml');
    }

    public function subCat()
    {
        $subCats = Subcategories::orderBy('updated_at', 'desc')->get();
        return response()->view('sitemap.sub-cat', compact('subCats'))
            ->header('Content-Type', 'text/xml');
    }

    public function product()
    {
        $products = Products::orderBy('updated_at', 'desc')->select('updated_at', 'seo_name')->get();
        return response()->view('sitemap.product', compact('products'))
            ->header('Content-Type', 'text/xml');
    }
}
