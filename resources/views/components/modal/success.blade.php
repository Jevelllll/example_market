<div class="modal-success">
    <div class="modal-content-success">
        <span class="close-purchare">&times;</span>
        <div class="purchase-bottom-line">
            <h4>{{session('success')}}</h4>
        </div>
    </div>
</div>
<script>
    function clearLocalStorage() {
        localStorage.setItem('purchase', JSON.stringify([]));
    }
    clearLocalStorage();
</script>
