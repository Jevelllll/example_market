@extends('admin.layouts.admin_template')
@section('content')
    <span class='tegs' tegs="/admin/country/edit/"></span>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary pt-3 ml-md-1">Список стран PayPal</h6>
            <a class="btn btn-md btn-success mt-2" href="{{route('new.country')}}">Добавить страну</a>
        </div>
        @if($countriesList)
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            @foreach($countriesTitle as $titles)
                                <th>{{Translators::translateRow()[$titles]}}</th>
                            @endforeach
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            @foreach($countriesTitle as $titles)
                                <th>{{Translators::translateRow()[$titles]}}</th>
                            @endforeach
                        </tr>
                        </tfoot>
                        <tbody>
                        @foreach($countriesList as $countries)
                            <tr data-id="{{$countries->id}}">
                                @foreach($countriesTitle as $key => $titles)
                                        <td>{{$countries->$titles}}</td>
                                @endforeach
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        @else
            <div style="font-size: 2.5rem" class="text-center">Отсутствуют страны PayPal</div>
        @endif
    </div>
@endsection
@push('scripts')
    <script src="{{asset('/js/admin/countries/countriesList.js')}}"></script>
@endpush
