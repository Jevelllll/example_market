<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subcategories extends Model
{
    protected $table = "subcategories";
    protected $fillable = ["name", "img", "id", "seo_name", "created_at", "updated_at", "category_id"];
//    public function products()
//    {
//        return $this->belongsTo('Initiales\Products');
//    }

    public function products()
    {
        return $this->hasOne('App\Products', 'subcategory_id', 'id')->orderBy('created_at','DESC');
    }

    public function category()
    {
        return $this->hasOne('App\ProductCategory', 'id', 'category_id');
    }

    public function get_products()
    {
        return 2222;
    }
}
