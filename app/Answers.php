<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer id
 * @property integer product_id
 * @property integer parent_id
 * @property string to
 * @property string from
 * @property string message
 * @property string viewed
 * @property string type
 */
class Answers extends Model
{
    protected $table = "answers";
    protected $fillable = ["to","from","message","viewed","type","created_at", "product_id", "updated_at", "parent_id"];

    public function product() {
        return $this->hasOne('App\Products', 'id', 'product_id');
    }

    public function parent() {
        return $this->hasOne('App\Answers', 'parent_id', 'id')->orderBy('id', 'desc');
    }
}
