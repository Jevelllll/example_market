import {addClass, viewErrorsAfterSubmit} from "./common";
import {Spinner} from "../my-component/spinner";

let hidePreSuccess = document.querySelectorAll('.hide-pre-success');
let successSendMsg = document.querySelectorAll('.success-send-msg');

class Ask_question {
    constructor() {
        this.asqElement = document.querySelector('.modal-asqQuestion');
        this.closeAsqQuestion = document.querySelector('.close-asqQuestion');
        this.askButton = document.getElementById('ascQw');
        this.askClose = document.querySelector('.close-asqQuestion');
    }

    toggle(isSee) {
        if (hidePreSuccess) {
            hidePreSuccess.forEach((elHide) => {
                elHide.classList.remove('d-none');
            });
        }
        successSendMsg.forEach((elMesh) => {
            elMesh.classList.remove('d-none');
            elMesh.classList.remove('d-flex');
            elMesh.classList.add('d-none');
        })
        let removeClassList = ['d-flex', 'd-none'];
        removeClassList.map((className) => this.asqElement.classList.remove(className));
        this.isAsk = isSee;
        this.viewClass = this.isAsk ? 'd-flex' : 'd-none';
        this.asqElement.classList.add(this.viewClass);
    }

    hideAskModalOtherClick(ev) {
        let target = ev.target;
        if (!this.askButton.contains(target)) {
            if (!this.asqElement.querySelector('.modal-content-asqQuestion').contains(target)) {
                this.toggle(false);
            }
        }
    }

    run() {
        if (this.asqElement && this.closeAsqQuestion && this.askButton && this.askClose) {
            this.askButton
                .addEventListener('click', () => this.toggle(true));
            this.askClose
                .addEventListener('click', () => this.toggle(false));
            document.addEventListener('click', (ev) => this.hideAskModalOtherClick(ev));
        }
    }
}

document.addEventListener('DOMContentLoaded', function (e) {
    let token = document.querySelector("meta[name='csrf-token']");
    let tokenContent = token.getAttribute('content');
    let askForm = document.getElementById('askModel');
    if (!askForm) return false;
    if (token) {
        askForm.addEventListener('submit', (e) => {
            e.preventDefault();
            Spinner();
            Spinner.show();
            let targetForm = e.target;
            const method = targetForm.getAttribute('method');
            const url = targetForm.getAttribute('action');
            const httpRequest = new XMLHttpRequest();
            httpRequest.open(method, url, true);
            httpRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            httpRequest.setRequestHeader("X-CSRF-TOKEN", tokenContent);
            httpRequest.setRequestHeader("X-Requested-With", "XMLHttpRequest");
            httpRequest.responseType = 'json';
            httpRequest.send(targetForm.serialize());
            httpRequest.onload = () => {
                let response = httpRequest.response;
                if (httpRequest.status === 200) {
                    targetForm.reset();
                    let commentMessage = targetForm.querySelector('#message');
                    commentMessage.classList.remove('mui--is-not-empty');
                    commentMessage.classList.add('mui--is-empty');
                    let mailMessage = targetForm.querySelector('#from')
                    mailMessage.classList.remove('mui--is-not-empty');
                    mailMessage.classList.add('mui--is-empty');
                    let elementError = document.querySelectorAll('.error-message');
                    elementError.forEach((el) => {
                        el.classList.remove('d-none');
                        el.classList.remove('d-flex');
                        el.classList.add('d-none');
                    });
                    hidePreSuccess.forEach((elHide) => {
                        addClass(elHide, 'd-none', () => {
                        });
                    });
                    successSendMsg.forEach((elMesh) => {
                        elMesh.classList.remove('d-none');
                        elMesh.classList.remove('d-flex');
                        elMesh.classList.add('d-flex');
                    })
                    Spinner.hide();
                } else {
                    successSendMsg.forEach((elMesh) => {
                        elMesh.classList.remove('d-none');
                        elMesh.classList.remove('d-flex');
                        elMesh.classList.add('d-none');
                    })
                    let elementError = document.querySelectorAll('.error-message');
                    elementError.forEach((el) => {
                        el.classList.remove('d-none');
                        el.classList.remove('d-flex');
                        el.classList.add('d-none');
                    })
                    viewErrorsAfterSubmit(response, askForm, true);
                    Spinner.hide();
                }
            }
            httpRequest.onerror = () => {
                Spinner.hide();
            }
        });
    }
    return false;
});

let askModel = new Ask_question();
askModel.run();

