@extends('admin.layouts.admin_template')
@push('style')
    <link href="{{asset('/css/admin/order_truns.css')}}" rel="stylesheet">
@endpush
@section('content')
    <span class='tegs' tegs="/admin/seo/edit/"></span>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary pt-3">Список SEO</h6>
        </div>
        @if($seo)
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            @foreach($seoTitle as $titles)
                                @if($titles === 'created_at' || $titles === 'updated_at' || $titles === 'id')
                                @else
                                    <th>{{Translators::translateRow()[$titles]}}</th>
                                @endif
                            @endforeach
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            @foreach($seoTitle as $key => $titles)
                                @if($titles === 'created_at' || $titles === 'updated_at' || $titles === 'id')
                                @else
                                    <th>{{Translators::translateRow()[$titles]}}</th>
                                @endif
                            @endforeach
                        </tr>
                        <tfoot>
                        <tbody>
                        @foreach($seo as $se)
                            <tr data-id="{{$se->id}}">
                                @foreach($seoTitle as $key => $titles)
                                    @if($titles === 'created_at' || $titles === 'updated_at' || $titles === 'id')
                                    @elseif($titles === 'product_id')
                                        @if($se->product)
                                            <td class="text-center">
                                                <span class="label label-success">
                                                 {{($se->product->name)}}
                                                    </span>
                                            </td>
                                        @else
                                            <td class="text-center"><span class="label label-warning">пропуск</span>
                                            </td>
                                        @endif
                                    @elseif($titles === 'product_category_id')
                                        @if($se->category)
                                            <td class="text-center">
                                                <span class="label label-success">
                                                   {{($se->category->category_name)}}
                                                </span>
                                            </td>
                                        @else
                                            <td class="text-center"><span class="label label-warning">пропуск</span>
                                            </td>
                                        @endif
                                    @elseif($titles === 'subcategory_id')
                                        @if($se->subcategory)
                                            <td class="text-center">
                                                <span class="label label-success">
                                                   {{($se->subcategory->name)}}
                                                </span>
                                            </td>
                                        @else
                                            <td class="text-center"><span class="label label-warning">пропуск</span>
                                            </td>
                                        @endif
                                    @else
                                        <td>
                                        @if (base64_encode(base64_decode($se->$titles)) === $se->$titles)
                                            {{(base64_decode($se->$titles))}}
                                        @else
                                            {{($se->$titles)}}
                                        @endif
                                    @endif
                                @endforeach
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        @else
            <div style="font-size: 2.5rem" class="text-center">Отсутствуют товары</div>
        @endif
    </div>
@endsection
@push('scripts')
    <script src=""></script>
    <script src="{{asset('/js/admin/orders/ordersList.js')}}"></script>
@endpush
