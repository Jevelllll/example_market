<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuickOffer extends Model
{
    protected $table = 'quick_offers';
//    protected $attributes = [
//        'status' => self::STATUS_UNCONFIRMED,
//    ];
    protected $fillable = ['name', 'desc', 'gallery', 'start', 'end'];
    public function setStartAttribute($value)
    {
        $this->attributes['start'] = date("Y-m-d", strtotime($value));
    }
    public function setEndAttribute($value)
    {
        $this->attributes['end'] = date("Y-m-d", strtotime($value));
    }
}
