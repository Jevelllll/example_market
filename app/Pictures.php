<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * This is the model class for table "pictures".
 *
 * @property integer $product_id
 * @property integer $position_index
 * @property string $image_list
 * @property string $created_at
 * @property string $updated_at
 */
class Pictures extends Model
{
    protected $table = "pictures";
    protected $fillable = ["product_id", "image_list", "created_at", "updated_at", "position_index"];
}
