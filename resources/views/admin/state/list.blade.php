@extends('admin.layouts.admin_template')
@section('content')
    <span class='tegs' tegs="/admin/state/edit/"></span>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary pt-3 ml-md-1">Список регионов PayPal</h6>
{{--            <a class="btn btn-md btn-success mt-2" href="{{route('new.country')}}">Добавить страну</a>--}}
        </div>
        @if($stateList)
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            @foreach($stateTitle as $titles)
                                <th>{{Translators::translateRow()[$titles]}}</th>
                            @endforeach
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            @foreach($stateTitle as $titles)
                                <th>{{Translators::translateRow()[$titles]}}</th>
                            @endforeach
                        </tr>
                        </tfoot>
                        <tbody>
                        @foreach($stateList as $st)
                            <tr data-id="{{$st->id}}">
                                @foreach($stateTitle as $key => $titles)
                                        <td>{{$st->$titles}}</td>
                                @endforeach
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        @else
            <div style="font-size: 2.5rem" class="text-center">Отсутствуют регионы PayPal</div>
        @endif
    </div>
@endsection
@push('scripts')
    <script src="{{asset('/js/admin/countries/countriesList.js')}}"></script>
@endpush
