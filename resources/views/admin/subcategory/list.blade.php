@extends('admin.layouts.admin_template')
@section('content')
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary pt-3">Список подкатегорий</h6>
        </div>
        @if($subcategories)
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        @foreach($subcategoryTitle as $titles)
                            @if($titles === 'img')
{{--                                <th class="image_thumbnails">{{Translators::translateRow()[$titles]}}</th>--}}
                            @else
                                <th>{{Translators::translateRow()[$titles]}}</th>
                            @endif
                        @endforeach
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        @foreach($subcategoryTitle as $key => $titles)
                            @if($titles === 'img')
{{--                                <th class="image_thumbnails">{{Translators::translateRow()[$titles]}}</th>--}}
                            @else
                                <th>{{Translators::translateRow()[$titles]}}</th>
                            @endif
                        @endforeach
                    </tr>
                    <tfoot>
                    <tbody>
                    @foreach($subcategories as $subcategory)
                        <tr data-id="{{$subcategory->id}}">
                            @foreach($subcategoryTitle as $key => $titles)
                                @if($titles === 'img')

{{--                                    @if(file_exists(public_path().'/images/subcategory/thumbnails/'. $subcategory->$titles))--}}
{{--                                        <td><img class="img-fluid rounded"--}}
{{--                                                 src="{!! asset('images/subcategory/thumbnails/'. $subcategory->$titles) !!}">--}}
{{--                                        </td>--}}
{{--                                    @else--}}
{{--                                        <td><img class="img-fluid rounded"--}}
{{--                                                 src="{!! asset('images/product/no_foto.png') !!}">--}}
{{--                                        </td>--}}
{{--                                    @endif--}}

                                @else
                                    <td>{{$subcategory->$titles}}</td>
                                @endif

                            @endforeach
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        @else
            <div style="font-size: 2.5rem" class="text-center">Отсутствуют подкатегории</div>
        @endif
    </div>
@endsection
@push('scripts')
    <script src="{{asset('/js/admin/subcategories/subcategoryList.js')}}"></script>
@endpush
