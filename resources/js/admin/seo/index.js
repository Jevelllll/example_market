import {errorsToDb, removeCol} from "../base";

document.addEventListener('DOMContentLoaded', function () {
    let seoInfoSList = document.querySelectorAll('.seoInfo');
    let seoForm = document.querySelector('#seoForm');
    let seoListRoute = document.querySelector('#seoList').getAttribute('href');
    let hideShowMethod = () => {
        seoInfoSList.forEach(el => {
            el.addEventListener('click', (e) => {
                e.preventDefault();
                const target = e.target;
                if (!target) return false;
                const infoElement = target.nextElementSibling;
                if (!infoElement) return false;
                if (infoElement.classList.contains('d-none')){
                    target.textContent = 'Инфо ? (скрыть)';
                    infoElement.classList.remove('d-none');
                } else {
                    target.textContent = 'Инфо ? (показать)';
                    infoElement.classList.add('d-none');
                }
            })
        })
    };
    let close = () => {
      document.querySelectorAll('.close').forEach((close) => {
            close.addEventListener('click', function (){
                // $(this).parent().removeClass("d-block").addClass('d-none')
                if (this.parentElement.classList.contains('d-block')) {
                    this.parentElement.classList.remove('d-block');
                }
            });
      });
    };
    let submitSeo = () => {
        seoForm.addEventListener('submit', (e) => {
            e.preventDefault();
            let httpRequestSeo = new XMLHttpRequest();
            httpRequestSeo.open(seoForm.getAttribute('method'), seoForm.getAttribute('action'), true);
            httpRequestSeo.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            httpRequestSeo.setRequestHeader(
                'X-CSRF-TOKEN',
                document.querySelector("meta[name='csrf-token']").getAttribute('content'));
            httpRequestSeo.setRequestHeader("X-Requested-With", "XMLHttpRequest");
            httpRequestSeo.responseType = 'json';
            httpRequestSeo.send(seoForm.serialize());
            httpRequestSeo.onload = () => {
                if (httpRequestSeo.status === 200) {
                    swal({
                        title: 'Успех!',
                        text: 'Сообщение оставлено!',
                        type: "success",
                        confirmButtonText: "OK",
                        showCancelButton: false
                    }).then(() => {
                        console.log(httpRequestSeo.response);
                        window.location.href = seoListRoute;
                    });
                } else {
                    errorsToDb({responseJSON: httpRequestSeo.response});
                }
            };
            httpRequestSeo.onerror = () => {
                swal({
                    title: 'Ошибка!',
                    text: 'Перезагрузите страницу и повторите операцию!',
                    type: "error",
                    confirmButtonText: "OK",
                    showCancelButton: false
                }).then(() => {
                });
            }
        })
    };

    (() => {
        if (document.getElementsByClassName('delete')[0]) {

            let elementsByClassName = document.getElementsByClassName('delete');
            elementsByClassName[0].addEventListener('click', (event) => {
                let eventTarget = event.target;
                let deleteId = eventTarget.getAttribute('attr-delete');
                let route = eventTarget.getAttribute('attr-route');
                removeCol(route, {id: deleteId}, "POST", "Успешно удаленно!", seoListRoute);
            });
        }
    })();
    close();
    hideShowMethod();
    submitSeo();
});
