<div class="modal-warning">
    <div class="modal-content-warning">
        <span class="close-purchare">&times;</span>
        <div class="purchase-bottom-line">
            <h4>{{session('warning')}}</h4>
        </div>
    </div>
</div>
<script>
    function clearLocalStorage() {
        localStorage.setItem('purchase', JSON.stringify([]));
    }
    clearLocalStorage();
</script>
