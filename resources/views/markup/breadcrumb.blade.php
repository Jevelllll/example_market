@push('style')
    <script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "BreadcrumbList",
  "itemListElement": [
  <?php
        $numItems = count($markupStr);
        $indexUpd = 0;
        foreach ($markupStr as $mStr) {
            echo '{';
            $numDownCount = count((array) $mStr);
            $indexDown = 0;
            echo '"@type":"ListItem","position":"'.$mStr['position'].'","item":{"@id":"'.$mStr['item'].'","name":"'.$mStr['name'].'"}';
            if (++$indexUpd === $numItems) {
                echo '}';
            } else {
                echo '},';
            }
        }
        ?>
        ]
  }
    </script>
@endpush
