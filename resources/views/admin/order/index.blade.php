@extends('admin.layouts.admin_template')
@push('style')
    <link href="{{asset('/css/admin/order_truns.css')}}" rel="stylesheet">
@endpush
@section('content')
    <span class='tegs' tegs="/admin/order/view/"></span>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary pt-3 ml-md-1">Список заказов</h6>
        </div>
        @if($orders)
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            @foreach($ordersTitle as $titles)
                                <th>{{Translators::translateRow()[$titles]}}</th>
                            @endforeach
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            @foreach($ordersTitle as $titles)
                                <th>{{Translators::translateRow()[$titles]}}</th>
                            @endforeach
                        </tr>
                        </tfoot>
                        <tbody>
                        @foreach($orders as $order)
                            <tr data-id="{{$order->id}}">
                                @foreach($ordersTitle as $key => $titles)
                                    @if($titles === 'is_order_read')
                                        @if($order->$titles == 1)
                                            <td><span class="label label-success">Да</span></td>
                                        @else
                                            <td><span class="label label-warning">Нет</span></td>
                                        @endif
                                    @elseif($titles === 'total_sum')
                                        <td>{{$order->$titles}}&nbsp;USD</td>
                                    @else
                                        <td>{{$order->$titles}}</td>
                                    @endif
                                @endforeach
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        @else
            <div style="font-size: 2.5rem" class="text-center">Отсутствуют заказы</div>
        @endif
    </div>
@endsection
@push('scripts')
    <script src="{{asset('/js/admin/orders/ordersList.js')}}"></script>
@endpush
