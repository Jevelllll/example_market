<?php

namespace App\Http\Controllers;

use App\ProductRate;
use Illuminate\Http\Request;

class ProductRateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $allData = $request->all();
        $allData = array_merge($allData, ['ip' => $request->ip()]);
        $rateModel = ProductRate::where('product_id', $allData['product_id'])->where('ip', $allData['ip'])->first();
        if ($rateModel instanceof ProductRate) {
            if ($rateModel->update($allData)) {
                return response()->json($request->all());
            } else {
                return response()->json($request->all(), 404);
            }
        } else {
            $rateModel = new ProductRate();
            if ($rateModel->create($allData)) {
                return response()->json($allData);
            } else {
                return response()->json($request->all(), 404);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\ProductRate $productRate
     * @return \Illuminate\Http\Response
     */
    public function show(ProductRate $productRate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\ProductRate $productRate
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductRate $productRate)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\ProductRate $productRate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductRate $productRate)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\ProductRate $productRate
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductRate $productRate)
    {
        //
    }
}
