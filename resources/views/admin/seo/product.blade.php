@extends('admin.layouts.admin_template')
@push('style')
    <link href="{{asset('/css/admin/product.css')}}" rel="stylesheet">
@endpush
@section('content')
    @if($isCreate)
        <h1 class="h3 mb-4 text-gray-800 text-center pt-3">Добавить SEO к товару</h1>
    @else
        @if($seo->product)
            <h1 class="h3 mb-4 text-gray-800 text-center pt-3">Обновить SEO Товар: ( {{$seo->product->name}} )</h1>
        @else
            <h1 class="h3 mb-4 text-gray-800 text-center pt-3">Обновить SEO</h1>
        @endif
    @endif
    <div class="card shadow mb-4">
        @if(count($products) > 1)
            <div class="row justify-content-center">
                @if($isCreate)
                    {{Form::model($seo, array('route' => array('seo.save'),
                             'id'=>'seoForm', 'class'=> 'w-75 pt-5 pb-5','method' => 'POST', 'enctype'=>'multipart/form-data')) }}
                    <div class="form-group">
                        {{Form::label('product_id', 'Товар', ['for' => 'category_id'])}}
                        {!! Form::select('product_id', $products, 0,  ['id' => 'product_id', 'class' => 'form-control']) !!}
                        <div class="alert alert-danger alert-dismissible d-none">
                            <button type="button" class="close">&times;</button>
                            <strong class="infoError"></strong>
                        </div>
                    </div>
                @else
                    {{Form::model($seo, array('route' => array('seo.update', $seo->id),
                               'id'=>'seoForm', 'class'=> 'w-75 pt-5 pb-5','method' => 'PUT', 'enctype'=>'multipart/form-data')) }}
                    {{ Form::hidden('product_id', $seo->product_id) }}
                @endif
                @include('admin.seo.seo_fields', ['isProduct' => true])

                @if($isCreate)
                    {{Form::submit('Создать',
   ['class' => 'btn btn-form btn-success shadow-custom text-uppercase border-radius-50 btn-lg w-100 mt-2 mb-3'])}}
                @else
                    {{Form::submit('Обновить',
   ['class' => 'btn btn-form btn-success shadow-custom text-uppercase border-radius-50 btn-lg w-100 mt-2 mb-3'])}}
                @endif

                {{ Form::close() }}
                @if(!$isCreate)
                    <button class="delete ml-auto btn btn-danger mb-5 float-right"
                            attr-delete="{{$seo->id}}"
                            attr-route="{{route("seo.delete")}}">Удалить&nbsp;<i class="far fa-trash-alt"></i></button>
                @endif
            </div>
        @else
            <div style="font-size: 1.5rem" class="text-center">
                Все установлено.
            </div>
        @endif

    </div>
@endsection
@push('scripts')
    <script src="{{asset('/js/admin/seo/index.js')}}"></script>
@endpush
