<?php


namespace App\Services;


use App\Http\Controllers\Controller;
use App\ProductCategory;

class CategoryListService extends Controller
{

//$data = ProductCategory::with(['subcategory' => function ($query) {
//    $query->with(['products' => function ($qp) {
//        $qp->where('status_available', '1');
//    }])->get();
//}])->join('products', 'product_category.id', '=', 'products.category_id')
//->where('products.status_available', '1')
//->get();
////        dd($data);
//return $data;

    public function getAll()
    {
        $data = ProductCategory::with(['subcategory' => function ($query) {
            $query->with(['products' => function ($qp) {
                $qp->where('status_available', '1');
            }])->get();
        }, 'productfc' => function ($qw) {
            $qw->where('status_available', '1');
        }])
            ->get();
        return $data;
    }

    public function getLimit()
    {
        return ProductCategory::limit(4)->get();
    }
}
