@extends('admin.layouts.admin_template')
@push('style')
    <link href="{{asset('/css/admin/product.css')}}" rel="stylesheet">
@endpush
@section('content')
    @if($errors->any)
        @foreach($errors->all() as $error)
            <span>
                    {{$error}}
                </span>
        @endforeach
    @endif
    <div class="card shadow mb-4">
        <div id="options-storage">{{$model->options}}</div>
        @if($isCreate)

            <h1 class="h3 mb-4 text-gray-800 text-center pt-3">Добавить новый товар</h1>
        @else
            <h1 class="h3 mb-4 text-gray-800 text-center pt-3">Обновить товар</h1>
        @endif
        <div class="row justify-content-center">
            @if($isCreate)
                {{Form::model($model, array('route' => array('save.product'),
                    'id'=>'catForm', 'class'=> 'w-75','method' => 'POST', 'enctype'=>'multipart/form-data')) }}
            @else
                {{Form::model($model, array('route' => array('update.product', $model->id),
                    'id'=>'catForm', 'class'=> 'w-75', 'method' => 'PUT', 'enctype'=>'multipart/form-data')) }}
            @endif
            {{--                $categories--}}
            <div class="form-group">
                <div class="d-none" id="hidden-category-data">{{$model->category_id}}</div>
                {{Form::label('category_id', 'Бренд', ['for' => 'category_id'])}}
                {!! Form::select('category_id', $categories, 0,  ['id' => 'category_id', 'class' => 'form-control']) !!}
                <div class="alert alert-danger alert-dismissible d-none">
                    <button type="button" class="close">&times;</button>
                    <strong class="infoError"></strong>
                </div>
            </div>
            <div class="form-group">
                <div class="d-none" id="hidden-subcategory-data">{{$model->subcategory_id}}</div>
                {{Form::label('subcategory_id', 'Подкатегория', ['for' => 'subcategory_id'])}}
                {!! Form::select('subcategory_id', [], null,  ['id' => 'subcategory_id', 'class' => 'form-control']) !!}
                <div class="alert alert-danger alert-dismissible d-none">
                    <button type="button" class="close">&times;</button>
                    <strong class="infoError"></strong>
                </div>
            </div>
            <div class="form-group">
                {{Form::label('name', 'Название', ['for' => 'name'])}}
                {{Form::text('name',  null, ['id' => 'name', 'class' => 'form-control'])}}
                <div class="alert alert-danger alert-dismissible d-none">
                    <button type="button" class="close">&times;</button>
                    <strong class="infoError"></strong>
                </div>
            </div>
            <div class="form-group">
                {{Form::label('seo_name', 'SEO ИМЯ', ['for' => 'seo_name'])}}
                {{Form::text('seo_name',  null, ['id' => 'seo_name', 'class' => 'form-control'])}}
                <div class="alert alert-danger alert-dismissible d-none">
                    <button type="button" class="close">&times;</button>
                    <strong class="infoError"></strong>
                </div>
            </div>
            <div class="form-group">
                {{Form::label('desc', 'Описание товара', ['for' => 'desc'])}}
                {{Form::textarea('desc',  null, ['id' => 'desc', 'class' => 'form-control', 'rows' => '5', 'cols' =>  120])}}
                <div class="alert alert-danger alert-dismissible d-none">
                    <button type="button" class="close">&times;</button>
                    <strong class="infoError"></strong>
                </div>
            </div>
            <div class="form-group">
                {{Form::label('condition', 'Cостояние(condition)', ['for' => 'condition'])}}
                {{Form::textarea('condition',  null, ['id' => 'condition', 'class' => 'form-control', 'rows' => '3', 'cols' =>  120])}}
                <div class="alert alert-danger alert-dismissible d-none">
                    <button type="button" class="close">&times;</button>
                    <strong class="infoError"></strong>
                </div>
            </div>
            <div class="form-group">
                {{Form::label('shipping', 'Перевозка(shipping)', ['for' => 'shipping'])}}
                {{Form::textarea('shipping',  null, ['id' => 'shipping', 'class' => 'form-control', 'rows' => '3', 'cols' =>  120])}}
                <div class="alert alert-danger alert-dismissible d-none">
                    <button type="button" class="close">&times;</button>
                    <strong class="infoError"></strong>
                </div>
            </div>
            <div class="form-group">
                {{Form::label('desc', 'Цена', ['for' => 'price'])}}
                {{Form::number('price',  null, ['id' => 'price', 'class' => 'form-control', 'step'=>'any'])}}
                <div class="alert alert-danger alert-dismissible d-none">
                    <button type="button" class="close">&times;</button>
                    <strong class="infoError"></strong>
                </div>
            </div>
            <input type="hidden" id="optionsData" name="options">
            <div class="form-group active updateActive">
                {{Form::label('Cтатус доступен')}}

                <div class="form-check">
                    <input type="radio" id="active" name="status_available"
                           value="1" {{ ($model->status_available==1)? "checked" : "" }} >
                    {{Form::label('active', 'Активен')}}
                </div>
                <div class="form-check">
                    <input type="radio" id="not_active" name="status_available"
                           value="0" {{ ($model->status_available==0)? "checked" : "" }} >
                    {{Form::label('not_active', 'Не активно')}}
                </div>
                <div class="alert alert-danger alert-dismissible d-none">
                    <button type="button" class="close">&times;</button>
                    <strong class="infoError"></strong>
                </div>
            </div>

            <div class="form-group active updateActive">
                {{Form::label('Статус продажи')}}

                <div class="form-check">
                    <input type="radio" id="sold_in" name="is_sold"
                           value="1" {{ ($model->is_sold==1)? "checked" : "" }} >
                    {{Form::label('sold_in', 'Продан')}}
                </div>
                <div class="form-check">
                    <input type="radio" id="sold_out" name="is_sold"
                           value="0" {{ ($model->is_sold==0)? "checked" : "" }} >
                    {{Form::label('sold_out', 'Не продан')}}
                </div>
                <div class="alert alert-danger alert-dismissible d-none">
                    <button type="button" class="close">&times;</button>
                    <strong class="infoError"></strong>
                </div>
            </div>

            <hr/>
            <h4>Дополнительные параметры</h4>
            <div class="additional_form">
                <div class="form-group">
                    <div>
                        <div class="d-flex flex-wrap justify-content-between">
                            <label for="additional_form_key">Ключ :</label>
                            <input id="additional_form_key" class="additional_form_key form-control w-75">
                        </div>
                        <div class="d-flex flex-wrap justify-content-between mt-2">
                            <label for="additional_form_value">Значение :</label>
                            <input id="additional_form_value" class="additional_form_value form-control w-75">
                        </div>
                        <div id="additional-error" class="alert alert-danger mt-2 mb-2 alert-dismissible d-none">
                            <button type="button" class="close">×</button>
                            <strong>Одно из полей "Пусто".</strong>
                        </div>
                    </div>
                    <span class="additional_button btn btn-primary" id="add-additional-option">ДОБАВИТЬ</span>
                    <span class="additional_button btn btn-primary d-none"
                          id="add-additional-option-cancel">Отмена</span>
                </div>

            </div>

            <div class="additional_content row" id="additional_content">
            </div>

            <hr/>

            <div class="form-group col">
                <label for="gallery">Изображение</label>
                <div class="needsclick dropzone " id="document-dropzone">
                </div>
                <div class="alert alert-danger alert-dismissible d-none">
                    <button type="button" class="close">&times;</button>
                    <strong class="infoError"></strong>
                </div>
            </div>
            @if($isCreate)
                {{Form::submit('Создать',
['class' => 'btn btn-form btn-success shadow-custom text-uppercase border-radius-50 btn-lg w-100 mt-2 mb-3'])}}
            @else
                <div class="d-flex flex-wrap">
                    <a class="btn btn-form btn-primary col shadow-custom
                    text-uppercase border-radius-50 btn-lg w-100 mt-2 mb-3 mr-2"
                       href="{{route('index.product-binding', ["id" => $model->id])}}">Связи с другими товарами</a>
                    {{Form::submit('Обновить',
     ['class' => 'btn btn-form btn-success col shadow-custom text-uppercase border-radius-50 btn-lg w-100 mt-2 mb-3'])}}
                    <a class="btn btn-form btn-info col shadow-custom
                    text-uppercase border-radius-50 btn-lg w-100 mt-2 mb-3 ml-2 h-100"
                       href="{{route('position.pictures', ['product_id' => $model->id])}}">Позиции изображений</a>
                </div>
            @endif
            {{ Form::close() }}
        </div>
        @if(!$isCreate)
            <a href="{{route('product.clone', ['id' => $model->id])}}" class="product-clone ml-auto btn btn-warning mb-5">Дублировать <i class="fas fa-clone"></i></a>
            <button class="delete ml-auto btn btn-danger mb-5 float-right"
                    attr-delete="{{$model->id}}"
                    attr-route="{{route("product.delete")}}">Удалить&nbsp;<i class="far fa-trash-alt"></i></button>
        @endif
    </div>
@endsection

@push('scripts')
    <script src="{{asset('/js/admin/products/productList.js')}}"></script>
    <script>
        var uploadedDocumentMap = {};
        var removeCurrentFile = '';
        Dropzone.options.documentDropzone = {

            url: '{{ route('projects.storeMedia') }}',
            // maxFilesize: 2, // MB
            autoProcessQueue: true,
            maxFiles: 10,
            uploadMultiple: false,
            addRemoveLinks: true,
            // acceptedFiles: ".jpeg,.jpg,.png,.gif",
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            success: function (file, response) {
                @if($isCreate)
                $('#catForm').append('<input type="hidden" name="gallery[]" value="' + response.name + '">')
                @else
                $('#catForm').append('<input type="hidden" name="gallery[]" value="' + response.name + '">')
                @endif

                    uploadedDocumentMap[file.name] = response.name
                console.log(uploadedDocumentMap);
            },
            sending: function () {
                // $('.dz-image-preview').remove();
            },
            init: function () {
                this.on('addedfile', function (file) {
                    var preview = document.getElementsByClassName('dz-preview');
                    preview = preview[preview.length - 1];
                    var imageName = document.createElement('span');
                    imageName.innerHTML = file.name;
                });
                @if(!$isCreate)
                var url = "{{route('product.files', ['id' => $model->id])}}";
                var thisDropzone = this;
                // Spinner.show();
                $.getJSON(url, function (data) { // get the json response
                    console.log(data);
                    if (Array.isArray(data)) {
                        removeCurrentFile = data[0].name;
                    }
                    $.each(data, function (key, value) { //loop through it
                        if (value.hasOwnProperty('shortName')) {
                            var mockFile = {name: value.shortName, size: value.size}; // here we get the file name and size as response
                            thisDropzone.options.addedfile.call(thisDropzone, mockFile);
                            thisDropzone.options.thumbnail.call(thisDropzone, mockFile, value.name);//uploadsfolder is the folder where you have all those uploaded files
                            $('#catForm').append('<input type="hidden" name="gallery[]" value="' + value.shortName + '">')
                        }
                    });
                    // Spinner.hide();
                });
                @endif
            },
            removedfile: function (file) {
                console.log(file);
                if (file.status === 'success') {
                    var foolFileName = {};
                    var name = '';
                    file.previewElement.remove();

                    foolFileName = file.xhr.responseText;
                    name = '';
                    if (typeof file.file_name !== 'undefined') {
                        name = file.file_name
                    } else {
                        name = uploadedDocumentMap[file.name]
                    }
                    file.previewElement.remove();

                    $('#catForm').find('input[name="gallery[]"][value="' + name.shortName + '"]').remove();
                    $('#catForm').find('input[name="gallery[]"][value="' + file.name + '"]').remove();
                    $('form').find('input[name="gallery[]"][value="' + name + '"]').remove();
                    var mainGallery = $('#document-dropzone');
                    var alert = $(mainGallery).siblings(".alert");
                    $(alert).find(".infoError").text('Тип файла не изображение');
                    $(alert).removeClass("d-block");

                    var dataDelete = {
                        name: foolFileName,
                        // product_id: 1
                    };
                    @if($isCreate)
                        delete dataDelete.product_id;
                    @endif

                    $.ajax({
                        url: '{{ route('product_image.delete')}}',
                        type: 'post',
                        data: dataDelete,
                        headers: {
                            'X-CSRF-TOKEN': "{{ csrf_token() }}"
                        },
                        dataType: 'json',
                        success: function (data) {
                            console.log(data);
                        }
                    });


                } else {
                    file.previewElement.remove();
                    $('#catForm').find('input[name="gallery[]"][value="' + file.name + '"]').remove();
                    $('form').find('input[name="gallery[]"][value="' + name + '"]').remove();
                }

            },
        };
        Dropzone.prototype.defaultOptions.dictRemoveFile = "Удалить файл";
        Dropzone.prototype.defaultOptions.dictDefaultMessage = "\n" +
            "Перетащите изображение сюда, чтобы загрузить";
        Dropzone.prototype.defaultOptions.dictRemoveFile = "Удалить файл";

    </script>
@endpush
