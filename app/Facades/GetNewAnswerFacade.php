<?php
namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class GetNewAnswerFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'getnewanswer';
    }
}
