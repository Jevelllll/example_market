<?php

namespace App\Http\Requests\admin\validate\banners;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreBanners extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:7',
            'desc' => 'required|min:7',
            'redirect_url' => 'required|min:7',
            'active' => 'required',
            'gallery' => 'required|max:10000'
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Поле "Название" Обязательно!',
            'name.min' => 'Минимальный размер поля "Название" 7 символов',
            'desc.required' => 'Поле "Описание" Обязательно!',
            'desc.min' => 'Минимальный размер поля "Описание" 7 символов',
            'redirect_url.required' => 'Поле "Переадресации" Обязательно!',
            'redirect_url.min' => 'Минимальный размер поля "Переадресации" 7 символов',
            'active.required' => 'Поле "Активация" Обязательно!',
            'gallery.required' => 'Поле Обязательно!',
        ];
    }

    public function  attributes()
    {
        return [
            'name' => 'category title'
        ];
    }
    public function filters()
    {
        return [
            'name' => 'trim|capitalize|escape'
        ];
    }

}
