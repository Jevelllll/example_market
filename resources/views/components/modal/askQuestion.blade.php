<div class="modal-asqQuestion">

    <div class="modal-content-asqQuestion mt-0">
        <span class="close-asqQuestion">&times;</span>
        <div class="asqQuestion-bottom-line pt-0 pt-md-2">
            <h4 class="asqQuestion_up_label pl-0  pl-md-3">Ask a Question</h4>
            <p class="asqQuestion_down_label pl-0 pl-md-3 hide-pre-success">Need more information about this item? Send the seller a message.</p>
            <p class="asqQuestion_down_label pl-0 pl-md-3 success-send-msg justify-content-lg-start d-none">Message sent successfully</p>
            <div class="row">
                <div class="success-send-msg d-none">
                    <i class="far fa-check-circle"></i>
                </div>

                <div class="col-12 col-md asqQuestion__main hide-pre-success">
                    <div class="asqQuestion__main__top pl-0 pl-md-4">
                        @if(count($product->pictures) > 0)
                            <img class="img-fluid img-thumbnail w-100 text-center text-md-left"
                                 alt="{{$product->name}}"
                                 src="{!!asset('/images/product/pictures/thumbnails')
                                    .'/'.$product->pictures[0]->image_list!!}"
                            />
                        @else
                            <img class="img-fluid img-thumbnail w-100"
                                 src="{!!asset('/images/product/no_foto.png').'/'!!}" alt="{{$product->name}}">
                        @endif
                    </div>
                    <div class="asqQuestion__main__center pt-md-2 pl-0 pl-md-3 text-center text-md-left">
                        {{$product->name}}
                    </div>
                    <div class="asqQuestion__main__down pl-0 pl-md-3 text-center text-md-left">
                         {{$product->price}} USD
                    </div>
                </div>
                <div class="col-12 col-md-8 hide-pre-success">
                    {{Form::model($askModel, ['route' => 'send.product.answer', 'id'=> 'askModel', 'method' => 'post', 'class' => 'mui-form'])}}
                    <div class="mui-textfield mui-textfield--float-label">
                        {{Form::text('from', null, ['class' => 'w-100' , 'id'=> 'from'])}}
                        {{Form::label('email', 'Email*', ['for' => 'from'])}}
                        <div class="error-message d-none">Error</div>
                    </div>
                    {{ Form::hidden('to', env('MAIL_USERNAME')) }}
                    {{ Form::hidden('product_id', $product->id) }}
                    <div class="mui-textfield mui-textfield--float-label">
                        {{Form::textarea('message',  null, ['id' => 'message', 'class' => 'w-100', 'rows' => 3])}}
                        {{Form::label('message', 'Comment*', ['for' => 'message'])}}
                        <div class="error-message d-none">Error</div>
                    </div>

                    {{Form::submit('Send!', ['class' => 'button-add-cart w-100 purchase purple-gr'])}}
                    {{Form::close()}}
                </div>
            </div>
        </div>
    </div>
</div>
