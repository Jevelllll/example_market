<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property double total_sum
 * @property integer id
 * @property string order_nr
 * @property string is_order_read
 * @property string status
 */
class Orders extends Model
{
    protected $table = "orders";
    protected $fillable = ["created_at", "status", "updated_at", "total_sum", "order_nr", "is_order_read"];

    public function status_paypal()
    {
        return $this->hasOne('App\PaymentStatuses', 'id', 'status');
    }

    public function order_product()
    {
        return $this->hasMany('App\OrderProduct', 'order_id', 'id');
    }

    public function saveNr()
    {
        $this->attributes['order_nr'] = '#' . mb_strtoupper($this->id . '-' . substr(str_shuffle(MD5(microtime())), 0, 6));
    }

    public function customer()
    {
        return $this->hasOne('App\Customers', 'order_id', 'id')->orderBy('id', 'desc');
    }

    public function transaction()
    {
        return $this->hasMany('App\Transactions', 'order_id', 'id')->orderBy('id', 'desc');;
    }

    public static function getNewOrder()
    {
        return self::where('is_order_read', '0')->with('transaction')->orderBy('created_at', 'desc')->get();
    }

    public static function readNewOrder($id)
    {
        $model = Orders::find($id);
        $model->is_order_read = '1';
        return $model->save();
    }
}
