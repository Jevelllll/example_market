<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banners extends Model
{
    protected $table = 'banners';
    protected $fillable = ['name', 'desc', 'gallery', 'redirect_url', 'active'];

    protected $attributes = [
        'gallery' => 'default.png'
    ];
}
