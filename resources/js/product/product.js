import {redirectRootCategories} from "./../common";

(function ($, undefined) {
    let currentPatchName = window.location.pathname;
    redirectRootCategories('menuProduct', 'product-landing_menu_item', 'DIV');
    $('.product-landing_menu_item').hover(function () {
        $(this).find('a').first().addClass('hoverMenu')
    }, function () {
        $(this).find('a').first().removeClass('hoverMenu')
    }).on('click', function (e) {
        e.preventDefault();
        let urlA = this.querySelector('a').getAttribute('href');
        if (currentPatchName === urlA) {
            let removeLinkStr = currentPatchName.split('/').pop();
            window.location.href = currentPatchName.replace(removeLinkStr, '');
        } else {
            window.location.href = urlA;
        }
    });

    // let productLandingMenuItems = document.getElementsByClassName('product-landing_menu_item');
    // if (productLandingMenuItems.length > 0) {
    //     [].slice.call(productLandingMenuItems).forEach((dataItem) => {
    //         console.log(dataItem.classList.contains('active'));
    //         dataItem.addEventListener('click', (() => {
    //             console.log(this.className);
    //         }), true)
    //     });
    // }
    /*******order buy************/
    $('#fool-products').on('click', '.buy', function (el) {
        let buy = $(this);
        if (buy) {
            let bottom = buy.parent().parent();
            if ($(bottom).hasClass('clicked')) {
            } else {
                $(bottom).addClass('clicked');
            }
        }
    }).on('click', '.remove', function () {
        let remove = $(this);
        if (remove) {
            let bottom = remove.parent().parent();
            if ($(bottom).hasClass('clicked')) {
                $(bottom).removeClass('clicked');
            }
        }
    });


})(jQuery);
