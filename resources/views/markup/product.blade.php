@push('style')
<script type="application/ld+json">
{
  "@context": "https://schema.org/",
  "@type": "Product",
  "name": "{{$product->name}}",
  "image": "{{$picture}}",
  "description": "{{$product->desc}}",
  "brand": "{{$product->category->category_name}}",
  "sku": "{{$product->id}}",
  "offers": {
    "@type": "Offer",
    "url": "/product/{{$product->seo_name}}",
    "priceCurrency": "USD",
    "price": "{{$product->price}}",
    "itemCondition": "https://schema.org/UsedCondition",
    "availability": "{{$availability}}"
  },
  "aggregateRating": {
    "@type": "AggregateRating",
    "ratingValue": "5",
    "ratingCount": "14",
    "bestRating": "5",
    "worstRating": "1"
  }
}
</script>
@endpush
