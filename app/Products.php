<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * This is the model class for table "products".
 *
 * @property integer $id
 * @property integer $category_id
 * @property integer $subcategory_id
 * @property string $name
 * @property string $desc
 * @property string $gallery
 * @property float $price
 * @property string $status_available
 * @property string $video_url
 * @property string $options
 * @property string $created_at
 * @property string $updated_at
 * @property string $seo_name
 * @property string $is_sold
 *
 */
class Products extends Model
{
    protected $table = 'products';
    protected $fillable = ['category_id', 'subcategory_id', 'name', 'desc', 'price',
        'status_available', 'video_url', 'options', 'created_at', 'updated_at', 'seo_name', 'condition', 'shipping', 'is_sold'];

    public static function getAllProducts($categoryId, $subcategory = null)
    {

        $dataList = self::with('pictures', 'bindings')->where('category_id', $categoryId)
            ->where('status_available', '1')
            ->orderBy("is_sold", "ASC")
            ->orderBy('id', 'DESC');
        if ($subcategory === null) {
        } else {
            $dataList = $dataList->where('subcategory_id', $subcategory);
        }
        return $dataList->paginate(1);
    }

    public function pictures()
    {
        return $this->hasMany('App\Pictures', 'product_id', 'id')->orderBy('id', 'DESC');
    }

    public function category()
    {
        return $this->hasOne('App\ProductCategory', 'id', 'category_id');
    }

    public function subcategory()
    {
        return $this->hasOne('App\Subcategories', 'id', 'subcategory_id');
    }

    public function bindings()
    {
        return $this->hasMany('App\ProductBinding', 'product_id', 'id');
    }

    public function rate()
    {
        return $this->hasOne('App\ProductRate', 'id', 'product_id');
    }
}
