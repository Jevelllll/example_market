<?php

namespace App\Http\Controllers;


use App\Banners;
use App\Http\Requests\admin\validate\banners\StoreBanners;
use App\Http\Requests\admin\validate\banners\StoreBannersUpdate;
use App\QuickOffer;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;


class BannersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $banner = Banners::where('id', $id)->first();
        $titleName = $banner->name;
        return view('admin.banners.edit', compact('titleName', 'banner'))->withTitle('Редактировать и просмотреть');
    }

    public function findFilesByBannersId($id, $is_q_o = false)
    {

        $path = '/images/banners/';
        $model = Banners::where('id', $id)->first();
        if ($is_q_o) {
            $path = '/images/banners/quick_offer/';
            $model = QuickOffer::where('id', $id)->first();
        }
        $myFile = [];
        if (strlen($model->gallery) > 3) {
            if (file_exists(public_path() . $path . $model->gallery)) {
                $fileName = $path . $model->gallery;
                $shortName = $model->gallery;
                $model->gallery = public_path() . $path . $model->gallery;
                $myFile['name'] = $fileName;
                $myFile['shortName'] = $shortName;
                $myFile['size'] = filesize($model->gallery);
            }
        }
        return response()->json([$myFile]);
    }


    /**
     * Update the specified resource in storage.
     * @param StoreBannersUpdate $request
     * @param $id
     * @return JsonResponse
     */
    public function update(StoreBannersUpdate $request, $id)
    {
        $requestAll = $request->all();
        $banner = Banners::find($id);
        if ($banner instanceof Banners) {
            if (isset($requestAll["gallery"])) {
                $galleryList = $requestAll["gallery"];
                if (is_array($galleryList)) {
                    if ($banner->gallery) {

                        $fileThumbnails = public_path() . '/images/banners/thumbnails/' . $banner->gallery;
                        $filePublic = public_path() . '/images/banners/' . $banner->gallery;

                        if (file_exists($fileThumbnails)) {
                            File::delete($fileThumbnails);
                        }
                        if (file_exists($filePublic)) {
                            File::delete($filePublic);
                        }

                        $this->saveImageMakeThumbnails($galleryList);
                        $requestAll['gallery'] = $galleryList[0];
                    }
                }
            }
            if ($banner->update($requestAll)) {
                return response()->json([
                    'msg' => true,
                ], 200);
            } else {
                return response()->json([
                    'msg' => false,
                ], 404);
            }
        } else {
            return response()->json([
                'msg' => false,
            ], 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /****************admin work*******************/
    public function dataAll()
    {
        $banners = Banners::orderBy('id')->get();
        if (count($banners) > 0) {
            if ($banners[0] instanceof Banners) {
                $bannersTitle = collect($banners[0])->toArray();
                $bannersTitle = array_keys($bannersTitle);

                $banners = collect($banners)->map(function ($item, $key) {
                    $newBanner = $item;
                    if (strlen($item->desc) > 25) {
                        $newBanner->desc = substr($item->desc, 0, 25) . "..";
                    } else {
                        $newBanner->desc = $item->desc;
                    }
                    return $newBanner;
                });
                return view('admin/banners/list', compact('bannersTitle', 'banners'))->withTitle('List of Banners');
            } else {
                dd(503);
            }
        } else {
            $data = ["bannersTitle" => null, "banners" => null];
            return view('admin/banners/list', $data)->withTitle('List of Banners');
        }

    }

    public function dataCreate()
    {
        return view('admin/banners/create')->withTitle('Создать новый баннер');
    }


    public function dataImgCreate(Request $request)
    {
        $path = storage_path('tmp/uploads');

        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }
        $this->validate($request, [
            'file' => 'image'
        ]);
        $file = $request->file('file');

        $date = date_create('now');
        $unicaleDate =  date_format($date, 'm-d-H-i');
        $name = $file->getClientOriginalName();
        $explodeRemove = '.'.explode(".", $name)[1];
        $replaceName = str_replace($explodeRemove, "",  $name);
        $replaceName = $replaceName . '-'. $unicaleDate . $explodeRemove;
        $img = Image::make($file);
        $img->orientate();
        $img->save($path .'/'. $replaceName);
        return response()->json([
            'name' => $replaceName,
            'original_name' => $replaceName,
        ]);
    }

    public function dataImgDelete(Request $request)
    {
        $path = storage_path('tmp/uploads');
        $fileResp = $request->request->get('name');
        $filePath = $request->request->get('filePath');
        $collection = collect(json_decode($fileResp, true));
        if (!empty($filePath)) {

            $fileThumbnails = public_path() . $filePath;
            $filePublic = public_path() . $filePath;
            $fileName = array_reverse(explode("/", $filePath));
            $quickOfferModel = QuickOffer::where('gallery', $fileName[0])->first();
            if ($quickOfferModel instanceof QuickOffer) {
                $quickOfferModel->gallery = "";
                if ($quickOfferModel->update()) {
                    if (file_exists($fileThumbnails)) {
                        File::delete($fileThumbnails);
                    }
                    if (file_exists($filePublic)) {
                        File::delete($filePublic);
                    }
                }
                return response()->json(['status' => $fileName[0]]);
            }

        } else {
            if ($collection['name']) {
                File::delete($path . '/' . $collection['name']);
            }
            return $collection['name'];
        }

    }

    /*********create new banner ***
     * @param StoreBanners $request
     * @return JsonResponse
     */
    public function dataBannerSave(StoreBanners $request)
    {

        $path = storage_path('tmp/uploads');
        $requestAll = $request->all();
        $galleryList = $requestAll["gallery"];

        if (is_array($galleryList)) {

            $this->saveImageMakeThumbnails($galleryList);

            $requestAll['gallery'] = $galleryList[0];

            if (Banners::create($requestAll)) {
                return response()->json([
                    'status' => 'success',
                    'msg' => 'Баннер успешно создан!'
                ]);
            };
        }
    }

    /************рекламное предложение******************/
    public function quickOfferCreate()
    {
        $quickOffer = QuickOffer::first();
        $model = new QuickOffer();
        $isCreate = true;
        if ($quickOffer instanceof QuickOffer) {
            $isCreate = false;
            $start = Carbon::parse($quickOffer->start)->format('Y-m-d');
            $end = Carbon::parse($quickOffer->end)->format('Y-m-d');
            $model = $quickOffer;
            $model->start = $start;
            $model->end = $end;
        }
        return view('admin/banners/quick-offer-create', compact('isCreate', 'model'))
            ->withTitle('Рекламное предложение');

    }

    public function quickOfferSave(Request $request)
    {

        $allData = $request->all();

        $messages = [
            'name.required' => 'Поле "Название" обязательно.',
            'start.required' => 'Поле "Начало предложения" обязательно.',
            'end.required' => 'Поле "Окончание предложения" обязательно.',
        ];
        $public_patch = 'images/banners/quick_offer/';
        if (isset($allData["gallery"])) {
            $galleryList = $allData["gallery"];
            $this->saveImageMakeThumbnails($galleryList, $public_patch);
            $allData['gallery'] = $galleryList[0];
        }
        Validator::make($request->all(), [
            'name' => 'required',
            'start' => 'required',
            'end' => 'required',
        ], $messages)->validate();
        if (QuickOffer::create($allData)) {
            return response()->json([
                'status' => 'success',
                'msg' => 'Запись успешно добавлена!'
            ]);
        };
    }

    public function quickOfferUpdate(Request $request, $id)
    {
        $public_patch = 'images/banners/quick_offer/';
        $requestAll = $request->all();
        $quickOffer = QuickOffer::find($id);
        if ($quickOffer instanceof QuickOffer) {
            if (isset($requestAll["gallery"])) {
                $galleryList = $requestAll["gallery"];
                if (is_array($galleryList)) {
                    $fileThumbnails = public_path() . '/' . $public_patch .'thumbnails/'. $quickOffer->gallery;
                    $filePublic = public_path() . '/' . $public_patch . $quickOffer->gallery;
                    $filePublicPreview = public_path() . '/' . $public_patch .'preview/'. $quickOffer->gallery;

                    if (file_exists($filePublicPreview)) {
                        File::delete($filePublicPreview);
                    }
                    if (file_exists($fileThumbnails)) {
                        File::delete($fileThumbnails);
                    }
                    if (file_exists($filePublic)) {
                        File::delete($filePublic);
                    }
                    $this->saveImageMakeThumbnails($galleryList, $public_patch);
                    $requestAll['gallery'] = $galleryList[0];
                }
            }
            if ($quickOffer->update($requestAll)) {
                return response()->json([
                    'msg' => true,
                ], 200);
            } else {
                return response()->json([
                    'msg' => false,
                ], 404);
            }

        }
    }


    /**
     * Перемешение изображения из tmp в public
     * @param array $galleryList
     * @param string $wherePatch
     * @return bool
     */
    private function saveImageMakeThumbnails(array $galleryList, $wherePatch = 'images/banners/')
    {
        $path = storage_path('tmp/uploads');
        foreach ($galleryList as $gallery) {
            if (file_exists($path . '/' . $gallery)) {
                $move = File::move($path . '/' . $gallery, $wherePatch . $gallery);
                if (!$move) {
                    return false;
                } else {

                    $img = Image::make(file_get_contents(public_path() . '/' . $wherePatch . $gallery));
//                    $img->fit(320, 240);
                    $imgPreview = Image::make(file_get_contents(public_path() . '/' . $wherePatch . $gallery));

                    $img->resize(null, 240, function ($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    });

                    $imgPreview->resize(null, 618, function ($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    });

                    $imgPreview->save(public_path() . '/' . $wherePatch . 'preview/' . $gallery);

                    if ($img->save(public_path() . '/' . $wherePatch . 'thumbnails/' . $gallery)) {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        }
    }

    public function delete(Request $request)
    {
        $requestData = $request->all();
        $model = Banners::find($requestData['id']);
        if ($model instanceof Banners) {
            if (!empty($model->gallery)) {
                $public_patch = 'images/banners/';
                $fileThumbnails = public_path() . '/' . $public_patch . 'thumbnails/' . $model->gallery;
                $filePreview = public_path() . '/' . $public_patch . 'preview/' . $model->gallery;
                $filePublic = public_path() . '/' . $public_patch . $model->gallery;

                if (file_exists($filePreview)) {
                    File::delete($filePreview);
                }
                if (file_exists($fileThumbnails)) {
                    File::delete($fileThumbnails);
                }
                if (file_exists($filePublic)) {
                    File::delete($filePublic);
                }
            }
            if ($model->delete()) {
                return response()->json([
                    'msg' => true,
                ], 200);
            }
        } else {
            return response()->json([
                'msg' => false,
            ], 404);
        }
    }

}
