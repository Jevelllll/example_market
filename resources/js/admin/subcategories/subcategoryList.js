import {detectTrTable, formSend, removeCol, translatorTableColumn} from "../base";

$(document).ready(function () {
    // Call the dataTables jQuery plugin
    $(document).ready(function () {
        $('#dataTable').DataTable(translatorTableColumn());
    });
    /******************категории******************************************************************************/
    formSend('#catForm', '/admin/subcategory/list');
    detectTrTable('dataTable', '/admin/subcategory/edit/');

    (() => {
        if (document.getElementsByClassName('delete')[0]) {

            let elementsByClassName = document.getElementsByClassName('delete');
            elementsByClassName[0].addEventListener('click', (event) => {
                let eventTarget = event.target;
                let deleteId = eventTarget.getAttribute('attr-delete');
                let route = eventTarget.getAttribute('attr-route');
                removeCol(route, {id: deleteId}, "POST", "Успешно удаленно!", '/admin/subcategory/list');
            });
        }
    })();

});
