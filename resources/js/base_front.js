let Redirect = (url) => {
    let browserName = navigator.appName;
    if (browserName === "Netscape") {
        window.location = url;
    } else if (browserName === "Microsoft Internet Explorer") {
        window.location = url;
    } else {
        window.location = url;
    }
}
export {Redirect};
