@extends('admin.layouts.admin_template')
@section('content')

    @if($errors->any)
        @foreach($errors->all() as $error)
            <span>
                {{$error}}
            </span>
        @endforeach
    @endif

    <div class="card shadow mb-4">
        @if($isCreate)
            <h1 class="h3 mb-4 text-gray-800 text-center pt-3">Добавить новый бренд</h1>
        @else
            <h1 class="h3 mb-4 text-gray-800 text-center pt-3">Обновить бренд</h1>
        @endif
        <div class="row justify-content-center">
            @if($isCreate)
                {{Form::model($model, array('route' => array('save.brand'),
                    'id'=>'catForm', 'class'=> 'w-75','method' => 'POST', 'enctype'=>'multipart/form-data')) }}
            @else
                {{Form::model($model, array('route' => array('update.brand', $model->id),
                    'id'=>'catForm', 'class'=> 'w-75', 'method' => 'PUT', 'enctype'=>'multipart/form-data')) }}
            @endif
            <div class="form-group">
                {{Form::label('category_name', 'Название', ['for' => 'category_name'])}}
                {{Form::text('category_name',  null, ['id' => 'category_name', 'class' => 'form-control'])}}
                <div class="alert alert-danger alert-dismissible d-none">
                    <button type="button" class="close">&times;</button>
                    <strong class="infoError"></strong>
                </div>
            </div>
            <div class="form-group">
                {{Form::label('seo_name', 'SEO ИМЯ', ['for' => 'seo_name'])}}
                {{Form::text('seo_name',  null, ['id' => 'seo_name', 'class' => 'form-control'])}}
                <div class="alert alert-danger alert-dismissible d-none">
                    <button type="button" class="close">&times;</button>
                    <strong class="infoError"></strong>
                </div>
            </div>
            <div class="form-group">
                {{Form::label('down_desc', 'Down description', ['for' => 'down_desc'])}}
                {{Form::textarea('down_desc',  null, ['id' => 'down_desc', 'rows' => 3, 'class' => 'form-control'])}}
                <div class="alert alert-danger alert-dismissible d-none">
                    <button type="button" class="close">&times;</button>
                    <strong class="infoError"></strong>
                </div>
            </div>
            {{--            <div class="form-group col-3">--}}
            {{--                <label for="gallery">Изображение</label>--}}
            {{--                <div class="needsclick dropzone" id="document-dropzone">--}}
            {{--                </div>--}}
            {{--                <div class="alert alert-danger alert-dismissible d-none">--}}
            {{--                    <button type="button" class="close">&times;</button>--}}
            {{--                    <strong class="infoError"></strong>--}}
            {{--                </div>--}}
            {{--            </div>--}}
            @if($isCreate)
                {{Form::submit('Создать',
['class' => 'btn btn-form btn-success shadow-custom text-uppercase border-radius-50 btn-lg w-100 mt-2 mb-3'])}}
            @else
                {{Form::submit('Обновить',
 ['class' => 'btn btn-form btn-success shadow-custom text-uppercase border-radius-50 btn-lg w-100 mt-2 mb-3'])}}
            @endif
            {{ Form::close() }}
        </div>
        @if(!$isCreate)
            <button class="delete ml-auto btn btn-danger mb-5 float-right"
                    attr-delete="{{$model->id}}"
                    attr-route="{{route("brand.delete")}}">Удалить&nbsp;<i class="far fa-trash-alt"></i></button>
        @endif
    </div>
@endsection
@push('scripts')
    <script src="{{asset('/js/admin/categories/categoryList.js')}}"></script>
    <script>
        var uploadedDocumentMap = {};
        var removeCurrentFile = '';
        Dropzone.options.documentDropzone = {

            url: '{{ route('projects.storeMedia') }}',
            // maxFilesize: 2, // MB
            maxFiles: 1,
            uploadMultiple: false,
            addRemoveLinks: true,
            // acceptedFiles: ".jpeg,.jpg,.png,.gif",
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            success: function (file, response) {

                @if($isCreate)
                $('#catForm').append('<input type="hidden" name="gallery[]" value="' + response.name + '">')
                @else
                $('#catForm').append('<input type="hidden" name="gallery[]" value="' + response.name + '">')
                @endif

                    uploadedDocumentMap[file.name] = response.name
            },
            sending: function () {
                $('.dz-image-preview').remove();
            },
            init: function () {
                @if(isset($project) && $project->gallery)
                var files =
                {!! json_encode($project->gallery) !!}
                    for (var i in files) {
                    var file = files[i]
                    console.log(file);
                    this.options.addedfile.call(this, file)
                    file.previewElement.classList.add('dz-complete')
                    $('#catForm').append('<input type="hidden" name="gallery[]" value="' + file.file_name + '">')
                }
                @endif
                    this.on("maxfilesexceeded", function (file) {
                    this.removeAllFiles();
                    this.addFile(file);
                });

                this.on('addedfile', function (file) {
                    var preview = document.getElementsByClassName('dz-preview');
                    preview = preview[preview.length - 1];
                    var imageName = document.createElement('span');
                    imageName.innerHTML = file.name;
                });
                @if(!$isCreate)
                var url = '/admin/brand/brand-files/{{$model->id}}';
                var thisDropzone = this;
                $.getJSON(url, function (data) { // get the json response
                    console.log(data);
                    if (Array.isArray(data)) {
                        removeCurrentFile = data[0].name;
                    }
                    $.each(data, function (key, value) { //loop through it
                        if (value.hasOwnProperty('shortName')) {
                            var mockFile = {name: value.shortName, size: value.size}; // here we get the file name and size as response
                            thisDropzone.options.addedfile.call(thisDropzone, mockFile);
                            thisDropzone.options.thumbnail.call(thisDropzone, mockFile, value.name);//uploadsfolder is the folder where you have all those uploaded files
                        }
                    });
                });
                @endif
            },
            removedfile: function (file) {
                if (file.status === 'success') {
                    var foolFileName = {};
                    var name = '';
                    file.previewElement.remove();

                    foolFileName = file.xhr.responseText;
                    name = '';
                    if (typeof file.file_name !== 'undefined') {
                        name = file.file_name
                    } else {
                        name = uploadedDocumentMap[file.name]
                    }

                    var mainGallery = $('#document-dropzone');
                    var alert = $(mainGallery).siblings(".alert");
                    $(alert).find(".infoError").text('Тип файла не изображение');
                    $(alert).removeClass("d-block");
                    $.ajax({
                        url: '{{ route('projects.delete')}}',
                        type: 'post',
                        data: {
                            name: foolFileName,
                            filePath: removeCurrentFile
                        },
                        headers: {
                            'X-CSRF-TOKEN': "{{ csrf_token() }}"
                        },
                        dataType: 'json',
                        success: function (data) {
                            console.info(data);
                        }
                    });


                } else {
                    file.previewElement.remove();
                    $('form').find('input[name="gallery[]"][value="' + name + '"]').remove();
                }

            },
        };
        Dropzone.prototype.defaultOptions.dictRemoveFile = "Удалить файл";
    </script>
@endpush
