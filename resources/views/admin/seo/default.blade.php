@extends('admin.layouts.admin_template')
@push('style')
    <link href="{{asset('/css/admin/product.css')}}" rel="stylesheet">
@endpush
@section('content')
    @if($isCreate)
        <h1 class="h3 mb-4 text-gray-800 text-center pt-3">Добавить SEO ко всему сайту</h1>
    @else
            <h1 class="h3 mb-4 text-gray-800 text-center pt-3">Обновить SEO Сайта</h1>
    @endif
    <div class="card shadow mb-4">
        <div class="row justify-content-center">
            @if($isCreate)
                {{Form::model($seo, array('route' => array('seo.save'),
                         'id'=>'seoForm', 'class'=> 'w-75 pt-5 pb-5','method' => 'POST', 'enctype'=>'multipart/form-data')) }}
            @else
                {{Form::model($seo, array('route' => array('seo.update', $seo->id),
                           'id'=>'seoForm', 'class'=> 'w-75 pt-5 pb-5','method' => 'PUT', 'enctype'=>'multipart/form-data')) }}
            @endif
            @include('admin.seo.seo_fields', ['isMetatitile' => true])

            @if($isCreate)
                {{Form::submit('Создать',
['class' => 'btn btn-form btn-success shadow-custom text-uppercase border-radius-50 btn-lg w-100 mt-2 mb-3'])}}
            @else
                {{Form::submit('Обновить',
['class' => 'btn btn-form btn-success shadow-custom text-uppercase border-radius-50 btn-lg w-100 mt-2 mb-3'])}}
            @endif

            {{ Form::close() }}
            @if(!$isCreate)
                <button class="delete ml-auto btn btn-danger mb-5 float-right"
                        attr-delete="{{$seo->id}}"
                        attr-route="{{route("seo.delete")}}">Удалить&nbsp;<i class="far fa-trash-alt"></i></button>
            @endif
        </div>
    </div>
@endsection
@push('scripts')
    <script src="{{asset('/js/admin/seo/index.js')}}"></script>
@endpush
