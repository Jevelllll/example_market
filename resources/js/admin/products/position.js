$(document).ready(function () {
    $(function () {
        $("#sortable").sortable({}).disableSelection();

        let positionPostBrn = document.getElementById('sendPosition');
        positionPostBrn.addEventListener('click', () => {
            let listOf = document.querySelectorAll('.item_sortable');
            const commonData = [];
            let index = 1;
            for (let list of listOf) {
                commonData.push(
                    {
                        id: Number(list.getAttribute('attr-picture-id')),
                        position_index: Number(index)
                    })
                index++;
            }
            post(positionPostBrn.getAttribute('attr-position-update'), commonData);

            function post(url, data) {
                data = data.map((item) => {
                    return `${item.id}=${item.position_index}`
                }).join('&')
                console.log(data);
                let httpRequest = new XMLHttpRequest();
                httpRequest.open('POST', url, true);
                httpRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                httpRequest.setRequestHeader(
                    'X-CSRF-TOKEN',
                    document.querySelector("meta[name='csrf-token']").getAttribute('content'));
                httpRequest.setRequestHeader("X-Requested-With", "XMLHttpRequest");
                httpRequest.responseType = 'json';
                httpRequest.send(data);
                httpRequest.onload = () => {
                    if (httpRequest.status === 200) {
                        swal({
                            title: 'Успех!',
                            text: 'Позиция обновлена!',
                            type: "success",
                            confirmButtonText: "OK",
                            showCancelButton: false
                        }).then(() => {
                            window.location.href = positionPostBrn.getAttribute('attr-current-url');
                        });
                    }
                };
                httpRequest.onerror = () => {
                }
            }
        });
    });
});
