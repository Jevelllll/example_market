import {Spinner} from "../../my-component/spinner";

let elementsByTagName = document.getElementsByTagName('body');
(function ($, undefined) {

    var purchaseContent = document.getElementById("purchase-content"),

        removeCartElement = null,
        baseUrl = "/product/",
        isNotEmpty = () => [].slice
            .call(document.getElementById("purchase-content")
                .getElementsByClassName("cart-purchase")).length > 0,
        purchase = getPurchase();

    var addCartElement = null,
        productId = null;


    function checkByIds(cb) {

        var purchase = localStorage.getItem('purchase');
        if (!purchase){
            purchase = '[]';
        }
        try {
            purchase = JSON.parse(purchase);
        } catch (e) {
        }

        let url = baseUrl + "checkByIds";
        let nPurchase = 'productId[]=0'
        if (purchase.length > 0) {
            nPurchase = purchase.map(el => {
                let split = el.split(':');
                return `${split[0]}[] = ${split[1]}`
            }).join('&');
        }
        let httpRequest = new XMLHttpRequest();
        httpRequest.open('POST', url, true);
        httpRequest.setRequestHeader(
            'X-CSRF-TOKEN',
            document.querySelector("meta[name='csrf-token']").getAttribute('content'));
        httpRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        httpRequest.setRequestHeader("X-Requested-With", "XMLHttpRequest");
        httpRequest.responseType = 'json';
        httpRequest.send(nPurchase)
        httpRequest.onload = () => {
            if (httpRequest.status === 200) {
                cb(httpRequest.response);
            }
        };
        httpRequest.onerror = () => {
            cb(false);
        }
    }

    function initPurchase(productId = null) {
        var purchase = localStorage.getItem('purchase');

        try {
            purchase = JSON.parse(purchase);
        } catch (e) {
        }

        var purchaseList = new Set();
        if (productId) {
            purchaseList.add('productId:' + productId);
        }
        if (purchase) {
            purchase.forEach(function (item) {
                if (item) {
                    purchaseList.add(item);
                }
            });
        }
        pushShow(Array.from(purchaseList).length, Array.from(purchaseList));
        localStorage.setItem('purchase', JSON.stringify(Array.from(purchaseList)));
        return purchase;
    }
    console.log(2)
    checkByIds((data) => {
        let purchase;
        var purchaseList = new Set();
        if (data) {
            purchase = data;
            purchase.forEach(function (item) {
                if (item) {
                    purchaseList.add(item);
                }
            });
            pushShow(Array.from(purchaseList).length, Array.from(purchaseList));
            localStorage.setItem('purchase', JSON.stringify(Array.from(purchaseList)));
            return purchase;
        }
    });

    initPurchase();

    $("body").on('click', '.purchase', function () {
        var productId = $(this).attr('data-purchase');
        var purchase = localStorage.getItem('purchase');
        if (purchase) {
            initPurchase(productId);
        } else {
            localStorage.setItem('purchase', JSON.stringify(['productId:' + productId]));
        }
    }).on("click", "#add-cart", function () {
        addCartProduct(purchase);
    }).on("click", "#remove-cart", function () {
        deleteCartProduct(purchase);
    })
    // shopping-bag.png
    function pushShow(count, datas = null) {
        count = count > 0 ? count : '';
        let dataImageUrl = $('.shopping[data-image]').attr('data-image');
        dataImageUrl = `${dataImageUrl}/shopping-bag.png`;
        $("#orderCart").html('<img src="'+dataImageUrl+'" class="shopping-bag hover-logo">&nbsp;' +
            '<span class="shopping-bag-item"></span>')
            // .html('<i class="fa fa-shopping-cart" aria-hidden="true">&nbsp;</i>' + count)
            .html('<img src="'+dataImageUrl+'" class="shopping-bag hover-logo">&nbsp;' +
                '<span class="shopping-bag-item">' + count + '</span>')
            .attr('data-purchase', JSON.stringify(datas));
        $("#orderCart1").html('<img src="'+dataImageUrl+'" class="shopping-bag hover-logo">&nbsp;' +
            '<span class="shopping-bag-item"></span>')
            // .html('<i class="fa fa-shopping-cart" aria-hidden="true">&nbsp;</i>' + count)
            .html('<img src="'+dataImageUrl+'" class="shopping-bag hover-logo">&nbsp;' +
                '<span class="shopping-bag-item">' + count + '</span>')
            .attr('data-purchase', JSON.stringify(datas));
    }

    function recalculation() {
        let collectionOf = [].slice.call(purchaseContent.getElementsByClassName("purchase-coast"));
        let sum = 0;
        try {
            sum = collectionOf.map((element) => parseFloat(element.innerText)).reduce((a, b) => a + b);
        } catch (e) {

        }
        let htmlElement = document.getElementById("total-sum");
        htmlElement.innerHTML = `${sum} USD&nbsp;&nbsp;&nbsp;&nbsp;`;
    }

    /******************** Open the basket and fill it********************************/
    function basketFilling() {
        Spinner();
        Spinner.show();
        var dataPurchare;
        try {
            dataPurchare = localStorage.getItem("purchase");
        } catch (e) {
        }

        if (dataPurchare) {
            $.ajax({
                async: false,
                method: 'POST',
                url: baseUrl + "by_ids",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {data: dataPurchare},
                beforeSend: function () {
                },
                success: function (data) {
                    if (data.msg === 'empty') {
                        purchaseContent.innerHTML = " <div style='    width: 130px;\n" +
                            "    margin: 0 auto;'> <img style='max-width: 100%;'" +
                            " src=\"/images/product/empty_cart.png\"> </div>";
                    } else {
                        purchaseContent.innerHTML = "";
                        purchaseContent.innerHTML = data;
                        recalculation();
                    }
                    Spinner.hide();
                },
                error: function (data) {
                    Spinner.hide();
                }

            });
        }
    }

    function openShopCart() {

        let modal = document.getElementById("myModal-purchare"),
            btn = document.getElementById("orderCart"),
            btn1 = document.getElementById("orderCart1"),
            span = document.getElementsByClassName("close-purchare")[0];

        btn1.onclick = function () {
            if (elementsByTagName) {
                let item = localStorage.getItem('purchase');
                if (item !== "[]" && item) {
                    basketFilling();
                    removePurchare();
                    elementsByTagName[0].style.overflow = 'hidden';
                    modal.style.display = "block";
                }
            }
        }

        btn.onclick = function () {

            if (elementsByTagName) {
                let item = localStorage.getItem('purchase');
                if (item !== "[]" && item) {
                    basketFilling();
                    removePurchare();
                    elementsByTagName[0].style.overflow = 'hidden';
                    modal.style.display = "block";
                }
            }
        };
        span.onclick = function () {
            if (elementsByTagName) {
                elementsByTagName[0].style.overflow = 'auto';
            }
            modal.style.display = "none";
        };

        window.onclick = function (event) {
            if (event.target === modal) {
                if (elementsByTagName) {
                    elementsByTagName[0].style.overflow = 'auto';
                }
                modal.style.display = "none";
            }
        }
    }

    /******************delete Purchare ****/
    function deleteById(element, isFromProductCard = false) {
        var productId = parseInt(element.getAttribute('remove-id'));
        if (productId && productId > 0) {

            var cartPurchase = element.parentElement;

            if (!isFromProductCard)
                cartPurchase.remove();

            var item = localStorage.getItem('purchase');
            try {
                item = JSON.parse(item);
            } catch (e) {
            }
            if (item && item !== "[]") {
                let updatePrP = item.filter((prId) => prId !== 'productId:' + productId);
                localStorage.setItem("purchase", JSON.stringify(updatePrP));
                pushShow(0, []);
                recalculation();
                pushShow(updatePrP.length, null);
                detectChangeCart();
                if (!isNotEmpty()) {
                    purchaseContent.innerHTML = " <div style='    width: 130px;\n" +
                        "    margin: 0 auto;'> <img style='max-width: 100%;'" +
                        " src=\"/images/product/empty_cart.png\"> </div>";

                }
            }
        }
        // }
    }

    function removePurchare() {
        purchase = getPurchase();
        var rList = purchaseContent.getElementsByClassName('cart-remove');
        [].slice.call(rList).forEach(function (element) {
            element.addEventListener("click", () => {
                deleteCartProduct(purchase, element);
                deleteById(element);
            });
        })
    }

    function removeFromProductCard() {
        var rList = document.getElementsByClassName('removeEl');
        [].slice.call(rList).forEach(function (element) {
            element.addEventListener("click", () => {
                deleteById(element, true);
            });
        })
    }

    removeFromProductCard();

    /****************** items added to cart********************/
    /******************< detectChangeCart********************/
    function addCartProduct(purchase) {
        productId = parseInt(addCartElement.getAttribute("data-purchase"));
        purchase = getPurchase();
        if (addCartElement) {
            if (productId && purchase) {
                const isBuy = purchase.find((pIt) => pIt === productId);

                if (isBuy) {
                    removeCartElement.style.display = "block";
                    addCartElement.style.display = "none";
                }
            }
        }
    }

    function deleteCartProduct(purchase, element = null) {
        if (removeCartElement || element) {
            if (removeCartElement) {
                var productIdRemove = parseInt(removeCartElement.getAttribute("remove-id"));
                productId = parseInt(removeCartElement.getAttribute("remove-id"));
                if (element) {
                    productId = parseInt(element.getAttribute("remove-id"));
                }
                purchase = getPurchase();
                if (removeCartElement || element) {
                    if (productId && purchase) {
                        const isBuy = purchase.find((pIt) => pIt === productId);
                        if (isBuy) {
                            if (element) {
                                if (productIdRemove === productId) {
                                    removeCartElement.style.display = "none";
                                    addCartElement.style.display = "block";
                                    deleteById(removeCartElement, true);
                                }
                            } else {
                                deleteById(removeCartElement, true);
                                removeCartElement.style.display = "none";
                                addCartElement.style.display = "block";
                            }
                        }
                    }
                }
            }

        }

    }

    function getPurchase() {
        let purchase = localStorage.getItem('purchase');
        if (purchase) {
            purchase = JSON.parse(purchase);
            if (Array.isArray(purchase)) {
                purchase = purchase.map((pCh) => {
                    let notErrElementPurchase;
                    try {
                        notErrElementPurchase = parseInt(pCh.split(":").reverse()[0]);
                    } catch (e) {
                    }
                    return notErrElementPurchase;
                }).filter((it) => it);
            }
        }
        return purchase;
    }

    function detectChangeCart() {
        let purchase = getPurchase();

        function instanceActiveCart(cart, deleteActiveAll = false) {
            let activateItem = cart.getElementsByClassName("bottom")[0];

            if (activateItem) {
                if (deleteActiveAll) {
                    activateItem.classList.remove("clicked");
                } else {
                    if (activateItem.classList.contains("clicked")) {
                    } else {
                        activateItem.classList.add("clicked");
                    }
                }

            }
        }

        let pathname = window.location.pathname;
        if (pathname.indexOf("catalog") !== -1 || pathname.indexOf("product") !== -1) {
            let wrapCart = document.getElementsByClassName("wrapper");
            [].slice.call(wrapCart).forEach((cart) => {
                let buy = cart.getElementsByClassName("buy")[0];
                if (buy && typeof buy === "object") {

                    if (buy.hasAttribute("data-purchase")) {
                        let idProduct = parseInt(buy.getAttribute("data-purchase"));
                        if (Array.isArray(purchase)) {
                            const isBuy = purchase.filter((pIt) => pIt === idProduct);
                            if (isBuy.length > 0) {
                                instanceActiveCart(cart);
                            } else {
                                instanceActiveCart(cart, true);
                            }
                        }
                    }
                }
            });
            addCartElement = document.getElementById("add-cart");
            if (addCartElement) {
                productId = parseInt(addCartElement.getAttribute("data-purchase"));
                removeCartElement = document.getElementById("remove-cart");
                addCartProduct(purchase);
            }
        } else if (pathname.indexOf("product") !== -1) {

            addCartElement = document.getElementById("add-cart");

            productId = parseInt(addCartElement.getAttribute("data-purchase"));
            removeCartElement = document.getElementById("remove-cart");
            addCartProduct(purchase);
        }
    }

    /****************** </ detectChangeCart********************/

    detectChangeCart();
    openShopCart();
})(jQuery);

elementsByTagName[0].onclick = function (ev) {
    let querySelectorModals = this.querySelectorAll(".modal-warning, .modal-success, .modal-purchare");
    if (ev.target.className === 'close-purchare') {
        this.style.overflow = 'auto';
        querySelectorModals.forEach((querySelectorModal) => {
            querySelectorModal.style.display = 'none';
        });
    }
}
