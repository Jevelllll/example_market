<?php

namespace App\Http\Controllers\Admin;

use App\City;
use App\Http\Controllers\Controller;
use App\State;
use Illuminate\Http\Request;

class CitiesController extends Controller
{
    public function save()
    {
    }

    public function index()
    {
        $stateList = State::all();
        if ($stateList) {
            if ($stateList[0] instanceof State) {
                return view('admin.city.list', compact('stateList'))
                    ->withTitle('Города PayPal');
            } else {
                dd(503);
            }
        } else {
            $data = ["stateList" => null];
            return view('admin.city.list', $data)
                ->withTitle("Города PayPal");
        }
    }

    public function getCitiesByStateId($id)
    {
        $citiesList = City::with('state')->select('id', 'name', 'state_id', 'country_code')
            ->where('state_id', $id)->get()->toArray();
        $newCityList = [];
        foreach ($citiesList as $key => $value) {
            $newCity = new City();
            foreach ($value as $k => $v) {
                if ($k == 'state') {
                    $newCity->state_id = $v['name'];
                } else {
                    $newCity->$k = $v;
                }

            }
            $newCityList[] = $newCity;
        }
        $citiesList = $newCityList;

        if (count($citiesList) > 0) {
            if ($citiesList[0] instanceof City) {
                $citiesTitle = collect($citiesList)[0]->toArray();
                $citiesTitle = array_keys($citiesTitle);
                return view('admin.city.citiesList', compact('citiesList', 'citiesTitle'))
                    ->withTitle('Города PayPal');
            } else {
                $citiesList = null;
                $citiesTitle = null;
                return view('admin.city.citiesList', compact('citiesList', 'citiesTitle'))
                    ->withTitle('Города PayPal');
            }
        } else {
            $citiesList = null;
            $citiesTitle = null;
            return view('admin.city.citiesList', compact('citiesList', 'citiesTitle'))
                ->withTitle('Города PayPal');
        }

    }

    public function edit($id)
    {
        $isCreate = false;
        $model = City::where('id', (int)$id)->first();

        if ($model instanceof City) {
            return view('admin/city/create', compact("isCreate", "model"))->withTitle('Редактировать город');
        } else {
            dd(503);
        }
    }

    public function update(Request $request, $id)
    {
        $requestAll = $request->all();
        $model = City::where('id', (int)$id)->first();

        if ($model instanceof City) {
            if ($model->update($requestAll)) {
                return response()->json([
                    'msg' => true,
                ], 200);
            } else {
                return response()->json([
                    'msg' => false,
                ], 404);
            }
        } else {
            return response()->json([
                'msg' => false,
            ], 404);
        }

    }

    public function delete(Request $request)
    {
        $requestData = $request->all();
        $model = City::find($requestData['id']);
        if ($model instanceof City) {
            if ($model->delete()) {
                return response()->json([
                    'msg' => true,
                ], 200);
            } else {
                return response()->json([
                    'msg' => false,
                ], 404);
            }
        } else {
            return response()->json([
                'msg' => false,
            ], 404);
        }
    }

}
