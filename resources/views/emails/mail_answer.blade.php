<!doctype html>
<html lang="en" style="height: 100%;padding: 0;margin: 0;width: 100%;font-family: arial,'helvetica neue',helvetica,sans-serif;">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Message reply</title>
</head>
<style>
    body, html {
        height: 100%;
        padding: 0;
        margin: 0;
        width:100%;
        font-family:arial,'helvetica neue',helvetica,sans-serif;
        /*background-color:#f6f6f6*/
    }
    .container {
        border-radius: 13px;
        max-width: 600px;
        margin: 0 auto;
        background-color: rgba(0, 128, 0, 0.74);
        color: white;
    }
    h1 {
        margin: 0;
        padding: 0 1rem;
        text-align: center;
    }
    hr {
        padding: 0;
        margin: 0;
    }
    .main_content {
        line-height: 1.5;
        text-align: center;
        font-family: helvetica,'helvetica neue',arial,verdana,sans-serif;
        font-size: 18px;
    }
    .history_content {
        padding: 16px 0 1rem 16px;
        font-size: 21px;
        font-family: helvetica,'helvetica neue',arial,verdana,sans-serif;
    }
    .your {
        font-weight: 600;
        text-align: center;
        text-transform: uppercase;
        padding-bottom: 1rem;
    }
    .your_message {
        line-height: 1.5;
        text-align: center;
        font-family: helvetica,'helvetica neue',arial,verdana,sans-serif;
        font-size: 18px;
    }
    .your_message_product {
        padding-top: 15px;
    }
    .answ_your_msg {
        font-weight: 600;
        text-align: center;
        text-transform: uppercase;
        font-size: 21px;
        line-height: 1.5;
        font-family: helvetica,'helvetica neue',arial,verdana,sans-serif;
    }
    a {
        color: white;
        text-decoration: none;
    }
    .top_element {
        text-align: center;
        padding: 14px;
        font-size: 28px;
        font-weight: 700;
    }
    .top_element a {
        color: lightsteelblue;
    }
</style>
<body style="height: 100%;padding: 0;margin: 0;width: 100%;font-family: arial,'helvetica neue',helvetica,sans-serif;">
<div class="container" style="border-radius: 13px;max-width: 600px;margin: 0 auto;background-color: rgba(0, 128, 0, 0.74);color: white;">
    <div class="top_element" style="text-align: center;padding: 14px;font-size: 28px;font-weight: 700;">
        <a href="{{env('APP_URL')}}" style="text-decoration: none;color: lightsteelblue;">{{env('APP_NAME')}}</a>
        <hr style="padding: 0;margin: 0;">
    </div>
{{--    <h1 style="margin: 0;padding: 0 1rem;text-align: center;">Hello {{$answer->from}}</h1>--}}
    <div class="answ_your_msg" style="font-weight: 600;text-align: center;text-transform: uppercase;font-size: 21px;line-height: 1.5;font-family: helvetica,'helvetica neue',arial,verdana,sans-serif;">
        Reply to your message
    </div>
    <div class="main_content" style="line-height: 1.5;text-align: center;font-family: helvetica,'helvetica neue',arial,verdana,sans-serif;font-size: 18px;">
        @if(isset($messageTo))
            <p>{{$messageTo}}</p>
        @endif
    </div>
    <hr style="padding: 0;margin: 0;">
    <div class="history_content" style="padding: 16px 0 1rem 16px;font-size: 21px;font-family: helvetica,'helvetica neue',arial,verdana,sans-serif;">
        <div class="your" style="font-weight: 600;text-align: center;text-transform: uppercase;padding-bottom: 1rem;">
            your message
        </div>
        <div class="your_message" style="line-height: 1.5;text-align: center;font-family: helvetica,'helvetica neue',arial,verdana,sans-serif;font-size: 18px;">
            {{$answer->message}}
        </div>
        <div class="your_message_product" style="padding-top: 15px;">
            <ul style="list-style: square outside;margin: 0;">
                <li style="display: flex;-webkit-box-align: center;-ms-flex-align: center;align-items: center;">
                    <div class="pr" style="padding-right: 10px;">
                        <a href="{{env('APP_URL')}}/product/{{$product->seo_name}}" style="color: white;text-decoration: none;">
                            <img src="{{asset('images/product/pictures/thumbnails/'. $product->pictures[0]->image_list)}}" alt="{{$product->name}}" style="max-height: 50px;">
                        </a>
                    </div>
                    <div class="pr" style="padding-right: 10px;">
                        <a href="{{env('APP_URL')}}/product/{{$product->seo_name}}" style="color: white;text-decoration: none;">
                            {{$product->name}}
                        </a>
                    </div>
                    <div class="pr order" style="font-family: 'Montserrat', sans-serif;font-weight: 700;padding-right: 10px;">{{$product->price}}
                        USD
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>

</body>
</html>
