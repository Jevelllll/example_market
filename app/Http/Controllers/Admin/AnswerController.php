<?php

namespace App\Http\Controllers\Admin;

use App\Answers;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Message\MessageGmailController;
use App\Products;
use Illuminate\Http\Request;


class AnswerController extends Controller
{
    public function index()
    {
        $answers = Answers::select('id', 'viewed', 'from', 'created_at')->whereNull('parent_id')->orderBy('id', 'desc')->get();
        if (count(collect($answers)->toArray()) > 0) {
            if ($answers[0] instanceof Answers) {
                $answersTitle = collect($answers)[0]->toArray();
                $answersTitle = array_keys($answersTitle);
                return view('admin.answer.index', compact('answers', 'answersTitle'))->withTitle('Список сообщений');
            } else {
                dd(503);
            }
        } else {
            $data = ["answersTitle" => null, "answers" => null];
            return view('admin.answer.index', $data)
                ->withTitle('Список сообщений');
        }
    }

    public function actionSendAnswerTo(Request $request)
    {
        $allPost = $request->all();
        if (isset($allPost['product_id']) && isset($allPost['answer_id'])) {
            $answer = Answers::where('id', $allPost['answer_id'])->first();
            $product = $answer->product()->first();
            if ($product instanceof Products) {
                if ($answer instanceof Answers) {
                    $message = $allPost['message'];
                    $newAnswer = new Answers();
                    $newAnswer->message = $message;
                    $newAnswer->parent_id = (int) $answer->id;
                    $newAnswer->to = $answer->from;
                    $newAnswer->from = $answer->to;
                    $newAnswer->product_id = (int) $allPost['product_id'];
                    $newAnswer->viewed = '1';
                    if ($newAnswer->save()) {
                        $messageGmail = new MessageGmailController('', $answer->from, new \stdClass(), 'Message reply');
                        $messageGmail->actionSendAnswer($answer, $product, $message);
                        return response()->json(['status' => 'success'], 200);
                    }
                    return response()->json( ['status' => 'error'], 404);
                }
            }
            return response()->json(['status' => 'error'], 404);
        } else {
            return response()->json( ['status' => 'error'], 404);
        }
    }

    public function show($id)
    {
        $answer = Answers::where('id', $id)->orderBy('id', 'desc')->first();
        $answer->viewed = '1';
        $answer->save();
        unset($answer->updated_at);
        unset($answer->viewed);
        unset($answer->type);
        unset($answer->parent_id);
        if ($answer instanceof Answers) {
            return view('admin.answer.show', compact('answer'))
                ->withTitle('Сообщение от ' . $answer->from);
        } else {
            dd(503);
        }
//        $order = Orders::where('id', $id)->select('id', 'order_nr', 'total_sum', 'created_at', 'status')
//            ->with(['order_product', 'customer' => function ($qw) {
//                return $qw->select(
//                    'order_id', 'first_name', 'last_name', 'address_country',
//                    'address_state', 'address_city', 'address_street', 'address_house_number',
//                    'address_zip_code', 'email', 'phone', 'comment')->get();
//            }])->orderBy('created_at', 'desc')->first();
//
//        $orderProducts = $order->order_product;
//        $customer = $order->customer;
//        unset($order->order_product);
//        unset($order->customer);
//        unset($order->id);
//        unset($customer->order_id);
//        Orders::readNewOrder($id);
//        $transactions = Transactions::where('invoice', 'like', '%-' . $order->order_nr)
//            ->orderBy('id', 'desc')->get();
//        return view('admin.order.show', compact('order', 'transactions', 'orderProducts', 'customer'))
//            ->withTitle('Заказ ' . $order->order_nr);
    }

}
