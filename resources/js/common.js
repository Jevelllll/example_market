import {Spinner} from "../my-component/spinner";

export function redirectRootCategories(containerId = 'categoryContainer', querySAll = 'categoryLink', targetSelector = 'LI') {
    // const categoryContainer = document.getElementById(containerId);
    // let querySelectorAll = categoryContainer.getElementsByClassName(querySAll);
    //
    // function detectLinkChange(selector) {
    //     selector.preventDefault();
    //     console.log(targetSelector)
    //     if (selector.target.nodeName === targetSelector) {
    //         let querySelector = selector.target.querySelector('a');
    //         if (typeof querySelector === 'object') {
    //             if (querySelector) {
    //                 querySelector.click();
    //             }
    //         }
    //     }
    // }
    //
    // [].slice.call(querySelectorAll).forEach(function (value, index, array) {
    //     addEventListener('click', detectLinkChange, false);
    // });

}

////////////////////////////////
export function viewErrorsAfterSubmit(data, payForm, bottomError = false) {
    if (data.hasOwnProperty('errors')) {
        for (const property in data.errors) {

            let propertyS = (property === 'phone') ? 'h-phone' : property;
            const selector = payForm.querySelector(`#${propertyS}`)
            errorKeyUp(selector, bottomError);
            addClass(selector, 'error-input', () => {
                if (bottomError) {
                    let elementError = selector.parentElement.querySelector('.error-message');
                    elementError.classList.remove('d-none');
                    elementError.classList.remove('d-flex');
                    elementError.textContent = '';
                    elementError.textContent = data.errors[property][0];
                    let labelElement = selector.nextElementSibling;
                    addClass(labelElement, 'color-red', () => {
                    });
                } else {
                    let labelElement = selector.nextElementSibling;
                    if (labelElement) {
                        let labelText = labelElement.textContent;
                        labelElement.textContent = labelText.replace('*', `(${data.errors[property][0]})`);
                        addClass(labelElement, 'color-red', () => {
                        });
                    }
                }
            });
        }
    }
}

/****/
function errorKeyUp(element, bottomError = false) {
    let elementVal;
    let labelUPD;

    element.addEventListener('click', (el) => {
        let element = el.target;
        let label = element.nextElementSibling
        elementVal = element.value;
        labelUPD = label.textContent;
        if (element) {
            if (element.disabled) {
            } else {
                element.classList.remove('error-input');
                label.classList.remove('color-red');
                label.textContent = labelUPD.replace(/\(.*?\)/g, '')
            }
        }
    })
    if (bottomError) {

    } else {
        element.addEventListener('focusout', (el) => {

            let element = el.target;
            let label = element.nextElementSibling
            if (element) {
                if (element.disabled) {
                } else {
                    if (element.value === elementVal) {
                        element.classList.add('error-input');
                        label.classList.add('color-red');
                        label.textContent = labelUPD;
                    }
                }
            }
        })
    }

}
export function addClass(element, className, cb) {
    try {
        if (element.classList.contains(className)) {
        } else {
            element.classList.add(className);
            cb();
        }
    } catch ($e) {
    }
    cb();
}
///////////////////////////////




export function paginationWork() {
    var paginationContainer = document.getElementById("paginationContainer");
    function paginationEvent() {
        var pagination = document.getElementsByClassName("pagination");
        if (pagination) {
            [].slice.call(pagination).forEach(function (el) {
                var cEl = el.getElementsByClassName("page-link");
                [].slice.call(cEl).forEach(function (link, i) {

                    if (link.nodeName === "A") {
                        link.addEventListener("click", clickedLink);
                    }
                })
            });
        }
    }

    paginationEvent();

    function clickedLink(el) {
        el.preventDefault();
        var find = $(document).find(".page-item");
        find.each(function (i, el) {
            $(el).removeClass("active");
        });
        el.target.parentElement.classList.add("active")
        var hrefLink = el.target.getAttribute("href");
        var foolProducts = document.getElementById("fool-products");
        window.history.replaceState('', '', hrefLink);
        $.ajax({
            method: "GET",
            url: hrefLink,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                var findWrapper = $(foolProducts);
                var dataResponseWrapper = $(data).find(".wrapper");
                var h1 = $(data).find("h1");
                var pgAgin = $(data).find(".pagination");
                $(paginationContainer).html(pgAgin);
                $('h1').text(h1.text());
                findWrapper.html(dataResponseWrapper);
                paginationEvent();

            }
        });
    }
}

function autocomplete(inp, arr) {
    /*the autocomplete function takes two arguments,
    the text field element and an array of possible autocompleted values:*/
    var currentFocus;
    /*execute a function when someone writes in the text field:*/
    inp.addEventListener("input", function(e) {
        var a, b, i, val = this.value;
        /*close any already open lists of autocompleted values*/
        closeAllLists();
        if (!val) { return false;}
        currentFocus = -1;
        /*create a DIV element that will contain the items (values):*/
        a = document.createElement("DIV");
        a.setAttribute("id", this.id + "autocomplete-list");
        a.setAttribute("class", "autocomplete-items");
        /*append the DIV element as a child of the autocomplete container:*/
        this.parentNode.appendChild(a);
        /*for each item in the array...*/
        for (i = 0; i < arr.length; i++) {
            /*check if the item starts with the same letters as the text field value:*/
            if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                /*create a DIV element for each matching element:*/
                b = document.createElement("DIV");
                /*make the matching letters bold:*/
                b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                b.innerHTML += arr[i].substr(val.length);
                /*insert a input field that will hold the current array item's value:*/
                b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                /*execute a function when someone clicks on the item value (DIV element):*/
                b.addEventListener("click", function(e) {
                    /*insert the value for the autocomplete text field:*/
                    inp.value = this.getElementsByTagName("input")[0].value;
                    /*close the list of autocompleted values,
                    (or any other open lists of autocompleted values:*/
                    closeAllLists();
                });
                a.appendChild(b);
            }
        }
    });
    /*execute a function presses a key on the keyboard:*/
    inp.addEventListener("keydown", function(e) {
        var x = document.getElementById(this.id + "autocomplete-list");
        if (x) x = x.getElementsByTagName("div");
        if (e.keyCode == 40) {
            /*If the arrow DOWN key is pressed,
            increase the currentFocus variable:*/
            currentFocus++;
            /*and and make the current item more visible:*/
            addActive(x);
        } else if (e.keyCode == 38) { //up
            /*If the arrow UP key is pressed,
            decrease the currentFocus variable:*/
            currentFocus--;
            /*and and make the current item more visible:*/
            addActive(x);
        } else if (e.keyCode == 13) {
            /*If the ENTER key is pressed, prevent the form from being submitted,*/
            e.preventDefault();
            if (currentFocus > -1) {
                /*and simulate a click on the "active" item:*/
                if (x) x[currentFocus].click();
            }
        }
    });
    function addActive(x) {
        /*a function to classify an item as "active":*/
        if (!x) return false;
        /*start by removing the "active" class on all items:*/
        removeActive(x);
        if (currentFocus >= x.length) currentFocus = 0;
        if (currentFocus < 0) currentFocus = (x.length - 1);
        /*add class "autocomplete-active":*/
        x[currentFocus].classList.add("autocomplete-active");
    }
    function removeActive(x) {
        /*a function to remove the "active" class from all autocomplete items:*/
        for (var i = 0; i < x.length; i++) {
            x[i].classList.remove("autocomplete-active");
        }
    }
    function closeAllLists(elmnt) {
        /*close all autocomplete lists in the document,
        except the one passed as an argument:*/
        var x = document.getElementsByClassName("autocomplete-items");
        for (var i = 0; i < x.length; i++) {
            if (elmnt != x[i] && elmnt != inp) {
                x[i].parentNode.removeChild(x[i]);
            }
        }
    }
    /*execute a function when someone clicks in the document:*/
    document.addEventListener("click", function (e) {
        closeAllLists(e.target);
    });
}
HTMLElement.prototype.serialize = function(){
    let requestArray = [];
    this.querySelectorAll('[name]').forEach((elem) => {
        requestArray.push(elem.name + '=' + elem.value);
    });
    if(requestArray.length > 0)
        return requestArray.join('&');
    else
        return false;
}
Element.prototype.hasClass = function (className) {
    return this.className && new RegExp("(^|\\s)" + className + "(\\s|$)").test(this.className);
};
