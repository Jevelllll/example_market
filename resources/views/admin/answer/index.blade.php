@extends('admin.layouts.admin_template')
@push('style')
    <link href="{{asset('/css/admin/order_truns.css')}}" rel="stylesheet">
@endpush
@section('content')
    <span class='tegs' tegs="/admin/answer/view/"></span>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary pt-3 ml-md-1">Список сообщений</h6>
        </div>
        @if($answers)
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            @foreach($answersTitle as $titles)
                                <th>{{Translators::translateRow()[$titles]}}</th>
                            @endforeach
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            @foreach($answersTitle as $titles)
                                <th>{{Translators::translateRow()[$titles]}}</th>
                            @endforeach
                        </tr>
                        </tfoot>
                        <tbody>
                        @foreach($answers as $answer)
                            <tr data-id="{{$answer->id}}">
                                @foreach($answersTitle as $key => $titles)
                                    @if($titles === 'viewed')
                                        @if($answer->$titles == 1)
                                            <td><span class="label label-success">Да</span></td>
                                        @else
                                            <td><span class="label label-warning">Нет</span></td>
                                        @endif
                                    @else
                                        <td>{{$answer->$titles}}</td>
                                    @endif
                                @endforeach
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        @else
            <div style="font-size: 2.5rem" class="text-center">Отсутствуют данные</div>
        @endif
    </div>
@endsection
@push('scripts')
    <script src="{{asset('/js/admin/orders/ordersList.js')}}"></script>
@endpush
