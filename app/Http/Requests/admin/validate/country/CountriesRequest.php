<?php

namespace App\Http\Requests\admin\validate\country;

use Illuminate\Foundation\Http\FormRequest;

class CountriesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'value' => 'required|unique:countries',
            'name_en' => 'required|unique:countries'
        ];
    }

    public function messages()
    {
        return [
            'value.required' => 'Обязательно к заполнению',
            'value.unique' => 'Существующая запись',
            'name_en.unique' => 'Существующая запись',
            'name_en.required' => 'Обязательно к заполнению',
        ];
    }
}
