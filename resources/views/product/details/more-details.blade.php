@extends('landing.layouts.app')
@push('style')
    {{--    more-details.css--}}
    <link rel="preload" href="{{ asset('css/more-details.css') }}" as="style">
    <link rel="stylesheet"
          href="{{asset('css/more-details.css')}}?v=<?= filemtime('css/more-details.css') ?>">
    <link rel="stylesheet"
          href="{{asset('plugins/swiper/css/swiper.min.css')}}?v=<?= filemtime('plugins/swiper/css/swiper.min.css') ?>">
    {{----}}
@endpush

@section('content')

    <section class="wrap box-s-box base-shadow  mb-5 mt-5 container">
        @include('landing/particals/breadcrums', ['data' => ['categories' => [$product->category],
'subcategories' => [$product->subcategory], 'productName' => $product->name]])

        <div class="product-description d-flex justify-content-center flex-wrap ml-0 ml-sm-3 mr-0 mr-sm-3">
            <div class="img-list col-12 col-sm-12 col-md-5 col-lg-6 pt-0 pt-sm-1">
                @if(count($product->pictures) > 0)
                    <?php
                    $sortPictures = collect($product->pictures);
                    $sortPictures = $sortPictures->sortBy('position_index');
                    $sortPictures = $sortPictures->values()->all();
                    $availability = ($product->status_available == 0)? "https://schema.org/SoldOut": "https://schema.org/InStock";
                    ?>
                        @push('style')
                            @include('markup.product', [
    'product' => $product,
     'availability' => $availability,
     'picture' => asset('images/product/pictures').'/'.$sortPictures[0]->image_list
    ])
                        @endpush
                    <div class="slider-wrap">
                        <div class="w-100 hand">
                            <div>
                                @include('product.rate', ['product' => $product])
                            </div>
                            <img style="height: 42px" src="{{asset('images/svg/method-draw-image.svg')}}">
                        </div>
                        <div class="swiper-container gallery-top">
                            <div class="swiper-wrapper">
                                @foreach($sortPictures as $picture)
                                    @if ($loop->first)
                                        <div class="swiper-slide">
                                            <a data-fancybox="gallery"
                                               href="{!!asset('/images/product/pictures/').'/'.$picture->image_list!!}">
                                                <img class="img-fluid"
                                                     alt="{{$picture->image_list}}"
                                                     src="{{asset('images/product/pictures/preview').'/'.$picture->image_list}}">
                                            </a>

                                        </div>
                                    @else
                                        <div class="swiper-slide">
                                            <a data-fancybox="gallery"
                                               href="{!!asset('/images/product/pictures/').'/'.$picture->image_list!!}">
                                                <img class="img-fluid"
                                                     alt="{{$picture->image_list}}"
                                                     src="{{asset('images/product/pictures/preview').'/'.$picture->image_list}}">
                                            </a>

                                        </div>
                                    @endif

                                @endforeach
                            </div>
                            <!-- Add Arrows -->
{{--                            <div class="swiper-button-next swiper-button-white sw-button"></div>--}}
{{--                            <div class="swiper-button-prev swiper-button-white sw-button"></div>--}}
                        </div>

                        <div class="swiper-container gallery-thumbs">
                            <div class="swiper-wrapper">
                                @foreach($sortPictures as $picture)
                                    <div class="swiper-slide">
                                        <img class="img-fluid"
                                             alt="{{$picture->image_list}}"
                                             src="{{asset('images/product/pictures/thumbnails').'/'.$picture->image_list}}">
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endif
            </div>
            <div class="product-description_details col">
                <div class="d-flex align-items-center m-2 justify-content-center">
                    &nbsp;<h1 class="mt-4 mb-4 word-wrap">{{$product->name}}</h1>
                </div>
                <div class="details-right">
                    <div class="wrap-card-pay w-100">
                        <div class="price">
                            <span>Price: </span>
                            <span>{{$product->price}}<i class="fas fa-dollar-sign"></i></span>
                        </div>
                        @if((boolean) $product->is_sold)
                            &nbsp;<h2
                                class="w-100 text-white color-red-back text-center pt-1 pb-1 disabled-user-select">
                                SORRY,
                                SOLD OUT
                                <i class="fas fa-exclamation-circle"></i>
                            </h2>
                        @else
                            @include('product/details/add-cart', ['data' => $product])
                        @endif

                    </div>
                    <div class="more-info mt-2 p-3 ask_question">
                        <div class="more-info_item">
                            <button class="button-add-cart clean-gradient purchase m-auto background-reset" id="ascQw">
                                <i class="far fa-question-circle"></i>
                                Ask a Question <i class="far fa-question-circle"></i></button>
                        </div>
                        @include('components.modal.askQuestion', ['product' => $product, 'askModel' => $askModel])
                    </div>
                    <div class="more-info mt-2 p-3">
                        @if($product->category)
                            <div class="more-info_item">
                                <i class="fas fa-arrow-right"></i>
                                <span class="more-info-key">brand:&nbsp;</span>
                                <span class="more-info-val">{{$product->category->category_name}}</span>
                            </div>
                        @endif
                        @if(strlen($product->desc) > 0)
                            <div class="more-info_item">
                                <i class="fas fa-arrow-right"></i>
                                <span class="more-info-key">DESCRIPTION:&nbsp;</span>
                                <span class="more-info-val">{{$product->desc}}</span>
                            </div>
                        @endif
                        @if(strlen($product->condition) > 0)
                            <div class="more-info_item">
                                <i class="fas fa-arrow-right"></i>
                                <span class="more-info-key">condition:&nbsp;</span>
                                <span class="more-info-val">{{$product->condition}}</span>
                            </div>
                        @endif
                        @if(strlen($product->shipping) > 0)
                            <div class="more-info_item">
                                <i class="fas fa-arrow-right"></i>
                                <span class="more-info-key">free shipping:&nbsp;</span>
                                <span class="more-info-val">{{$product->shipping}}</span>
                            </div>
                        @endif
                    </div>
                    @if($product->options)
                        <div class="options-more more-info mt-2 p-3">
                            <?php
                            $options = json_decode($product->options);
                            ?>
                            @if(count($options) > 0)
                                @foreach($options as $option)
                                    <div class="more-info_item">
                                        <i class="fas fa-arrow-right"></i>
                                        <span class="more-info-key">{{$option->contentKey}}:&nbsp;</span>
                                        <span class="more-info-val">{{$option->contentVal}}</span>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    @endif
                </div>
            </div>
        </div>
        @if(count($product->bindings) > 0)
            <div class="position-relative binding-product ml-3 mr-3">
                <h2 class="text-center mt-2 mb-2 highlightTittle">COMPLETE YOUR LOOK</h2>
                <div class="swiper-container-binding">
                    <div class="swiper-wrapper" id="fool-products">
                        @foreach($product->bindings as $prod)
                            <?php $productC = $prod->prodbind ?>
                            @if($productC)
                                <div class="swiper-slide">
                                    @include('/product/product-card', ["product" => $productC])
                                </div>
                            @endif
                        @endforeach;
                    </div>
                    <!-- Add Pagination -->
                    <div class="swiper-pagination"></div>
                </div>
            </div>
        @endif
    </section>

    <div id="containerLink"
         style="display: none">@if(count($product->pictures) > 0){{trim(json_encode($product->pictures))}}@endif</div>
@endsection
@push('scripts')
    <script async src="{{asset('js/ask_question.js')}}?v=<?= filemtime('js/ask_question.js') ?>"></script>
    <script>
        $.getScript("{{ asset('plugins/swiper/js/swiper.min.js') }}?v=<?= filemtime('plugins/swiper/js/swiper.min.js') ?>", function (data, textStatus, jqxhr) {
            var galleryThumbs = new Swiper('.gallery-thumbs', {
                spaceBetween: 10,
                centeredSlides: true,
                slidesPerView: 'auto',
                touchRatio: 0.2,
                slideToClickedSlide: true,
                loop: true,
                loopedSlides: 4
            });
            var galleryTop = new Swiper('.gallery-top', {
                spaceBetween: 10,
                autoHeight: true,
                effect: 'fade',
                // navigation: {
                //     nextEl: '.swiper-button-next',
                //     prevEl: '.swiper-button-prev',
                // },
                preloadImages: false,
                // Enable lazy loading
                loop: true,
                loopedSlides: 4
            });
            galleryTop.controller.control = galleryThumbs;
            galleryThumbs.controller.control = galleryTop;

            galleryTop.init();
            galleryThumbs.init();
        });
        $.getScript("https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js", function (data, textStatus, jqxhr) {
            $('head').append('<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css">');
        });
        $.getScript("{{asset('js/product/details/more-details.js')}}?v=<?= filemtime('js/product/details/more-details.js') ?>", function (data, textStatus, jqxhr) {
        });
    </script>
@endpush
