<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductRate extends Model
{
    protected $fillable = ['product_id', 'value', 'ip', 'created_at', 'updated_at'];
}
