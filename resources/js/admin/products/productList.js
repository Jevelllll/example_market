import {detectTrTable, formSend, removeCol, translatorTableColumn} from "../base";

$(document).ready(function () {

    setTimeout(function () {
        $('#dataTable').DataTable(translatorTableColumn());
    }, 0);


    function generateSubcategories(categoryValue, subcategory) {

        return new Promise(resolve => {

            let categoryId = Number(categoryValue);
            if (categoryId > 0) {
                $.ajax({
                    async: false,
                    method: 'GET',
                    url: `/admin/product/subcat-by-category/${categoryId}`,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    beforeSend: function () {
                    },
                    success: function (data) {
                        subcategory.options.length = 0;
                        if (data) {
                            if (data.length > 0) {
                                subcategory.add(new Option('Не выбранно', null));
                                data.forEach((subCat) => {
                                    subcategory.add(new Option(subCat.name, subCat.id));
                                });
                            } else {
                                subcategory.add(new Option('Нет данных', null));
                            }
                        }
                        resolve();
                    },
                    error: function (data) {
                        subcategory.add(new Option('Нет данных', null));
                        resolve()
                    }
                });
            } else {
                subcategory.options.length = 0;
                subcategory.add(new Option('Не выбранно', null));
                resolve();
            }
        });

    }


    function detectCategoryChange() {

        let subcategory = document.getElementById('subcategory_id'),
            hiddenSubcategoryId = document.getElementById('hidden-subcategory-data');

        let category = document.getElementById('category_id'),
            hiddenCategoryId = document.getElementById('hidden-category-data');

        if (hiddenCategoryId && hiddenSubcategoryId) {
            let dataCategory = Number(hiddenCategoryId.innerText),
                dataSubcategory = Number(hiddenSubcategoryId.innerText);

            if (hiddenSubcategoryId && hiddenCategoryId) {

                generateSubcategories(dataCategory, subcategory)
                    .then(() => {
                        category.value = dataCategory;
                        subcategory.value = dataSubcategory;
                    });
            }

        }

        // let subcategory = document.getElementById('subcategory_id');
        // let category = document.getElementById('category_id');
        // let hiddenSubcategoryId = document.getElementById('hidden-subcategory-data');
        // let hiddenCategoryId = document.getElementById('hidden-category-data');
        // if (hiddenSubcategoryId && hiddenCategoryId) {
        //     generateSubcategories(Number(hiddenCategoryId), subcategory)
        //         .then(() => {
        //
        //             subcategory.value = hiddenSubcategoryId.innerText
        //         });
        // }
        if (subcategory) {
        //     subcategory.add(new Option('Не выбранно', null));
            category.addEventListener('change', (ev => {
                let categoryId = ev.target;
                let categoryValue = ev.target.value;
                if (categoryId) {
                    generateSubcategories(categoryValue, subcategory).then(() => {
                    });
                }
            }));
        }
    }

    /*************** additional options from products *****************************/
    function initOptionsChange() {
        let isUpdate = false;
        let indexMc = 1;
        let currentIndex = null;
        let infoError = document.getElementById('additional-error');
        let keyS = document.getElementById('additional_form_key');
        let valueS = document.getElementById('additional_form_value');
        let optionsData = document.getElementById('optionsData');
        let addAdditionalOption = document.getElementById('add-additional-option')
        let additionalContent = document.getElementById('additional_content');
        let additionalCancel = document.getElementById('add-additional-option-cancel');
        let addAdditionalContainer = (key, value) => {

            let removeEl = document.createElement('span');
            removeEl.innerText = 'x';
            removeEl.className = 'removeEl';

            let mainContainer = document.createElement('div');
            mainContainer.className = 'm-3 p-2 text-center card d-flex  cursor-pointer ' +
                'justify-content-between flex-colum flex-wrap align-items-center mainContainer';
            mainContainer.setAttribute('index-attr', `${indexMc}`);
            let keyElement = document.createElement('div');
            keyElement.className = 'additional_content_key';
            keyElement.innerText = `${key}`;

            let valElement = document.createElement('div');
            valElement.className = 'additional_content_value';
            valElement.innerText = `${value}`;

            mainContainer.append(keyElement);
            mainContainer.append(valElement);
            mainContainer.append(removeEl);
            additionalContent.append(mainContainer);
            keyS.value = '';
            valueS.value = '';
            indexMc++;
        };
        let keyValAdditional = () => {
            let retData = {key: null, value: null};

            if (keyS && valueS) {
                let key = keyS.value;
                let value = valueS.value;
                retData = {key, value};
            }
            return retData;
        };

        let clickDaraOption = (ev) => {
            if (isUpdate) {
                let elementHTMLContainer = document.getElementsByClassName('mainContainer');
                [].slice.call(elementHTMLContainer).forEach((element) => {
                    const currentELIndex = element.getAttribute('index-attr');
                    if (currentELIndex === currentIndex) {
                        element.getElementsByClassName('additional_content_key')[0].innerText = keyS.value;
                        element.getElementsByClassName('additional_content_value')[0].innerText = valueS.value;
                        keyS.value = '';
                        valueS.value = '';
                        additionalCancel.classList.remove('d-block');
                        addAdditionalOption.innerText = 'ДОБАВИТЬ';
                        isUpdate = false;
                    }
                });
            } else {
                ev.preventDefault();
                let {key, value} = keyValAdditional();
                if (key.length > 0 && value.length > 0) {
                    addAdditionalContainer(key, value);
                    changeBlock();
                    infoError.classList.remove('d-block');
                } else {
                    infoError.classList.add('d-block');
                }
            }
        };


        /****************change block****************************/
        function changeBlock() {
            let mainContainerList = document.getElementsByClassName('mainContainer');
            [].slice.call(mainContainerList).forEach((el) => {
                el.addEventListener('click', clickChangeBlock);
            });

            function clickChangeBlock(ev) {
                if (ev.target.classList.contains('removeEl')) {
                    this.remove();
                    resetIndex();
                } else {
                    currentIndex = this.getAttribute('index-attr');
                    keyS.value = this.getElementsByClassName('additional_content_key')[0].innerText;
                    valueS.value = this.getElementsByClassName('additional_content_value')[0].innerText;
                    addAdditionalOption.innerText = 'ОБНОВИТЬ';
                    isUpdate = true;
                    additionalCancel.classList.add('d-block');
                }
            }
        }

        /*****************reset index***************************/
        function resetIndex() {
            let mCount = [].slice.call(additionalContent.getElementsByClassName('mainContainer')).length;
            if (mCount === 0) indexMc = 1;
        }

        /****************cancel block****************************/
        function cancelBlock() {
            if (additionalCancel) {
                additionalCancel.addEventListener('click', () => {
                    if (currentIndex) {
                        keyS.value = '';
                        valueS.value = '';
                        addAdditionalOption.innerText = 'ДОБАВИТЬ';
                        isUpdate = false;
                        additionalCancel.classList.remove('d-block');
                    }
                });
            }
        }

        /****************establish existing additional data (model->options)****************************/
        function establishExistingAdditionalData() {
            let hiddenOptionData = document.getElementById('options-storage');
            if (hiddenOptionData) {
                hiddenOptionData = hiddenOptionData.innerText.trim()
                let establishOptionData = [];
                try {
                    establishOptionData = JSON.parse(hiddenOptionData);
                } catch (e) {
                }
                if (establishOptionData.length > 0) {
                    establishOptionData.forEach((dataOption) => {
                        addAdditionalContainer(dataOption.contentKey, dataOption.contentVal);
                        changeBlock();
                    });

                }
            }
        }

        cancelBlock();
        if (addAdditionalOption) {
            establishExistingAdditionalData();
            addAdditionalOption.addEventListener('click', clickDaraOption)
        }
    }

    (() => {
        if (document.getElementsByClassName('delete')[0]) {

            let elementsByClassName = document.getElementsByClassName('delete');
            elementsByClassName[0].addEventListener('click', (event) => {
                let eventTarget = event.target;
                let deleteId = eventTarget.getAttribute('attr-delete');
                let route = eventTarget.getAttribute('attr-route');
                removeCol(route, {id: deleteId}, "POST", "Успешно удаленно!", '/admin/product/list');
            });
        }
    })();
    initOptionsChange();
    formSend('#catForm', '/admin/product/list');
    detectCategoryChange();
    detectTrTable('dataTable', '/admin/product/edit/');
});
