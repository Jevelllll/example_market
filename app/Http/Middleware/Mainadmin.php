<?php


namespace App\Http\Middleware;
use Closure;
use Illuminate\Support\Facades\Auth;

class Mainadmin
{
//$request, Closure $next, $guard = null
    public function handle($request, Closure $next, $guard = null)
    {
        $auth = Auth::guard($guard);
        if(!$auth->user() || !$auth->user()->isAdmin())
            return redirect('/catalog/louis_vuitton/shoes')->with('danger', 'В доступе отказано6!');
        return $next($request);
    }
}
