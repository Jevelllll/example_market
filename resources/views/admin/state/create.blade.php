@extends('admin.layouts.admin_template')
@push('style')
    <link href="{{asset('/css/admin/product.css')}}" rel="stylesheet">
@endpush
@section('content')
    @if($errors->any)
        @foreach($errors->all() as $error)
            <span>
                    {{$error}}
                </span>
        @endforeach
    @endif
    <div class="card shadow mb-4">
        @if($isCreate)
            <h1 class="h3 mb-4 text-gray-800 text-center pt-3">Добавить город</h1>
        @else
            <h1 class="h3 mb-4 text-gray-800 text-center pt-3">Обновить город</h1>
        @endif
        <div class="row justify-content-center">
            @if($isCreate)
                {!! Form::model($model, ['route' => ['city.save'], 'id'=>'countryForm', 'class'=> 'w-75','method' => 'POST']) !!}
            @else
                <span dIscreate="true" class="d-none"></span>
                {!! Form::model($model, ['route' => ['update.city', 'id' => $model->id], 'id'=>'countryForm', 'class'=> 'w-75','method' => 'PUT']) !!}
            @endif
            <div class="form-group">
                {{Form::label('name', 'Значение', ['for' => 'name'])}}
                {{Form::text('name',  null, ['id' => 'name', 'class' => 'form-control'])}}
                <div class="alert alert-danger alert-dismissible d-none">
                    <button type="button" class="close">&times;</button>
                    <strong class="infoError"></strong>
                </div>
            </div>

            @if($isCreate)
                {{Form::submit('Создать',
['class' => 'btn btn-form btn-success shadow-custom text-uppercase border-radius-50 btn-lg w-100 mt-2 mb-3'])}}
            @else
                {{Form::submit('Обновить',
 ['class' => 'btn btn-form btn-success shadow-custom text-uppercase border-radius-50 btn-lg w-100 mt-2 mb-3'])}}
            @endif
            {!! Form::close() !!}
        </div>
        @if(!$isCreate)
            <button class="delete ml-auto btn btn-danger mb-5 float-right"
                    attr-delete="{{$model->id}}"
                    attr-route="{{route("city.delete")}}">Удалить&nbsp;<i class="far fa-trash-alt"></i></button>
        @endif

    </div>
@endsection
@push('scripts')
    <script src="{{asset('/js/admin/countries/countriesList.js')}}"></script>
@endpush
