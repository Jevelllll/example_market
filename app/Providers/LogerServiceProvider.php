<?php

namespace App\Providers;

use App\Helper\SaveLog;
use Illuminate\Support\ServiceProvider;

class LogerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('savel', function () {
            return new SaveLog();
        });
    }
}
