<?php

namespace App\Http\Controllers\Message;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MessageGmailController extends Controller
{

    private $recName;
    private $recMailAddress;
    private $recTextMsg;
    private $subject;
    function __construct($receiverName, $receiverMailAddress, $receiverTextMsg, $mailSubject = '')
    {
        $this->recName = $receiverName;
        $this->recMailAddress = $receiverMailAddress;
        $this->recTextMsg = $receiverTextMsg;
        $this->subject = $mailSubject;
    }

    public function actionSend()
    {
        $data = ['name' => env('APP_NAME'), 'orderModel' => $this->recTextMsg];
        Mail::send('emails.mail', $data, function ($message) {
            $message->to($this->recMailAddress, $this->recName)
                ->subject($this->subject);
            $message->from(env('MAIL_USERNAME'), config('app.name'));
        });
    }

    public function actionSendAnswer($answer, $product, $messageG)
    {
        $answerTo = $answer;
        $productTo = $product;
        $messageTo = $messageG;
        $data = ['answer' => $answerTo, 'product' => $productTo, 'messageTo' => $messageTo];
        Mail::send('emails.mail_answer', $data, function ($message) {
            $message->to($this->recMailAddress, $this->recName)
                ->subject($this->subject);
            $message->from(env('MAIL_USERNAME'), config('app.name'));
        });
    }
}
