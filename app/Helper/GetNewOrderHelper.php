<?php


namespace App\Helper;


use App\Orders;

class GetNewOrderHelper
{
    public function all()
    {
        return Orders::getNewOrder();
    }

}
