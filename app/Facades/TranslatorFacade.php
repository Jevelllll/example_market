<?php


namespace App\Facades;


use Illuminate\Support\Facades\Facade;

class TranslatorFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'translators';
    }
}
