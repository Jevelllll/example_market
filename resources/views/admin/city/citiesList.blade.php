@if($citiesList)
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                <tr>
                    @foreach($citiesTitle as $titles)
                        <th>{{Translators::translateRow()[$titles]}}</th>
                    @endforeach
                </tr>
                </thead>
                <tfoot>
                <tr>
                    @foreach($citiesTitle as $titles)
                        <th>{{Translators::translateRow()[$titles]}}</th>
                    @endforeach
                </tr>
                </tfoot>
                <tbody>
                @foreach($citiesList as $cities)
                    <tr data-id="{{$cities->id}}">
                        @foreach($citiesTitle as $key => $titles)
                            <td>{{$cities->$titles}}</td>
                        @endforeach
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@else
    <div style="font-size: 2.5rem" class="text-center">Отсутствуют города PayPal</div>
@endif
