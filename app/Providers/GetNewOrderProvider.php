<?php

namespace App\Providers;

use App\Helper\GetNewOrderHelper;
use Illuminate\Support\ServiceProvider;

class GetNewOrderProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('getneworder', function () {
            return new GetNewOrderHelper();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
