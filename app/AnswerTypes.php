<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer id
 * @property string controller_name
 */
class AnswerTypes extends Model
{
    protected $table = "answer_types";
    protected $fillable = ["controller_name","created_at","updated_at"];
}
