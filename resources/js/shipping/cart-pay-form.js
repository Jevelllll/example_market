import {Spinner} from "../../my-component/spinner";
import {addClass, viewErrorsAfterSubmit} from "../common";

document.addEventListener('DOMContentLoaded', function () {
    let addressCountry = document.getElementById('address_country'),
        stateElement = document.getElementById('address_state'),
        cityElement = document.getElementById('address_city');
    let payNow = document.getElementById('payNow'),
        muiPayForm = document.getElementById('muiPayForm');
    let dataState;
    let stateNameInput;
    let collectionIds = [
            'address_state', 'address_city', 'address_street', 'address_house_number', 'address_zip_code', 'h-phone'
        ],
        token = document.querySelector("meta[name='csrf-token']");
    let stateRoute = addressCountry.getAttribute('state-route')

    function autocomplete(inp, arr) {
        /*the autocomplete function takes two arguments,
        the text field element and an array of possible autocompleted values:*/
        var currentFocus;
        /*execute a function when someone writes in the text field:*/
        inp.addEventListener("input", function (e) {
            var a, b, i, val = this.value;
            /*close any already open lists of autocompleted values*/
            closeAllLists();
            if (!val) {
                return false;
            }
            currentFocus = -1;
            /*create a DIV element that will contain the items (values):*/
            a = document.createElement("DIV");
            a.setAttribute("id", this.id + "autocomplete-list");
            a.setAttribute("class", "autocomplete-items");
            /*append the DIV element as a child of the autocomplete container:*/
            this.parentNode.appendChild(a);
            /*for each item in the array...*/
            for (i = 0; i < arr.length; i++) {
                /*check if the item starts with the same letters as the text field value:*/
                if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                    /*create a DIV element for each matching element:*/
                    b = document.createElement("DIV");
                    /*make the matching letters bold:*/
                    b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                    b.innerHTML += arr[i].substr(val.length);
                    /*insert a input field that will hold the current array item's value:*/
                    b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                    /*execute a function when someone clicks on the item value (DIV element):*/
                    b.addEventListener("click", function (e) {
                        /*insert the value for the autocomplete text field:*/
                        inp.value = this.getElementsByTagName("input")[0].value;
                        /*close the list of autocompleted values,
                        (or any other open lists of autocompleted values:*/
                        closeAllLists();
                    });
                    a.appendChild(b);
                }
            }
        });
        /*execute a function presses a key on the keyboard:*/
        inp.addEventListener("keydown", function (e) {
            var x = document.getElementById(this.id + "autocomplete-list");
            if (x) x = x.getElementsByTagName("div");
            if (e.keyCode == 40) {
                /*If the arrow DOWN key is pressed,
                increase the currentFocus variable:*/
                currentFocus++;
                /*and and make the current item more visible:*/
                addActive(x);
            } else if (e.keyCode == 38) { //up
                /*If the arrow UP key is pressed,
                decrease the currentFocus variable:*/
                currentFocus--;
                /*and and make the current item more visible:*/
                addActive(x);
            } else if (e.keyCode == 13) {
                /*If the ENTER key is pressed, prevent the form from being submitted,*/
                e.preventDefault();
                if (currentFocus > -1) {
                    /*and simulate a click on the "active" item:*/
                    if (x) x[currentFocus].click();
                }
            }
        });

        function addActive(x) {
            /*a function to classify an item as "active":*/
            if (!x) return false;
            /*start by removing the "active" class on all items:*/
            removeActive(x);
            if (currentFocus >= x.length) currentFocus = 0;
            if (currentFocus < 0) currentFocus = (x.length - 1);
            /*add class "autocomplete-active":*/
            x[currentFocus].classList.add("autocomplete-active");
        }

        function removeActive(x) {
            /*a function to remove the "active" class from all autocomplete items:*/
            for (var i = 0; i < x.length; i++) {
                x[i].classList.remove("autocomplete-active");
            }
        }

        function closeAllLists(elmnt) {
            /*close all autocomplete lists in the document,
            except the one passed as an argument:*/
            var x = document.getElementsByClassName("autocomplete-items");
            for (var i = 0; i < x.length; i++) {
                if (elmnt != x[i] && elmnt != inp) {
                    x[i].parentNode.removeChild(x[i]);
                }
            }
        }

        /*execute a function when someone clicks in the document:*/
        document.addEventListener("click", function (e) {
            closeAllLists(e.target);
        });
    }

    function stateCityStepper() {

        function detectChangeState(cd) {
            stateElement.addEventListener('change', (ev) => {
                if (document.getElementById('address_stateautocomplete-list')) {
                    document.getElementById('address_stateautocomplete-list').addEventListener('click', (el) => {
                        if (el.target.querySelector('input')) {
                            stateNameInput = el.target.querySelector('input').value
                            cd(stateNameInput);
                        } else {
                            cd(false);
                        }
                    })
                }
            })
            stateElement.addEventListener('keyup', (ev) => {
                cd(ev.target.valueOf().value)
            })
            cd(false);
        }

        detectChangeState((data) => {
            if (data) {
                let respCollClass = '';
                let filterCH = dataState.filter(it => it.name.toLowerCase() === data.toLowerCase());
                if (filterCH[0]) {
                    // if (filterCH[0].city.length > 0) {
                    respCollClass = 'col-md-4';
                    adaptiveCols(filterCH, respCollClass);
                    cityElement.parentElement.classList.remove('d-none')
                    let citiesList = filterCH[0].city;
                    autocomplete(cityElement, citiesList.map(city => city.name));
                    // } else {
                    //     respCollClass = 'col-md-6'
                    //     cityElement.parentElement.classList.remove('d-block')
                    //     addClass(cityElement.parentElement, 'd-none', () => {
                    //         adaptiveCols(filterCH, respCollClass);
                    //     });
                    // }
                } else {
                    cityElement.value = '';
                    cityElement.classList.forEach((classS) => {
                        cityElement.classList.remove(classS);
                    });
                    cityElement.classList.add('mui--is-touched');
                    cityElement.classList.add('mui--is-dirty');
                    cityElement.classList.add('mui--is-empty');
                }
            }
        })
    }

    function countryChange() {
        let value = addressCountry.value;
        if (value === '0') {
            lockedFields();
        } else {
            unlockedFields(value);
        }
        addressCountry.addEventListener('change', (el) => {
            [stateElement, cityElement].forEach((el) => el.value = '')
            let valueChange = el.target.value;
            addressCountry.classList.remove('selectInput');
            if (valueChange === '0') {
                lockedFields();
            } else {
                unlockedFields(valueChange);
            }
        })
    }

    function lockedFields() {
        hideCityAndState();
        ['col-md-4', 'col-md-6'].forEach((classRemove) => addressCountry.parentElement.classList.remove(classRemove));
        addClass(addressCountry.parentElement, '', () => {
        });

        for (const el of collectionIds) {
            let elementById = document.getElementById(el) || false;
            elementById ? elementById.disabled = true : false;
            if (elementById.disabled) {
                elementById.parentNode.onmouseout = function () {
                    if (addressCountry.classList.contains('selectInput') && elementById.disabled) {
                        addressCountry.classList.remove('selectInput');
                    }
                };
                elementById.parentNode.onmouseover = () => {
                    if (!addressCountry.classList.contains('selectInput') && elementById.disabled) {
                        addressCountry.classList.add('selectInput');
                    }
                };
            }
        }
    }

    function unlockedFields(countryId) {
        stateAutocompleteStep(countryId);
        for (const el of collectionIds) {
            let elementById = document.getElementById(el) || false;
            elementById.disabled = false;
        }
    }

    function hideCityAndState() {
        stateElement.parentElement.classList.remove('d-block');
        stateElement.parentElement.classList.remove('d-block');
        if (!stateElement.parentElement.classList.contains('d-none')) {
            stateElement.parentElement.classList.add('d-none');

        }
        if (!cityElement.parentElement.classList.contains('d-none')) {
            cityElement.parentElement.classList.add('d-none');
        }
    }

    function adaptiveCols(countNone, responseCollClass) {
        [addressCountry.parentElement, stateElement.parentElement, cityElement.parentElement]
            .forEach((currentElement) => {
                ['col-md-4', 'col-md-6'].forEach((classRemove) => currentElement.classList.remove(classRemove));
                addClass(currentElement, responseCollClass, () => {
                });
            })
    }

    function stateAutocompleteStep(countryId) {
        let responseCollClass = '';
        let locationData = [];
        if (token) {
            hideCityAndState();
            let tokenContent = token.getAttribute('content'),
                countNone = [],
                httpRequest = new XMLHttpRequest();
            httpRequest.open('POST', stateRoute, true);
            httpRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            httpRequest.setRequestHeader("X-CSRF-TOKEN", tokenContent);
            httpRequest.setRequestHeader("X-Requested-With", "XMLHttpRequest");
            httpRequest.responseType = 'json';
            httpRequest.send(`id=${countryId}`);
            Spinner();
            Spinner.show();
            httpRequest.onload = () => {
                if (httpRequest.status === 200) {
                    let stateList = httpRequest.response;
                    if (stateList.length > 0) {
                        locationData = stateList;
                        dataState = locationData;
                        autocomplete(stateElement, dataState.map((it) => it.name))
                        addClass(stateElement.parentElement, 'd-none', () => {
                            countNone.push(stateElement.parentElement);
                            // stateElement.required = true;
                        })
                        addClass(cityElement.parentElement, 'd-none', () => {
                            countNone.push(cityElement.parentElement);
                        })
                        if (countNone.length > 0) {
                            for (const unNoneParentElement of countNone) {
                                unNoneParentElement.classList.remove('d-none');
                            }
                        }
                        switch (countNone.length) {
                            case 2:
                                responseCollClass = 'col-md-4';
                                break;
                            default:
                                responseCollClass = '';
                                break;
                        }
                    } else {
                        responseCollClass = '';
                    }
                    adaptiveCols(countNone, responseCollClass);
                }
                Spinner.hide();
            };
            httpRequest.onerror = () => {
                Spinner.hide();
                console.log('server error!');
            }
        }
    }

    function errorR() {
        Spinner.hide();
    }


    function nextPayCart(method, validateUel, tContent) {
        let httpRequest = new XMLHttpRequest();
        httpRequest.open(method, validateUel, true);
        httpRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        httpRequest.setRequestHeader('X-CSRF-TOKEN', tContent);
        httpRequest.setRequestHeader("X-Requested-With", "XMLHttpRequest");
        httpRequest.responseType = 'json';
        const data = this.serialize();
        return {httpRequest, data};
    }

    function payNowSubmit() {
        muiPayForm.onsubmit = function (e) {
            e.preventDefault();
            Spinner();
            Spinner.show();
            const method = this.getAttribute('method');
            const url = this.getAttribute('action');
            const validateUel = this.getAttribute('validate-payment');
            const tContent = token.getAttribute('content');
            let {httpRequest, data} = nextPayCart.call(this, method, validateUel, tContent);
            httpRequest.send(data);
            httpRequest.onload = () => {
                if (httpRequest.status === 200) {
                    muiPayForm.submit();
                } else {
                    viewErrorsAfterSubmit(httpRequest.response, muiPayForm);
                    Spinner.hide();
                }
            }
            httpRequest.onerror = errorR;
        }
    }
    payNowSubmit();
    countryChange();
    stateCityStepper()
});
window.addEventListener("pageshow", () => {
    let skipIds = ['paypal', '_token', 'order_id'];
    let disabledInp = ['address_street', 'address_house_number', 'address_zip_code', 'h-phone'];
    function resetForm() {
        setTimeout(() => {
            let inputs = muiPayForm.getElementsByTagName('input');
            document.getElementById('address_country').value = 0;
            for (let index = 0; index < inputs.length; ++index) {

                let htmlElement = inputs[index];
                let nameElement = htmlElement.getAttribute('name');
                if (disabledInp.includes(nameElement)) {
                    htmlElement.disabled = true;
                }

                if (!skipIds.includes(nameElement)) {
                    if (htmlElement.classList.contains('mui--is-not-empty')) {
                        htmlElement.classList.remove('mui--is-not-empty');
                        htmlElement.classList.add('mui--is-empty');
                        htmlElement.value = "";
                    }
                }
            }
        }, 0)
    }

    /****/
    resetForm();
});
