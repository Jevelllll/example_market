<?php

namespace App\Http\Controllers\Admin;

use App\Countries;
use App\Http\Controllers\Controller;
use App\Http\Requests\admin\validate\country\CountriesRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CountriesController extends Controller
{
    public function index()
    {
        $countriesList = Countries::all()->sortBy('id')->all();

        if ($countriesList) {
            if ($countriesList[0] instanceof Countries) {
                $countriesTitle = collect($countriesList)[0]->toArray();
                $countriesTitle = array_keys($countriesTitle);
                return view('admin.countries.list', compact('countriesList', 'countriesTitle'))
                    ->withTitle('Страны PayPal');
            } else {
                dd(503);
            }
        } else {
            $data = ["countriesTitle" => null, "countriesList" => null];
            return view('admin.countries.list', $data)
                ->withTitle("Страны PayPal");
        }

    }

    public function create()
    {
        $isCreate = true;
        $model = new Countries();
        return view('admin/countries/create', compact("isCreate", "model"))->withTitle('Добавить страну');
    }

    public function saveCountry(CountriesRequest $request)
    {
        if (Countries::create($request->all())) {
            return response()->json([
                'status' => 'success',
                'msg' => 'Запись успешно добавлена!'
            ]);
        }
    }

    public function show($id)
    {

    }

    public function edit($id)
    {
        $isCreate = false;
        $model = Countries::where('id', (int)$id)->first();

        if ($model instanceof Countries) {
            return view('admin/countries/create', compact("isCreate", "model"))->withTitle('Редактировать страну');
        } else {
            dd(503);
        }
    }

    public function update(Request $request, $id)
    {
        $requestAll = $request->all();
        $model = Countries::where('id', (int)$id)->first();
        $validators = [
            'value' => 'required|unique:countries',
            'name_en' => 'required|unique:countries'];
        foreach ($requestAll as $key => $value) {
            if ($key !== '_method' && $key !== '_token') {
                if ($model->$key === $value) {
                    if (isset($validators[$key])) {
                        $validatorStr = implode('|', array_diff(explode('|', $validators[$key]), ['unique:countries']));
                        $validators[$key] = $validatorStr;
                    }
                }
            }

        }
        $messages = [
            'value.required' => 'Обязательно к заполнению',
            'value.unique' => 'Существующая запись',
            'name_en.unique' => 'Существующая запись',
            'name_en.required' => 'Обязательно к заполнению',
        ];
        Validator::make($requestAll, $validators, $messages)->validate();
        if ($model instanceof Countries) {
            if ($model->update($requestAll)) {
                return response()->json([
                    'msg' => true,
                ], 200);
            } else {
                return response()->json([
                    'msg' => false,
                ], 404);
            }
        } else {
            return response()->json([
                'msg' => false,
            ], 404);
        }

    }

    public function delete(Request $request)
    {
        $requestData = $request->all();
        $model = Countries::find($requestData['id']);
        if ($model instanceof Countries) {
            if ($model->delete()) {
                return response()->json([
                    'msg' => true,
                ], 200);
            } else {
                return response()->json([
                    'msg' => false,
                ], 404);
            }
        } else {
            return response()->json([
                'msg' => false,
            ], 404);
        }
    }

}
