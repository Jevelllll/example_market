<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/****************Main page********/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', 'HomeController@welcome')->name('welcome');
Route::get('/welcome', 'HomeController@index')->name('welcome_index');

/********transition to categories*********/
Route::get('/catalog/{category}', 'CategoryController@index');
Route::get('/catalog/{category}/{subcategory}', 'CategoryController@subcategory');

/**********product********************/
Route::get('/product/{seoName}', 'ProductsController@show')->name('client-product');
Route::post('/product/by_ids', 'ProductsController@by_ids');
Route::post('/product/checkByIds', 'ProductsController@checkByIds');
Route::delete('/product/delete/{id}', 'ProductsController@destroy')->where('id', '[0-9]+');
Route::resource('/product-rate', 'ProductRateController', ['only' => ['store', 'update']]);
/************rules-info-show*****************/
Route::get('/user-information/{name}', 'Admin\RulesForUsersController@show')->name('rfu.show');
/************cart checkout*****************/
Route::post('/cart-checkout', 'CartController@checkout')->name('cart-checkout');
Route::post('/checkout/paypal', 'CartController@index')->name('cart-index');
Route::post('/set-data-order', 'CartController@actionAddDataOrder')->name('set.data.order-cart');
Route::post('/state-by-country', 'CartController@getStateByCountryId')->name('get.state.by-country-id');
Route::post('/city-by-state', 'CartController@getCityByStateId')->name('get.city.by-state-id');
/***************PayPal********************************/
Route::post('/pay-pal/payment', 'PayPalController@payment')->name('payment');
Route::post('/pay-pal/validate', 'PayPalController@validatePayment')->name('payment.validate');
Route::post('/pay-pal/payment-notify', 'PayPalController@notify')->name('payment.notify');
Route::get('/pay-pal/payment-cancel', 'PayPalController@cancel')->name('payment.cancel');
Route::get('/pay-pal/payment-success', 'PayPalController@success')->name('payment.success');
/**************Send GMAIL******************************/
Route::post('/message/send-product-answer', 'PayPalController@actionSendAnswer')->name('send.product.answer');

Auth::routes();

/******admin*******/
//Route::group(['middleware' => ['admin']], function () {
//    Route::get('/admin', 'Admin\AdminController@index');
//});
Route::get('/home', function () {
    return redirect('/')->with('danger', 'Invalid action, access denied!');
})->name('home');
Route::group(
    [
        'prefix' => 'admin',
        'middleware' => ['auth', 'admin'],
    ],
    function () {

        Route::get('/', 'Admin\AdminController@index')->name('admin-redirect');
        Route::get('/logout', 'Admin\AdminController@logout')->name('admin.logout');
        Route::post('/get-new-order', 'Admin\AdminController@getNewOrder')->name('admin.getNewOrder');
        Route::post('/message/send-product-answer-to', 'Admin\AnswerController@actionSendAnswerTo')->name('send.product.answer.to');
        Route::group(['prefix' => 'banner'], function () {

            Route::get('/list', 'BannersController@dataAll');
            Route::get('/create', 'BannersController@dataCreate');
            Route::get('/edit/{id}', 'BannersController@edit')->where('id', '[0-9]+')->name('banner.edit');
            Route::put('/update/{id}', 'BannersController@update')->name('banner.update');
            Route::get('/banners-files/{id}', 'BannersController@findFilesByBannersId')->where('id', '[0-9]+')->name('banner.files');
            Route::get('/banners-files/{id}/{is_q_o}', 'BannersController@findFilesByBannersId')->name('qOffer.files');
            Route::post('/dataBannerSave', 'BannersController@dataBannerSave');
            Route::get('/quick-offer-create', 'BannersController@quickOfferCreate');
            Route::post('/quick-offer-save', 'BannersController@quickOfferSave')->name('qOffer.save');
            Route::put('/quick-offer-update/{id}', 'BannersController@quickOfferUpdate')->name('qOffer.update');
            Route::post('/dataImgCreate', 'BannersController@dataImgCreate')->name('projects.storeMedia');
            Route::post('/dataImgDelete', 'BannersController@dataImgDelete')->name('projects.delete');
            Route::post('/delete', 'BannersController@delete')->where('id', '[0-9]+')->name('banner.delete');
        });
        /**************category === brands************************/
        Route::group(['prefix' => 'brand'], function () {

            Route::get('/list', 'CategoryController@categoryList')->name('list.brand');
            Route::get('/create', 'CategoryController@create')->name('new.brand');
            Route::post('/save', 'CategoryController@save')->name('save.brand');
            Route::put('/update/{id}', 'CategoryController@update')->name('update.brand');
            Route::get('/edit/{id}', 'CategoryController@edit')->where('id', '[0-9]+')->name('edit.brand');
            Route::get('/brand-files/{id}', 'CategoryController@findFilesByCatId')->where('id', '[0-9]+')->name('brand.files');
            Route::post('/delete', 'CategoryController@delete')->where('id', '[0-9]+')->name('brand.delete');
        });
        /************* subcategory*****************************/
        Route::group(['prefix' => 'subcategory'], function () {
            Route::get('/list', 'Admin\SubcategoryController@subcategoryList')->name('list.subcategory');
            Route::get('/create', 'Admin\SubcategoryController@subcategoryCreate')->name('create.subcategory');
            Route::post('/save', 'Admin\SubcategoryController@subcategorySave')->name('save.subcategory');
            Route::put('/update/{id}', 'Admin\SubcategoryController@subcategoryUpdate')->where('id', '[0-9]+')->name('update.subcategory');
            Route::get('/edit/{id}', 'Admin\SubcategoryController@subcategoryEdit')->where('id', '[0-9]+')->name('edit.subcategory');
            Route::post('/delete', 'Admin\SubcategoryController@subcategoryDelete')->where('id', '[0-9]+')->name('delete.subcategory');
            Route::get('/subcategory-files/{id}', 'Admin\SubcategoryController@findFilesByCatId')->where('id', '[0-9]+')->name('subcategory.files');
        });
        /**************admin - products************************/
        Route::group(['prefix' => 'product'], function () {

            Route::get('/list', 'ProductsController@productList')->name('list.product');
            Route::get('/position/{product_id}', 'ProductsController@positionPictures')->name('position.pictures');
            Route::post('/position/update', 'ProductsController@positionPUpdate')->name('position.pictures.update');
            Route::get('/create', 'ProductsController@create')->name('new.product');
            Route::post('/save', 'ProductsController@productSave')->name('save.product');
            Route::put('/update/{id}', 'ProductsController@update')->name('update.product');
            Route::get('/edit/{id}', 'ProductsController@edit')->where('id', '[0-9]+')->name('edit.product');
            Route::get('/product-files/{categoryId}', 'ProductsController@findFilesByProductId')->where('categoryId', '[0-9]+')->name('product.files');
            Route::post('/delete', 'ProductsController@delete')->where('id', '[0-9]+')->name('product.delete');
            Route::get('/subcat-by-category/{categoryId}', 'ProductsController@getSubcategoryByCategoryId')
                ->where('categoryId', '[0-9]+')->name('product.getSubByCategory');
            Route::post('/dataImgDelete', 'ProductsController@dataImgDelete')->name('product_image.delete');

            Route::get('/binding/list/{id}', 'Admin\ProductBindingController@index')->name('index.product-binding');
            Route::post('/binding/save', 'Admin\ProductBindingController@save')->name('save.product-binding');
        });
        /*************admin - product clone************************/
        Route::group(['prefix' => 'product-clone'], function () {
            Route::get('/show/{id}', 'Admin\ProductsHistoryController@show')->where('id', '[0-9]+')->name('product.clone');
        });
        /***********************SEO *******************************/
        Route::group(['prefix' => 'seo'], function () {

            Route::get('/list', 'Admin\SeoController@index')->name('seo.list');
            Route::get('/product', 'Admin\SeoController@product')->name('seo.product');
            Route::get('/subcategory', 'Admin\SeoController@subcategory')->name('seo.subcategory');
            Route::get('/brand', 'Admin\SeoController@brand')->name('seo.brand');
            Route::get('/default', 'Admin\SeoController@default')->name('seo.default');
            Route::get('/edit/{id}', 'Admin\SeoController@edit')->name('seo.edit');
            Route::put('/update/{id}', 'Admin\SeoController@update')->name('seo.update');
            Route::post('/save', 'Admin\SeoController@save')->name('seo.save');
            Route::post('/delete', 'Admin\SeoController@delete')->name('seo.delete');

        });
        /***********************RULES******************************/
        Route::group(['prefix' => 'rules'], function () {
            Route::get('/list', 'Admin\RulesForUsersController@index')->name('rfu.list');
            Route::get('/edit/{id}', 'Admin\RulesForUsersController@edit')->where('id', '[0-9]+')->name('rfu.edit');
            Route::put('/update/{id}', 'Admin\RulesForUsersController@update')->name('rfu.update');
        });
        /*********************countries PAYPAL************************/
        Route::group(['prefix' => 'country'], function () {
            Route::get('/list', 'Admin\CountriesController@index')->name('country.list');
            Route::get('/create', 'Admin\CountriesController@create')->name('new.country');
            Route::post('/save', 'Admin\CountriesController@saveCountry')->name('country.save');
            Route::get('/edit/{id}', 'Admin\CountriesController@edit')->where('id', '[0-9]+')->name('edit.country');
            Route::put('/update/{id}', 'Admin\CountriesController@update')->name('update.country');
            Route::post('/delete', 'Admin\CountriesController@delete')->where('id', '[0-9]+')->name('country.delete');

        });
        /*********************city PAYPAL************************/
        Route::group(['prefix' => 'city'], function () {
            Route::get('/list', 'Admin\CitiesController@index')->name('city.list');
            Route::get('/create', 'Admin\CitiesController@create')->name('new.city');
            Route::post('/save', 'Admin\CitiesController@save')->name('city.save');
            Route::get('/edit/{id}', 'Admin\CitiesController@edit')->where('id', '[0-9]+')->name('edit.city');
            Route::get('/get-city-by-stat/{id}', 'Admin\CitiesController@getCitiesByStateId')->where('id', '[0-9]+')->name('getCitiesByStateId.list');
            Route::put('/update/{id}', 'Admin\CitiesController@update')->name('update.city');
            Route::post('/delete', 'Admin\CitiesController@delete')->where('id', '[0-9]+')->name('city.delete');
        });
        /*********************state PAYPAL************************/
        Route::group(['prefix' => 'state'], function () {
            Route::get('/list', 'Admin\StateController@index')->name('state.list');
            Route::get('/create', 'Admin\StateController@create')->name('new.state');
            Route::post('/save', 'Admin\StateController@save')->name('state.save');
            Route::get('/edit/{id}', 'Admin\StateController@edit')->where('id', '[0-9]+')->name('edit.state');
            Route::put('/update/{id}', 'Admin\StateController@update')->name('update.state');
            Route::post('/delete', 'Admin\StateController@delete')->where('id', '[0-9]+')->name('state.delete');
        });
        /*****************Transactions******************************/
        Route::group(['prefix' => 'transactions'], function () {
            Route::get('/list', 'Admin\TransactionsController@index')->name('admin.transactions.list');
            Route::get('/view/{id}', 'Admin\TransactionsController@show')->name('admin.transactions.view');
        });
        /*****************Order******************************/
        Route::group(['prefix' => 'order'], function () {
            Route::get('/list', 'Admin\OrderController@index')->name('admin.order.list');
            Route::get('/view/{id}', 'Admin\OrderController@show')->name('admin.order.view');
        });
        /*****************Answer******************************/
        Route::group(['prefix' => 'answer'], function () {
            Route::get('/list', 'Admin\AnswerController@index')->name('admin.answer.list');
            Route::get('/view/{id}', 'Admin\AnswerController@show')->name('admin.answer.view');
        });
    }
);
/* Sitemap */
Route::get('/sitemap.xml', 'site_map\SitemapController@index')->name('sitemap.index');
Route::get('/sitemap/brands.xml', 'site_map\SitemapController@brand')->name('sitemap.brand');
Route::get('/sitemap/subcategories.xml', 'site_map\SitemapController@subCat')->name('sitemap.subCat');
Route::get('/sitemap/goods.xml', 'site_map\SitemapController@product')->name('sitemap.product');
