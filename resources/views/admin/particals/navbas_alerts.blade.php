<a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <i class="fas fa-bell fa-fw"></i>
    <!-- Counter - Alerts -->
    <?php $orderModel = OrderModel::all() ?>
    <span class="badge badge-danger badge-counter"><span id="badgeOrder">+{{count($orderModel)}}</span></span>
</a>

<!-- Dropdown - Alerts -->
<div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
    <h6 class="dropdown-header">
        Новые заказы
    </h6>
    <div class="container-list">
        @foreach($orderModel as $order)

            <a class="dropdown-item d-flex align-items-center" href="{{route('admin.order.view', ['id'=> $order->id])}}">
                <div class="mr-3">
                    <div class="icon-circle bg-success">
                        <i class="fas fa-donate text-white"></i>
                    </div>
                </div>
                <div>
                    <div class="small text-gray-500">{{$order->created_at}}</div>
                    <span class="font-weight-bold">Заказ на сумму {{$order->total_sum}} USD</span>
                </div>
            </a>

            @if(count($order->transaction) > 0)
                @foreach($order->transaction as $transaction)
                    <a class="dropdown-item d-flex align-items-center" href="{{route('admin.transactions.view', ['id'=> $transaction->id])}}">
                        <div class="mr-3">
                            <div class="fab fa-paypal">
                                <i class="fas fa-donate text-white"></i>
                            </div>
                        </div>
                        <div>
                            <div class="small text-gray-500">{{$transaction->created_at}}</div>
                            <span class="font-weight-bold">Статус платежа {{$transaction->payment_status}}</span>
                        </div>
                    </a>
                @endforeach
            @endif
        @endforeach
    </div>
</div>
