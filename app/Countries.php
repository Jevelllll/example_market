<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer id
 * @property integer idMap
 * @property string name_en
 * @property string name_ru
 * @property string value
 * @property string custom_value
 * @property string phonecode
 */
class Countries extends Model
{
    protected $table = 'countries';
    protected $fillable = [
        "created_at", "updated_at", "value", "name_en",
        "name_ru","custom_value", "phonecode", "idMap"
    ];

    public function oneState() {
        return $this->hasOne('App\State', 'country_id', 'idMap');
    }
    public function oneCity() {
        return $this->hasOne('App\City', 'country_id', 'idMap');
    }
}
