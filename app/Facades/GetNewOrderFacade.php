<?php
namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class GetNewOrderFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'getneworder';
    }
}
