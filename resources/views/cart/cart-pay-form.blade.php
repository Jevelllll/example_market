<div class="container">
    {{Form::model($customer, ['route' => 'payment', 'method' => 'post',
 'class' => 'mui-form', 'id' => 'muiPayForm', 'validate-payment' => route('payment.validate')])}}
    {{Form::hidden('order_id', $orderModel->id)}}
    <div class="row">
        <div class="mui-textfield mui-textfield--float-label col-12 col-md-6 ">
            {{Form::text('first_name',  null, ['id' => 'first_name'])}}
            {{Form::label('first_name', 'First name *', ['for' => 'first_name'])}}
        </div>
        <div class="mui-textfield mui-textfield--float-label col-12 col-md-6">
            {{Form::text('last_name',  null, ['id' => 'last_name'])}}
            {{Form::label('last_name', 'Last name *', ['for' => 'last_name'])}}
        </div>
    </div>
    <div class="row">
        <div class="mui-select col-12">
            <select name="address_country" id="address_country"
                    state-route="{{route('get.state.by-country-id')}}"
                    city-route="{{route('get.city.by-state-id')}}">
                <option disabled selected value="0">Select Country</option>
                @foreach($countryList as $country)
                    <option country-code="{{$country['custom_value']}}" value="{{$country['idMap']}}">
                        {{$country['name_en']}}
                    </option>
                @endforeach
            </select>
            {!! Form::label('address_country', 'Country *', ['for' => 'address_country'])!!}
        </div>
        <div class="mui-textfield mui-textfield--float-label col-12 d-none">
            {{Form::text('address_state',  null, ['id' => 'address_state'])}}
            {{Form::label('address_state', 'State *', ['for' => 'address_state'])}}
        </div>
        <div class="mui-textfield mui-textfield--float-label col-12 d-none">
            {{Form::text('address_city',  null, ['id' => 'address_city'])}}
            {{Form::label('address_city', 'City *', ['for' => 'address_city'])}}
        </div>
    </div>
    <div class="row">
        <div class="mui-textfield mui-textfield--float-label col-12 col-md-4">
            {{Form::text('address_street',  null, ['id' => 'address_street'])}}
            {{Form::label('address_street', 'Street *', ['for' => 'address_street'])}}
        </div>
        <div class="mui-textfield mui-textfield--float-label col-12 col-md-4">
            {{Form::text('address_house_number',  null, ['id' => 'address_house_number'])}}
            {{Form::label('address_house_number', 'House number *', ['for' => 'address_house_number'])}}
        </div>
        <div class="mui-textfield mui-textfield--float-label col-12 col-md-4">
            {{Form::text('address_zip_code',  null, ['id' => 'address_zip_code'])}}
            {{Form::label('address_zip_code', 'Zip/Postal Code *', ['for' => 'address_zip_code'])}}
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-md-6 ">
            {{Form::text('h-phone',  null, ['id' => 'h-phone'])}}
        </div>
        {{Form::hidden('phone', null, ['id' => 'phone'])}}
        <div class="mui-textfield mui-textfield--float-label col-12 col-md-6">
            {{Form::email('email',  null, ['id' => 'email', 'type' => 'email'])}}
            {{Form::label('email', 'Email *', ['for' => 'email'])}}
        </div>
    </div>
    <div class="row">
        <div class="mui-textfield mui-textfield--float-label col">
            {{Form::textarea('comment',  null, ['id' => 'comment', 'rows' => 3])}}
            {{Form::label('comment', 'Comment', ['for' => 'comment'])}}
        </div>
    </div>

    {{Form::submit('pay with paypal', ['name' => 'paypal', 'id' => 'payNow', 'class' => 'button-add-cart col proceedToCheckout purple-gr'])}}
    {{Form::close()}}

</div>
@push('scripts')
    <script
        src="{{ asset('plugins/phone-mask/js/intlTelInput.js') }}?v=<?= filemtime('plugins/phone-mask/js/intlTelInput.js') ?>"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            // setTimeout(() => {
            //     console.log(22);
            //    $('#first_name').removeClass('mui--is-not-empty').addClass("mui--is-empty").val("");
            // },1222)


            var input = document.querySelector("#h-phone");
            var inputHiddenPhone = document.querySelector("#phone");
            input.value = "";
            inputHiddenPhone.value = "";
            $(input).unmask();
            setTimeout(function () {
                var itiContainer = document.querySelector(".iti");
                ["mui-textfield", "w-100"].forEach(function (addCl) {
                    itiContainer.classList.add(addCl);
                });
                var label = document.createElement("label");
                label.innerText = "Phone number *";
                label.id = "phone_label";
                label.setAttribute("for", "h-phone");
                itiContainer.appendChild(label);
                itiContainer.addEventListener("focusin", function () {
                    if (!input.disabled) {
                        label.style.left = "50px";
                    }
                }, false);
                itiContainer.addEventListener("focusout", function () {
                    if (!input.disabled) {
                        label.style.left = "0";
                    }
                }, false);

            }, 0)

            function preLoad(callback) {
                var iti = window.intlTelInput(input, {
                    initialCountry: "auto",
                    formatOnDisplay: true,
                    geoIpLookup: function (success) {
                        document.querySelector('#address_country').addEventListener('change', function () {
                            input.value = "";
                            inputHiddenPhone.value = "";
                            $(input).unmask();
                            if (!['0', 0, 'null', null].includes(this.value)) {
                                var valueCountry = this.value;
                                var querySelector = this.querySelector("option[value= '" + valueCountry + "']");
                                var countryCode = querySelector.getAttribute('country-code');
                                success(countryCode)
                                callback(iti);
                            }
                        });
                    },
                    utilsScript: "{{asset('plugins/phone-mask/js/utils.min.js')}}",
                });
            }

            preLoad(function (iti) {
                input.onkeyup = function () {
                    inputHiddenPhone.value = iti.getNumber(intlTelInputUtils.numberFormat.E164);
                };
                setTimeout(function () {
                    let placeholderModify = input.getAttribute('placeholder').replace(/[0-9]/g, 0).replace(/\s/g, '-');
                    $(input).mask(placeholderModify);
                }, 0)
            });
        })
    </script>
@endpush

