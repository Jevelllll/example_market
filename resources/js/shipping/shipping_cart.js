let proceedToCheckout = {

    eventToCheckout: document.querySelector(".proceedToCheckout"),
    getPCurrentPurchase() {
        let purchase = localStorage.getItem('purchase');
        let parsePurchase;
        try {
            parsePurchase = JSON.parse(purchase);
        } catch ($e) {
            console.log('error cart process');
        }
        if (Array.isArray(parsePurchase)) {
            return parsePurchase;
        }
        return false;
    },
    toCheckout($arg) {
        let token = document.querySelector("meta[name='csrf-token']");
        if (token) {
            let tokenContent = token.getAttribute('content');
            if (tokenContent) {
                if (!$arg.target.getAttribute('data-attr-r-checkout')) return false;
                let target = $arg.target;
                let isProceedToCheckout = target.classList.contains('proceedToCheckout');
                if (isProceedToCheckout) {
                    let toUrl = target.getAttribute('data-attr-r-checkout');
                    this.sendRequest(toUrl, this.getPCurrentPurchase(), tokenContent)
                }
            }
        }
        return false;
    },

    sendRequest(url, productIds, tokenContent) {
        if (productIds) {
            productIds = productIds.map(idsItem => idsItem.split(':').pop());
            let httpRequest = new XMLHttpRequest();
            httpRequest.open('POST', url, true);
            httpRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            httpRequest.setRequestHeader("X-CSRF-TOKEN", tokenContent);
            httpRequest.setRequestHeader("X-Requested-With", "XMLHttpRequest");
            httpRequest.responseType = 'json';
            httpRequest.send(`data=[${productIds}]`);
            httpRequest.onload = () => {
                if (httpRequest.status === 200) {
                    let response = httpRequest.response;
                    let isHaveRedirect = response.hasOwnProperty('redirect');
                    let isHaveOrderId = response.hasOwnProperty('orderId');
                    if (isHaveRedirect && isHaveOrderId) {
                        let newForm = document.createElement('form');
                        newForm.action = response.redirect;
                        newForm.method = "POST";
                        let input = document.createElement('input');
                        input.type = 'hidden';
                        input.name = 'orderId';
                        input.value = response.orderId;
                        let inputToken = document.createElement('input');
                        inputToken.type = 'hidden';
                        inputToken.name = '_token';
                        inputToken.value = tokenContent;
                        newForm.appendChild(inputToken);
                        newForm.appendChild(input);
                        document.body.appendChild(newForm);
                        newForm.submit();
                    }
                }
            }
            httpRequest.onerror = () => {
                console.log('server error!');
            }
        }
    },


    startListener() {
        this.eventToCheckout.addEventListener('click', this.toCheckout.bind(this));
    }
}

proceedToCheckout.startListener();
