<?php

namespace App\Providers;

use App\Helper\GetNewAnswerHelper;
use Illuminate\Support\ServiceProvider;

class GetNewAnswerProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('getnewanswer', function () {
            return new GetNewAnswerHelper();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
