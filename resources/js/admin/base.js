export function detectTrTable(tableId = 'dataTable', urlId = '/admin/banner/edit/') {
    $(document).on("click", ".close", function (e) {
        e.preventDefault();
        $(this).parent().removeClass("d-block").addClass('d-none')
    });
    let elementById = document.getElementById(tableId);

    if (elementById) {
        let elementTbody = elementById.querySelector('tbody');
        let elementAllTr = elementTbody.querySelectorAll('tr');

        let clickToTr = function (e) {
            let trElement = e.target.parentElement;
            if (trElement.classList.contains('even') || trElement.classList.contains('odd')) {

                if (trElement.getAttribute('data-id')) {
                    let bannerId = trElement.getAttribute('data-id');

                    const url = `${urlId}${bannerId}`;
                    document.location.href = url
                }
            }
        };
        [].slice.call(elementAllTr).forEach((element) => element.addEventListener('dblclick', clickToTr));
    }

}

export function errorsToDb(data) {
    let datum = data["responseJSON"].errors;
    Object.keys(datum).map((fields) => {

        let htmlElement = document.getElementById(fields);
        if (fields === 'active') {
            let active = htmlElement.parentElement.parentElement;
            let alert = $(active).find('.alert');
            $(alert).find(".infoError").text(datum[fields]);
            $(alert).addClass("d-block");
        } else if (fields === 'gallery') {
            let mainGallery = $('#document-dropzone');
            let alert = $(mainGallery).siblings(".alert");
            $(alert).find(".infoError").text(datum[fields]);
            $(alert).addClass("d-block");
        } else {
            if (htmlElement) {
                let alert = $(htmlElement).siblings(".alert");
                $(alert).find(".infoError").text(datum[fields]);
                $(alert).addClass("d-block");
            }
        }
    });
}

export function formSend(idFormJqFormat, toReloadUrl = null) {

    $(idFormJqFormat).submit(function (e) {
        e.preventDefault();
        let additionalContent = document.getElementById("additional_content");
        let optionsData = document.getElementById("optionsData");
        if (optionsData) {
            optionsData.setAttribute('value', '');
        }
        if (additionalContent) {
            let subContents = document.getElementsByClassName('mainContainer');
            if (subContents.length > 0) {
                let additionalElements = [];
                [].slice.call(subContents).forEach((el) => {
                    let contentKey = el.getElementsByClassName('additional_content_key');
                    let contentVal = el.getElementsByClassName('additional_content_value');
                    if (contentKey.length > 0 && contentVal.length > 0) {
                        contentKey = contentKey[0].innerText;
                        contentVal = contentVal[0].innerText;
                        if (contentKey.length > 0 && contentVal.length > 0) {
                            additionalElements.push({contentKey, contentVal})
                        }
                    }
                });
                if (optionsData) {
                    optionsData.setAttribute('value', JSON.stringify(additionalElements));
                }
            }
        }

        $(".alert").removeClass("d-block").addClass("d-none");
        $(".infoError").text("");
        $.ajax({
            method: e.currentTarget.method,
            url: e.currentTarget.action,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: $(this).serialize(),
            beforeSend: function () {
            },
            success: function (data) {
                swal({
                    title: 'Успех!',
                    text: 'Запись успешно добавлена!',
                    type: "success",
                    confirmButtonText: "OK",
                    showCancelButton: false
                }).then((result) => {
                    $(idFormJqFormat)[0].reset();
                    if (toReloadUrl) {
                        window.location.href = toReloadUrl;
                    }
                });
            },
            error: function (data) {
                try {
                    errorsToDb(data);
                } catch (e) {

                }
            }
        });
    });
}

export function translatorTableColumn() {
    return {
        "order": [[ 0, "desc" ]],
        "language": {
            "lengthMenu": "Показано _MENU_ записей на странице",
            "zeroRecords": "Nothing found - sorry",
            "info": "Показ страницы _PAGE_ из _PAGES_",
            "infoEmpty": "Нет доступных записей",
            "infoFiltered": "(отфильтровано _MAX_ записей)",
            'search': "Найти",
            "paginate": {
                "first": "Первый",
                "last": "Прошлой",
                "next": "Вперед",
                "previous": "Назад"
            },
        }
    };
}

export function removeCol(url, body, method, text = "", redirect) {

    if (typeof body === 'object') {
        body = $.param(body);
    } else {
        body = $(body).serialize();
    }

    $.ajax({
        method: method,
        url: url,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: body,
        beforeSend: function () {
        },
        success: function (data) {
            swal({
                title: 'Успех!',
                text: text,
                type: "success",
                confirmButtonText: "OK",
                showCancelButton: false
            }).then((result) => {
                if (redirect) {
                    window.location.href = redirect;
                }
            });
        },
        error: function (data) {
            try {
                errorsToDb(data);
            } catch (e) {

            }


        }
    });
}




